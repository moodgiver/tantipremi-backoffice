<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * UUID Class
 *
 * This implements the abilities to create UUID's for CodeIgniter.
 * Code has been borrowed from the followinf comments on php.net
 * and has been optimized for CodeIgniter use.
 * http://www.php.net/manual/en/function.uniqid.php#94959
 *
 * @category Libraries
 * @author Dan Storm
 * @link http://catalystcode.net/
 * @license GNU LPGL
 * @version 2.1 
 */
class Service 
{

	protected $CI;
	
	// We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct()
    {
    	// Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

	public function web_url(){
		return $this->CI->config->item('tpremi');
	}
	
	public function cdn_url(){
		return $this->CI->config->item('cdn');
	}

}