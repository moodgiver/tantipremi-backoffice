<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Status extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function export_status ( $id ){
		$this->load->library('export');
		$this->load->model('Campaign_model');
		$sql = $this->Campaign_model->campaign_export($id);
		if ( count ( $sql ) > 0 ){
			$this->export->to_excel($sql, 'partecipanti'); 
			
		} else {
			echo '<script>alert("Nessun record trovato");</script>';
		}

	}
	
	public function export_status_filter ( $id,$status ){
		$this->load->library('export');
		$this->load->model('Campaign_model');
		$sql = $this->Campaign_model->campaign_export_filter($id,$status);
		if ( count ( $sql ) > 0 ){
			$this->export->to_excel($sql, 'partecipanti'); 
			
		} else {
			echo '<script>alert("Nessun record trovato");</script>';
		}

	}
	 
	public function ajax_customer_status(){
		$sendmail = $_POST['sendmail'];
		$this->load->model('Customers_model');
		$campaign = $this->Customers_model->update_customer_campaign_status($_POST);
	}
	 
	public function ajax_refresh_status(){
		$status = $_POST['nextstatus'];
		$s = $this->config->item('status');
		$class = $this->config->item('status_label');
		$h = $this->config->item('status_help');
		$sendmail = $this->config->item('status_send_mail');
		$nextstatus = $this->config->item('status_flow');
		$btn = $this->config->item('status_btn');
		if ( $status != '8' ){
            //if ( $sendmail[$_POST['sendmail']] ){
	    	//	$this->send_status_mail($status);
		    //}
    		$label = '
			<label class="label label-'.$class[$status].'" 
				data-toggle="tooltip" 
				title="'.$h[$status].'">'
				.$s[$status].
			'</label>
			<br>
			<span class="fa fa-chevron-down"></span>
			<br>
				<button class="btn btn-'.$class[$nextstatus[$status]].' 
					btn-sm btn-action" 
					data-controller="update-status" 
					data-id="'.$_POST['id'].'" 
					data-status="'.$status.'" 
					data-campaign="'.$_POST['campaign'].'" 
					data-email="'.$_POST['email'].'" 
					data-flow="'.$nextstatus[$status].'" 
					data-sendmail="'.$sendmail[$status].'">'.$btn[$status].'</button>
			';
		    echo $label;
        } else {
            $label = '
			<label class="label label-'.$class[$status].'" 
				data-toggle="tooltip" 
				title="'.$h[$status].'">'
				.$s[$status].
			'</label>';
            echo $label;
            
        }
	} 
	 
	public function send_status_mail($status)
	{	
		$this->load->model('Campaigns_model');
		$campaign = $this->Campaigns_model->campaign($_POST['campaign']);
		
		$msg = $this->config->item('status_email');
		$content = $msg[$status];
		
		/*
		$message = '<h2>Concorso '.$campaign[0]['campaign'].'</h2>';
		$message .= 'Gentile Cliente ('.$_POST['id'] .')<br>';
		$message .= "la tua registrazione &egrave; stata accettata ". $_POST['status'];
    	$this->email->message($message);
    	//$this->email->attach('/path/to/file1.png'); // attach file
    	//$this->email->attach('/path/to/file2.pdf');
    	if ($this->email->send()){
        	echo 'OK';
			return true;
    	} else {
			echo 'error sending email';
        	return false;
		}
		*/
		
		$message = '<h2>Concorso '.$campaign[0]['campaign'].'</h2>';
		$message .= 'Gentile Cliente <br>';
		$message .= $content;
		
		$this->load->library('email'); // load email library
		
    	$this->email->from('no-reply@tantipremi.it', 'TANTIPREMI');
		$this->email->bcc('tantipremi@indual.it');
    	$this->email->to($_POST['email']);
    	$this->email->subject("Concorso ".$campaign[0]['campaign']);
		$this->email->message($message);
		if ($this->email->send()){
			return true;
    	} else {
			echo 'error sending email';
        	return false;
		}
		
		/*
		$subject = "Concorso ".$campaign[0]['campaign'];
		$headers = "From: antonio.nardone@indual.it\r\n";
		$headers .= "Reply-To: antonio.nardone@indual.it\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		
		$message = '<h2>Concorso '.$campaign[0]['campaign'].'</h2>';
		$message .= 'Gentile Cliente <br>';
		$message .= $content;
		mail ( $_POST['email'] , $subject , $message , $headers );
		//echo $message;
		return true;
		*/
		
	}
	
	
}