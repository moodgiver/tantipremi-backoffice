<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Campaigns_model');
		$this->load->model('Service_model');
		$this->load->model('Instantwin_model');
		$this->load->helper('captcha');
        date_default_timezone_set("Europe/Rome");

	}

	public function index()
	{
		if ( isset ( $_SESSION['login'] ) && $_SESSION['login'] ){
			$this->load->view('header');
			$this->load->view('sidebar-menu');
			$data['campaigns'] = $this->Campaigns_model->campaigns();
			$this->load->view('concorsi',$data);
			$this->load->view('footer');
		} else {
			$this->load->view('login');
		}
	}

	public function logout(){
		$this->session->set_userdata('login', false);
		echo '
				<script>
					window.location.href = "'.base_url().'";
				</script>';
	}


	//upload FILES FOR CAMPAIGN
	public function upload ( ){
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'];
			$fileName = $_FILES['file']['name'];

			if ( $_POST['folder'] != 'public' ){
				$fileName = $_POST['code'].'_'.str_replace ( ' ' , '' , $fileName );
				$targetPath = getcwd() . "/public/download/";
			} else {
				$fileName = str_replace ( ' ' , '_' , $fileName );
				$targetPath = "/home/admin/public_html/tpbolton/public/img/".$_POST['code']."/";
			}

			$targetFile = $targetPath . $fileName ;
			list($width, $height,$type,$attr) = getimagesize($tempFile);
			if ( intval($width) != 0 && intval($height) != 0 ) {
				if  ( move_uploaded_file($tempFile, $targetFile) ){
					if ( $_POST['field'] != 'prize' ){
						$data = array (
							'field' => $_POST['field'],
							'file' => $fileName ,
							'id' => $_POST['id']

						);
						$this->load->model('Campaign_model');
						$this->Campaign_model->uploaded_file($data);
						return true;
					} else {
						echo $_POST['id'];
						$data = array (
							'file' => $fileName ,
							'id' => $_POST['id']

						);
						$this->load->model('Campaign_model');
						$this->Campaign_model->prize_uploaded_file($data);
						return true;

					}
				}
			}

		}
	}

    public function upload_scontrino(){
        if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'];
			$myFile = explode ('.' , $_FILES['file']['name']);


			$fileName = $_POST['customer_id'].'_'.$_POST['uuid'].'.'.$myFile[count($myFile)-1];
			$targetPath = "/home/admin/public_html/tpbolton/public/users/upload/temp/";
			$targetFile = $targetPath . $fileName ;
			$_SESSION['scontrino'] = $fileName;

			list($width, $height,$type,$attr) = getimagesize($tempFile);

			if ( intval($width) != 0 && intval($height) != 0 ) {
				move_uploaded_file($tempFile, $targetFile);
			    return $fileName;
            }
		}
    }

	public function export_coupons ( $id , $prize ){

		$this->load->library('export');
		$sql = $this->Service_model->list_coupons($id ,$prize);
		if ( count ( $sql->result_array() ) > 0 ){
			$this->export->to_excel($sql, 'coupons');

		} else {
			echo '<script>alert("Non ci sono coupon per questo premio. Clicca sul pulsante Crea Coupon");</script>';
		}

	}

	public function export_instant_win ( $id ){

		$this->load->library('export');
		$sql = $this->Instantwin_model->vincite($id);
		if ( count ( $sql->result_array() ) > 0 ){
			$this->export->to_excel($sql, 'instant-win');
		} else {
			echo '<script>alert("Non ci sono vincite assegnate per questo concorso.");</script>';
		}

	}

	 public function importCSV()
        {
          if(isset($_POST["id"]))
            {
                $filename=$_FILES["file"]["tmp_name"];
                if($_FILES["file"]["size"] > 0)
                  {
                    $file = fopen($filename, "r");
                     while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
                     {
                            $data = array(
								'CAMPAIGN_ID' => $_POST['id'],
                                'GRUPPO' => $emapData[1],
                                'ORGANIZZAZIONE' => $emapData[2],
                                'INSEGNA' => $emapData[3],
                                'REGIONE' => $emapData[4],
                                'PROVINCIA' => $emapData[5],
                                'START_DATE' => $emapData[6],
                                'END_DATE' => $emapData[7],
                                );
                        $insertId = $this->Service_model->insertCSV($data);
                     }
                    fclose($file);
                  }
            }
        }

	function pippo(){
		$this->load->library('email');
		$this->email->from('no-reply@tantipremi.it', 'sender name');
		$this->email->to('swina.allen@gmail.com');
    	$this->email->subject('TEST FROM LINUX 7');
    	$this->email->message('This is a message test');
    	//if ($this->email->send())
        //	echo "Mail Sent!";
    	//else
        //	echo "There is error in sending mail!";
		/*
		$username = 'demo';
		$password  = MD5('demo');
		$sql = "SELECT users.* ,
			campaign_users.campaign_id
			FROM users
			INNER JOIN campaign_users ON users.id = campaign_users.user_id
			WHERE username = ? AND password = ?";
		$query = $this->db->query ( $sql , array ( $username , $password ) );
		$user = $query->result_array();
		$a = array ();
			foreach ( $user as $u ){
				array_push( $a , $u['campaign_id'] );
			}
		if ( in_array("1",$a) ){
			echo 'found';
		}
		print_r ( $a );
		print_r ( $_SESSION );
		*/
	}
}
