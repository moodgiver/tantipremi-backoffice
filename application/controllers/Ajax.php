<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Campaigns_model');
		$this->load->model('Campaign_model');
		$this->load->model('Service_model');
		$this->load->model('Instantwin_model');
		$this->load->helper('captcha');
		$this->load->config('meccanica');
	}

	public function index(){
    date_default_timezone_set("Europe/Rome");
		$action = $_POST['action'];
		switch ( $action ){

			case "login":
				$result = $this->Service_model->login($_POST);
				echo $result;
			break;


			case "concorsi":
				$data['campaigns'] = $this->Campaigns_model->campaigns();
				$this->load->view('concorsi',$data);
			break;

            case "list-province":
                $province = $this->Service_model->campaign_shops_states($_POST['id']);
                echo $this->render_province_campaign($province);
            break;

            case "list-shops":
                $shops = $this->Service_model->campaign_shops_by_state($_POST);
                echo $this->render_shops_options($shops);
            break;

            case "list-concorsi":
                $campaigns = $this->Campaigns_model->campaigns();
                echo  $this->render_concorsi_options($campaigns);
            break;

            case "premi-concorso":
                $premi = $this->Campaign_model->premi($_POST['id']);
            	echo $this->render_premi($premi);
            break;



			case "edit-concorso":
				//if ( file_exists ( FCPATH.'application/config/concorso_opzioni.php' ) ){

				//}
				$data['promo'] = $this->Campaigns_model->campaign($_POST['id']);
				$this->load->view('concorso_edit',$data);
			break;

			case "concorso-setting":
				$data['promo'] = $this->Campaigns_model->campaign($_POST['id']);
				$this->load->view('concorso_edit',$data);
			break;

			case "voucher-template":
				$template = $this->Campaign_model->prize_template($_POST['id']);
				echo $template;
			break;

			case "iscritti-concorso":

				$this->load->model('Campaign_model');
				if ( !isset ( $_POST['status'] ) ){

					$data['iscritti'] = $this->Campaign_model->campaign($_POST['id']);

					//$this->load->view('concorso_iscritti',$data);
					$this->load->view('giocate',$data);

				} else {

					$data['iscritti'] = $this->Campaign_model->campaign_filter($_POST);

					//$this->load->view('concorso_iscritti',$data);
					$this->load->view('giocate',$data);
				}
			break;

            case 'iscritti-concorso-filter':
                $this->load->model('Campaign_model');
                $data['iscritti'] = $this->Campaign_model->campaign_filter($_POST);
                $this->load->view('giocate',$data);
            break;

						case 'iscritti-concorso-estrai':
							$estrazione = true;
							$this->load->model('Campaign_model');
							$data['iscritti'] = $this->Campaign_model->campaign_estrai($_POST);
							$this->load->view('giocate',$data);

							break;

							case 'estratti-concorso':
								$estrazione = true;
								$this->load->model('Campaign_model');
								$data['iscritti'] = $this->Campaign_model->campaign_estratti($_POST);
								$this->load->view('giocate',$data);

								break;

						case 'conferma-estrazione':
								$this->load->model('Campaign_model');
								$this->Campaign_model->campaign_estratto($_POST);
								break;
						case 'premi-a-punti-concorso':
								$this->load->model('Campaign_model');
								$data['iscritti'] = $this->Campaign_model->campaign_premi_a_punti($_POST);
								$this->load->view('giocate',$data);
								break;

            case 'receipt-update':
                $res =  $this->Service_model->receipt_update($_POST);
                echo $res;
            break;

            case 'receipt-insert':
                $res = $this->Campaign_model->receipt_insert($_POST);
                echo $res;
            break;

						case 'receipt-checked':
                //$res = $this->Campaign_model->receipt_insert($_POST);
								$res = $_POST['id'].'-'.$_POST['checked'];
								$res = $this->Campaign_model->receipt_checked();
								echo $res;
            break;

						case 'estratto-checked':
                //$res = $this->Campaign_model->receipt_insert($_POST);
								$res = $_POST['id'].'-'.$_POST['checked'];
								$res = $this->Campaign_model->estratto_checked();
								echo $res;
            break;

            case 'delete-customer-receipt':
    			$res = $this->Campaign_model->receipt_delete($_POST);
	            echo $res;
						break;
						case 'pv-giocata':							$res = $this->Campaign_model->receipt_shop($_POST);							echo $res;						break;
            case 'send-coupon':
                $coupon = $this->Service_model->send_coupon($_POST);
                if ( $coupon != "error" && $coupon != "error-no-coupon" ){
                    $this->send_voucher($_POST,$coupon);
                } else {
                    echo $coupon;
                }
            break;

            case 'confirm-send-coupon':
                $this->confirm_send_voucher ( $_POST );
            break;

            case 'delete-send-coupon':
                $this->Service_model->revoke_coupon();
            break;

            case 'resend-coupon':
                $this->send_voucher($_POST,$_POST['coupon']);
            break;

            case 'confirm-resend-coupon':
                $this->confirm_resend_voucher ( $_POST );
            break;

            case 'revoke-user-coupon':
                $this->Service_model->revoke_user_coupon($_POST);
            break;

            case 'send-coupon-multi':
                $coupon = $this->Service_model->send_coupon_multi($_POST);
                if ( $coupon != "error" && $coupon != "error-no-coupon" ){
                    $this->send_voucher_multi($_POST,$coupon);
                } else {
                    echo $coupon;
                }
            break;

            case 'confirm-send-coupon-multi':
                $this->confirm_send_voucher_multi ( $_POST );
            break;

            case 'delete-send-coupon-multi':
                $this->Service_model->revoke_coupon_multi();
            break;

				case 'email-vincita':
					$rec = $this->Service_model->invia_mail_vincita( $_POST );
					$uri = 'http://www.tantipremi.it/public/img/'.$rec['code'].'/';

					if ( $rec['email_template'] ){
                $html = $rec['email_template'];
								$logo = $uri.'/'.$rec['thumbnail'];
								$html = str_replace('_logo_concorso_',$logo,$html);
								//$message = '<body><h2>Concorso '.$rec['campaign'].'</h2>';
								//$message .= 'Gentile Cliente <br>';
								//$message .= '<div style="background:url('.$uri.'/'.$rec['background_image'].') no-repeat center center;width:400px;max-width:400;"><img src="'.$uri.'/'.$rec['thumbnail'].'" style="width:400px;max-width:400px;"/></div><br><div>';
								//$message = $html.'</div></body>';
								$this->load->library('email'); // load email library
						    $this->email->from('no-reply@tantipremi.it', 'TANTIPREMI');
								$this->email->bcc('tantipremi@indual.it'); //sostituire tantipremi@indual.it
						    $this->email->to($_POST['email']); //sostituire $form['email']
						    $this->email->subject("Concorso ".$rec['campaign']);
								$this->email->message($html);
						    $sent = $this->email->send();

						     if ( $sent ){
									 		echo 'Mail inviata';
											$this->Service_model->vincita_email_inviata($_POST);
											/*if ( $this->Service_model->coupon_sent($form) ){
						     				return true;
						          } else {
						              return false;
						          }*/
						    	} else {
						        	echo 'Non e\' stato possibile inviare la mail';
									}
            } else {
                echo 'Non e\' stato possibile inviare la mail';
            }

				break;

        case "search-user":
				$this->load->model('Campaign_model');
				$data['iscritti'] = $this->Campaign_model->campaign_user_search($_POST);
				$this->load->view('concorso_iscritti',$data);
			break;

            case "registrazioni":
                $this->load->view('registrazioni');
            break;

            case "registrazioni-filtra":
                $this->load->model('Customers_model');
				$data['customers'] = $this->Customers_model->customer_search($_POST);
				$this->load->view('registrazioni',$data);
            break;


            case "customer-scontrini":
                $this->load->model('Campaign_model');
                $data['scontrini'] = $this->Campaign_model->campaign_user_filter($_POST['customer_id']);
								$this->load->view('customer_scontrini',$data);
                //echo $this->render_scontrini($scontrini);
            break;

            case "customer-edit":
                $this->load->model('Customers_model');
								$data['customer'] = $this->Customers_model->customer($_POST['id']);
                echo json_encode($data['customer']);
            break;

            case "customer-save":
							$this->customer_profile_update($_POST);
              //print_r ( $_POST );
            break;

			case "setting":

				$data['promo'] = $this->Campaigns_model->campaign($_POST['id']);
				$this->load->view('concorso_edit',$data);
			break;

			case "settings-save":
				$data['promo'] = $this->Campaign_model->save_settings($_POST);
			break;

			case "settings_color":
				 $this->Campaign_model->save_settings_colors($_POST);
			break;

			case "style":
				$data['promo'] = $this->Campaigns_model->campaign($_POST['id']);
				$this->load->view('concorso_style',$data);
			break;

			case "premi":
				$data['promo'] = $this->Campaigns_model->campaign($_POST['id']);
				$this->load->view('concorso_premi',$data);
			break;

			case "save-prize":
				$this->Campaign_model->save_prize($_POST);
			break;

			case "add-prize":
				$this->Campaign_model->add_prize($_POST);
			break;

			case "faq":
				$data['promo'] = $this->Campaigns_model->campaign($_POST['id']);
				$this->load->view('concorso_faq',$data);
			break;

			case "regolamento":
				$data['promo'] = $this->Campaigns_model->campaign($_POST['id']);
				$this->load->view('concorso_pdf',$data);
			break;

			case "shops-concorso":
				$data['shops'] = $this->Service_model->all_shops($_POST['id']);
				$this->load->view('concorso_shops',$data);
			break;

			case "contest-concorso":
				$data['shops'] = $this->Service_model->consest_shops($_POST['id']);
				$this->load->view('concorso_shops_contest',$data);
			break;

			case "instantwin-concorso":
				$data['id'] = $_POST['id'];
				$data['organizzazioni'] = $this->Instantwin_model->organizzazioni($_POST['id']);
				$data['instant_wins'] = $this->Instantwin_model->instant_wins($_POST['id']);
				$this->load->view('concorso_instant_win',$data);
			break;

			case "instantwin-detail":
				$data['instant_wins'] = $this->Instantwin_model->receipt();
				$this->load->view('instant_win_detail',$data);
			break;

			case 'add-instant-win':
				$added = $this->Instantwin_model->add_instant_wins($_POST);
				if ( $added ){
					echo 'added';
				}
				//print_r ( $_POST );
			break;

			case "contest-shop-foto":
				$data['shops'] = $this->Service_model->consest_foto($_POST['id']);
				if ( $data['shops'] ) {
					$this->load->view('concorso_shop_foto',$data);
				}
			break;

			case "create-coupons":
				$this->Service_model->create_coupons($_POST);

			break;

			case "export-coupons":
				$this->load->library('export');
				$sql = $this->Service_model->list_coupons($_POST);
				print_r ( $sql );
				if ( count ( $sql ) > 0 ){
					return $this->export->to_excel($sql, 'coupons');
				}
			break;

			case "utenti":
				$data['users'] = $this->Service_model->users('0');
				$this->load->view('utenti',$data);
			break;

            case "settings":
                $this->load->view('settings');
            break;
		}
	}

    public function render_concorsi_options( $concorsi ){
        $options = '';
        foreach ( $concorsi as $concorso ){
            if ( $concorso['active'] && $concorso['url'] == '' ){
                $options .= '<option value="'.$concorso['campaign_id'].'">'.$concorso['campaign'].'</option>';
            }
        }
        return $options;
    }

    public function render_province_campaign ( $province ){
        $options = '';
        foreach ( $province as $pv ){
            $options .= '<option value="'.$pv['provincia'].'">'.$pv['provincia'].'</option>';
        }
        return $options;
    }

    public function render_shops_options ( $shops ){
        $options = '';
        foreach ( $shops as $shop ){
            $options .= '<option value="'.$shop['insegna_id'].'">'.$shop['insegna'].'-'.$shop['indirizzo'].'('.$shop['comune'].')</option>';
        }
        return $options;
    }

    public function render_premi ( $premi ) {
        $options = '';
        foreach ( $premi as $premio ){
            $options .= '<option value="'.$premio['prize_id'].'">'.$premio['prize'].'</option>';
        }
        return $options;
    }

    public function render_scontrini ( $scontrini ){
        $hide = 'hide';
        if ( $_SESSION['login']['user_type'] != 'O' ){
            $hide = '';
        }
        $return = '';
        foreach ( $scontrini as $scontrino ){
			$ext = substr($scontrino['receipt'],-3);
			if ( $ext != 'pdf' ){
				$sc = '<span class="fa fa-picture-o pointer preview-receipt btn-action" data-controller="preview-doc" data-title="scontrino"  data-id="//www.tantipremi.it/public/users/upload/'.$scontrino['receipt'].'"></span>';
			} else {
				$sc = '<a href="//www.tantipremi.it/public/users/upload/'.$scontrino['receipt'].'" target="_blank"><span class="fa fa-file-pdf-o pointer"></span></a>';
			}
            if ( $scontrino['receipt_nr'] != '' ){
                $return .= '<tr>
                <td>'.$scontrino['receipt_nr'].'['.$scontrino['id'].']</td>
                <td>'.date("d/m/Y",strtotime($scontrino['receipt_date'])).'</td>
                <td>'.date("d/m/Y",strtotime($scontrino['register_date'])).'</td>
                <td>'.$scontrino['campaign'].'</td>
                <td>'.$scontrino['prize'].'</td>
                <td>'.$scontrino['insegna'].' ('.$scontrino['shop_state'].')<br>'.$scontrino['organizzazione'].'</td>
                <td>'.$sc.'</td>
                <td><button class="btn btn-sm btn-primary btn-action '.$hide.'" data-controller="edit-receipt" data-campaign="'.$scontrino['campaign_id'].'" data-originator="clienti"  data-receipt="'.$scontrino['id'].'" data-customer="'.$scontrino['firstname'].' '.$scontrino['lastname'].'" data-registration="'.date("d/m/Y",strtotime($scontrino['register_date'])).'" data-receipt_nr="'.$scontrino['receipt_nr'].'" data-receipt_date="'.date("d/m/Y",strtotime($scontrino['register_date'])).'" data-customer_id="'.$scontrino['customer_id'].'" data-image="'.$scontrino['receipt'].'" data-prize="'.$scontrino['prize_id'].'"><span class="fa fa-edit"></span></button>
				<input type="hidden" id="extra_'.$scontrino['id'].'" value="'.$scontrino['extra'].'">
                </td>
                </tr>';
            }
            //echo '<tr><td colspan="8 text-right"><button class="btn btn-sm btn-primary btn-action '.$hide.'" data-controller="add-receipt" data-customer="'.$scontrini[0]['firstname'].' '.$scontrini[0]['lastname'].'" data-registration="'.date("d/m/Y").'"  data-receipt_date="'.date("d/m/Y").'" data-customer_id="'.$scontrini[0]['customer_id'].'" ><span class="fa fa-plus"></span> Aggiungi</button></td></tr>';
        }
        return $return;

    }

    public function send_voucher( $form , $coupon ){
            if ( $coupon != "error-coupon-sent" ){
                $html = $this->Service_model->voucher_template();

                $content = str_replace('_codice_coupon_' , $coupon , $html);
                $content = str_replace('_scadenza_' , date('d-m-Y', strtotime("+90 days")), $content );
                echo $content;
            } else {
                echo $coupon;
            }
    }

    public function confirm_send_voucher ( $form ){

		$message = '<h2>Concorso '.$form['campaign'].'</h2>';
		//$message .= 'Gentile Cliente <br>';
		$message .= $form['voucher'];

		$this->load->library('email'); // load email library

    	$this->email->from('no-reply@tantipremi.it', 'TANTIPREMI');
		$this->email->bcc('tantipremi@indual.it'); //sostituire tantipremi@indual.it
    	$this->email->to($form['email']); //sostituire $form['email']
    	$this->email->subject("Concorso ".$form['campaign']);
		$this->email->message($message);
        $sent = $this->email->send();
        if ( $sent ){
            if ( $this->Service_model->coupon_sent($form) ){
     			return true;
            } else {
                return false;
            }
    	} else {
        	return false;
		}
    }

    public function confirm_send_voucher_multi ( $form ){
        $message = '<h2>Concorso '.$form['campaign'].'</h2>';

        //$message .= 'Gentile Cliente <br>';
        $message .= $form['voucher'];
        $this->load->library('email'); // load email library
        $this->email->from('no-reply@tantipremi.it', 'TANTIPREMI');
        $this->email->bcc('tantipremi@indual.it'); //sostituire tantipremi@indual.it
        $this->email->to($form['email']); //sostituire $form['email']
        $this->email->subject("Concorso ".$form['campaign']);
        $this->email->message($message);
        $sent = $this->email->send();
        $this->Service_model->coupon_multi_sent($form);
   }

    public function confirm_resend_voucher ( $form ){

		$message = '<h2>Concorso '.$form['campaign'].'</h2>';
		//$message .= 'Gentile Cliente <br>';
		$message .= $form['voucher'];

		$this->load->library('email'); // load email library

    	$this->email->from('no-reply@tantipremi.it', 'TANTIPREMI');
		$this->email->bcc('tantipremi@indual.it'); //sostituire tantipremi@indual.it
    	$this->email->to($form['email']); //sostituire $form['email']
    	$this->email->subject("Concorso ".$form['campaign']);
		$this->email->message($message);
        $sent = $this->email->send();
        if ( $sent ){
   			return true;
    	} else {
            return false;
       }
    }
	public function send_voucher_multi( $form , $coupon ){
		if ( $coupon != "error-coupon-sent" ){
			$html = $this->Service_model->voucher_template($form);
			$content = str_replace('_codice_coupon_' , $coupon , $html);
			if ( strpos ( $content , '_scadenza_' ) ){
				$content = str_replace('_scadenza_' , date('d-m-Y', strtotime("+90 days")), $content );
			}
			echo $content;
		} else {
			echo $coupon;
		}
	}
	public function concorsi()
	{
		echo 'CONCORSI';
		//$data['campaigns'] = $this->Campaigns_model->campaigns();
		//$this->load->view('admin',$data);
	}

	public function customer_profile_update ( $form ){
		//create date
		date_default_timezone_set('Europe/Rome');
		$day = (int) substr($form['dob'], 0, 2);
   		$month = (int) substr($form['dob'], 3, 2);
   		$year = (int) substr($form['dob'], 6, 4);
		$birth = $year.'/'.$month.'/'.$day;

		//encrypt password if modified
    $pwd = false;
    if ( $form['password'] != '' ){
    	$pwd = MD5($form['password']);
    }
		$data = array(
	        'firstname'		=> $form['firstname'],
	        'lastname' 		=> $form['lastname'],
					'dob' 				=> $birth,
					'cf'					=> $form['cf'],
					'address_1'		=> $form['address_1'],
					'address_nr'	=> $form['address_nr'],
					'city'				=> $form['city'],
					'state'				=> $form['state'],
					'zip'					=> $form['zip'],
					'phone'				=> $form['phone'],
					'mobile'			=> $form['mobile'],
					'email'				=> $form['email'],
					'registration_date' => date('Y-m-d H:i:s'),
					'update_date' 	=> date('Y-m-d H:i:s')
		);
    if ( $pwd ){
    	$data['password']	= $pwd;
			echo $data['password'];
    }
    $this->db->where ( 'customer_id' , $form['id'] );
		if ( ! $this->db->update('customers', $data) ){
			$error = $this->db->error(); // Has keys 'code' and 'message'
			echo '<script>
			        alert("'.$error['code'].'");
		    </script>';
		} else {
    		return true;
		}
	}
}
