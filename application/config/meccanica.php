<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['opzioni'] = array (
  array('send_user_authentication','Invia dati registrazione',1,null),
  array('no_upload_identita','Richiedi documento identit&agrave;',0,null),
  array('estrazione','Estrazione',1,null),
  array('scelta_premio','Scelta del premio',1,null),
  array('selezione_prodotti','Selezione dei prodotti',1,null),
  array('importo_minimo_prodotti','Importo dei prodotti',1,5),
  array('quantita_minima_prodotti','Quantita dei prodotti',0,2),
  array('voucher_coupon','Voucher coupon',0,null),
  array('email_vincita','Email vincita',1,null)
);
