<?php

class Instantwin_Model extends CI_Model {

	function __construct(){
		parent::__construct();

	}

  function organizzazioni($id){
    $sql = "SELECT * FROM shops WHERE campaign_id=? GROUP BY organizzazione";
    $query = $this->db->query ( $sql , $id );
    if ( count($query->result_array()) != 0  ){
      return($query->result_array());
    } else {
      return false;
      //'Nessuna organizzazione definita per l\'Instant Win';
    }
  }

  function instant_wins($id){
    $sql = "SELECT
      campaigns_istant_win.* ,
      shops.organizzazione
      FROM campaigns_istant_win
      LEFT JOIN shops ON campaigns_istant_win.organizzazione = shops.organizzazione
      WHERE campaigns_istant_win.campaign_id = ?
      GROUP BY instant_win_id
      ORDER BY date_string ASC";
    $query = $this->db->query ( $sql , $id );
  	if ( count($query->result_array()) != 0  ){
      return($query->result_array());
    } else {
      return false;
      //'Nessuna organizzazione definita per l\'Instant Win';
    }
  }

  function add_instant_wins($form){
    $day = (int) substr($form['from'], 0, 2);
    $month = (int) substr($form['from'], 3, 2);
    $year = (int) substr($form['from'], 6, 4);
    $s = $year.'/'.$month.'/'.$day;
    $fromDate = date('Y-m-d', strtotime($s));
    $day = (int) substr($form['to'], 0, 2);
    $month = (int) substr($form['to'], 3, 2);
    $year = (int) substr($form['to'], 6, 4);
    $s = $year.'/'.$month.'/'.$day;
    $toDate = date('Y-m-d',strtotime($s));

    for ( $n=0 ; $n < (int)$form['qty'] ; $n++ ){
      $rand_date = $this->random_date($fromDate,$toDate);
      $data = array (
        "campaign_id"     => $form['campaign_id'],
        "organizzazione"  => $form['organizzazione'],
        "date_string"     => (string)date('Y-m-d' ,$rand_date)
      );
      //print_r ( $data );
      $this->db->insert ( 'campaigns_istant_win' , $data );
      /*
      if ( $this->db->insert ( 'campaigns_istant_win' , $data ) ){
				return true;
			} else {
				return false;
			}
      */
    }
  }

  public function receipt(){
  	 $sql = "Select
customers.customer_id,
customers.firstname,
customers.lastname,
customers.dob,
customers.address_1,
customers.address_nr,
customers.zip,
customers.city,
customers.state,
customers.country,
customers.phone,
customers.mobile,
customers.email,
customers.id_image,
campaign_prizes.prize,
campaign_prizes.prize_image,
customers_campaigns.id ,
customers_campaigns.campaign_id,
customers_campaigns.register_date,
customers_campaigns.receipt,
customers_campaigns.receipt_date,
customers_campaigns.receipt_nr,
customers_campaigns.status,
customers_campaigns.prize_id,
customers_campaigns.extra,
customers_campaigns.shop_id,
customers_campaigns.estratto,
shops.organizzazione,
shops.insegna,
shops.provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state,
campaigns.register_days,
campaigns.campaign,
campaigns.logic,
campaign_coupons.coupon,
campaign_coupons.released_date,
campaign_coupons.mail_sent,
coupons.coupon_id,
coupons.coupon as multi_coupon,
coupons.mail_sent as multi_coupon_sent,
coupons.release_date as multi_coupon_date
From
customers_campaigns
Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
WHERE
customers_campaigns.receipt = ?
GROUP BY customers_campaigns.id";
    $query = $this->db->query ( $sql , $_POST['uuid'] );
  	if ( $query ){
      return($query->result_array());
    } else {
      return false;
      //'Nessuna organizzazione definita per l\'Instant Win';
    }
  }

	function vincite($id){
		$sql = "SELECT
			campaigns.campaign AS CONCORSO,
			instant_win_id AS 'VINCITA_ID',
			id AS 'GIOCATA_ID',
			receipt_nr AS 'SCONTRINO_NR',
			campaigns_istant_win.receipt_uuid AS 'SCONTRINO_UNIVOCO',
			campaigns_istant_win.date_string 'DATA_VINCITA',
			campaigns_istant_win.date_assigned AS 'DATA_ESATTA_VINCITA',
			IF ( customers_campaigns.status <> 1 ,
				IF ( customers_campaigns.status = 6 , 'ACCETTATO' ,
					IF ( customers_campaigns.status = 4 , 'NON VALIDO' , '' )
				),'IN VERIFICA')
			 AS ACCETTAZIONE,
			status AS 'STATO_SCONTRINO',
			campaigns_istant_win.is_assigned,
			customers.customer_id,
			UPPER(customers.firstname) AS NOME,
			UPPER(customers.lastname) AS COGNOME,
			customers.email AS EMAIL,
			UPPER(shops.organizzazione) AS ORGANIZZAZIONE,
			UPPER(shops.insegna) AS INSEGNA,
			UPPER(shops.indirizzo) AS INDIRIZZO,
			UPPER(shops.comune) AS COMUNE,
			UPPER(shops.provincia) AS PV
			From
			campaigns_istant_win
			LEFT Join customers_campaigns ON campaigns_istant_win.receipt_uuid = customers_campaigns.receipt
			LEFT Join customers ON customers_campaigns.customer_id = customers.customer_id
			LEFT JOIN shops ON customers_campaigns.shop_id = shops.insegna_id
			INNER JOIN campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
			Where
			campaigns_istant_win.campaign_id = ?
			GROUP BY receipt_uuid
			ORDER BY instant_win_id";
			return( $this->db->query ( $sql , $id ) );
	}


  function random_date($from,$to){
    $rand_date = rand(strtotime($from), strtotime($to));
    return $rand_date;
  }
}
