<?php

class Campaigns_Model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		
	}
	function campaigns(){
		$sql = "SELECT campaigns.* , campaigns.active AS is_active , campaign_asset.* FROM `campaigns` INNER JOIN campaign_asset ON campaigns.campaign_id = campaign_asset.campaign_id";
		$query = $this->db->query($sql);
    	return($query->result_array());
		
	}	
	
	function campaign($id){
		$sql = "
		SELECT campaigns.* 	, 
		campaigns.active AS isActive,
		campaign_asset.* 	,
		campaign_prizes.*	
		FROM `campaigns` 
		INNER JOIN campaign_asset ON campaigns.campaign_id = campaign_asset.campaign_id 
		INNER JOIN campaign_prizes ON campaigns.campaign_id = campaign_prizes.campaign_id
		WHERE campaigns.campaign_id = ?";
		$query = $this->db->query($sql,$id);
    	return($query->result_array());
		
	}	
		
}	