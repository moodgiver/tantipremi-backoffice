<?php

class Customers_Model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		
	}
	
	function login ( $form ){
	
		$this->load->library('uuid');
			
		$e = $form['email'];
		$p = MD5($form['password']);
		$sql = "SELECT * FROM customers WHERE email=? AND password = ?";
		
		$query=$this->db->query($sql,array($e,$p));
		if ( $query->num_rows() == 0 ){
			$session_data = array (
				'login' => false
			);
			$this->session->set_userdata('customer', $session_data);
			return false;
		} else {
			$login = $query->result_array() ;
			$session_data = array (
				'login' => true,
				'email' => $login[0]['email'],
				'customer' => $login[0]['firstname'].' '.$login[0]['lastname'],
				'customer_id' => $login[0]['customer_id'],
				'uuid' => $this->uuid->v4()
			);
			$this->session->set_userdata('customer', $session_data);
			return true;
		}
	}
	
	function customer($id){
		$sql = "SELECT * , 
                DATE_FORMAT(dob,'%d/%m/%Y') AS data_nascita ,
                DATE_FORMAT(registration_date,'%d/%m/%Y') AS data_registrazione 
                FROM customers WHERE customer_id = ?";
		$query=$this->db->query($sql,$id);
		return($query->result_array());
	}
    
    function customer_search($form){
		$sql = "SELECT * FROM customers WHERE lastname LIKE '%".$form['search']."%' OR email LIKE '%".$form['search']."%' ORDER BY lastname, firstname, email";
		$query=$this->db->query($sql);
        
		return($query->result_array());
	}
	
	function register($form){
		//create date
		date_default_timezone_set('Europe/Rome'); 
		$day = (int) substr($form['dob'], 0, 2);
   		$month = (int) substr($form['dob'], 3, 2);
   		$year = (int) substr($form['dob'], 6, 4);
		$birth = $year.'-'.$month.'-'.$day;
		$date= date_create($birth);
		
		//encrypt password
		$pwd = MD5($form['password']);
		$data = array(
	        'campaign_id' 	=> $form['campaign_id'],
	        'firstname'		=> $form['nome'],
	        'lastname' 		=> $form['cognome'],
			'dob' 			=> date_format($date,"Y/m/d H:i:s"),
			'cf'			=> $form['cf'],
			'gender'		=> $form['sesso'],
			'address_1'		=> $form['indirizzo'],
			'address_nr'	=> $form['civico'],
			'destination'	=> $form['destinazione'],
			'door_name'		=> $form['citofono'],
			'address_2'		=> $form['presso'],
			'city'			=> $form['comune'],
			'village'		=> $form['frazione'],
			'state'			=> $form['pv'],
			'zip'			=> $form['cap'],
			'country'		=> 'IT',
			'phone'			=> $form['telefono'],
			'mobile'		=> $form['mobile'],
			'email'			=> $form['email'],
			'password'		=> $pwd,
			'registration_date' => date('Y-m-d H:i:s'),
			'update_date' 	=> date('Y-m-d H:i:s')
		);
		if ( ! $this->db->insert('customers', $data) ){
			$error = $this->db->error(); // Has keys 'code' and 'message'
			if ( $error['code'] == '1062' ){
				echo '<script>
					alert("'.$form['email'].' e\' gia\' registrato. Effettuare il login");
					window.history.back();
					</script>';
				
			}
		} else {
				
				$sql = "SELECT * FROM customers WHERE email=? AND password = ?";
				$query=$this->db->query($sql,array($form['email'],$pwd));
				$login = $query->result_array() ;
				$session_data = array (
					'login' => true,
					'email' => $login[0]['email'],
					'customer' => $login[0]['firstname'].' '.$login[0]['lastname'],
					'customer_id' => $login[0]['customer_id']
				);
				$this->session->set_userdata('customer', $session_data);
				return true;
		}
	}	
    
    function customer_save($form){
		//create date
		date_default_timezone_set('Europe/Rome'); 
		$day = (int) substr($form['dob'], 0, 2);
   		$month = (int) substr($form['dob'], 3, 2);
   		$year = (int) substr($form['dob'], 6, 4);
		$birth = $year.'/'.$month.'/'.$day;
		
		//encrypt password if modified
        $pwd = false;
        if ( $form['password'] != '' ){
    		$pwd = MD5($form['password']);
        }
		$data = array(
	        'firstname'		=> $form['firstname'],
	        'lastname' 		=> $form['lastname'],
			'dob' 			=> $birth,
			'cf'			=> $form['cf'],
			'address_1'		=> $form['address_1'],
			'address_nr'	=> $form['address_nr'],
			'city'			=> $form['city'],
			'state'			=> $form['state'],
			'zip'			=> $form['zip'],
			'phone'			=> $form['phone'],
			'mobile'		=> $form['mobile'],
			'email'			=> $form['email'],
			'registration_date' => date('Y-m-d H:i:s'),
			'update_date' 	=> date('Y-m-d H:i:s')
		);
        if ( $pwd ){
    		//	$data.pu'password'	=> $pwd,
        }
        $this->db->where ( 'customer_id' , $form['id'] );
		if ( ! $this->db->update('customers', $data) ){
			$error = $this->db->error(); // Has keys 'code' and 'message'
			echo '<script>
			        alert("'.$error['code'].'");
		    </script>';
		} else {
    		return true;
		}
	}	
	
	function save_profile($form){
		//create date
		date_default_timezone_set('Europe/Rome'); 
		$day = (int) substr($form['dob'], 0, 2);
   		$month = (int) substr($form['dob'], 3, 2);
   		$year = (int) substr($form['dob'], 6, 4);
		$birth = $year.'-'.$month.'-'.$day;
		$date= date_create($birth);
		$data = array(
	        'firstname'		=> $form['nome'],
	        'lastname' 		=> $form['cognome'],
			'dob' 			=> date_format($date,"Y/m/d H:i:s"),
			'cf'			=> $form['cf'],
			'gender'		=> $form['sesso'],
			'address_1'		=> $form['indirizzo'],
			'address_nr'	=> $form['civico'],
			'destination'	=> $form['destinazione'],
			'door_name'		=> $form['citofono'],
			'address_2'		=> $form['presso'],
			'city'			=> $form['comune'],
			'village'		=> $form['frazione'],
			'state'			=> $form['pv'],
			'zip'			=> $form['cap'],
			'country'		=> 'IT',
			'phone'			=> $form['telefono'],
			'mobile'		=> $form['mobile'],
			'update_date'	=> date('Y-m-d H:i:s')
		);
		
		//$data = $this->security->xss_clean($data);
		
		if ( ! $this->db->update('customers', $data) ){
			$error = $this->db->error(); // Has keys 'code' and 'message'
			if ( $error['code'] == '1062' ){
				echo '<script>
					alert("'.$form['email'].' e\' gia\' registrato. Effettuare il login");
					window.history.back();
					</script>';
				
			}
		}
		
	}	
	
	function customer_campaign(){
	$sql = "Select
campaigns.campaign,
customers.customer_id,
customers.campaign_id,
customers.firstname,
customers.lastname,
customers.address_1,
customers.address_2,
customers.address_nr,
customers.door_name,
customers.village,
customers.city,
customers.zip,
customers.state,
customers.country,
customers.phone,
customers.mobile,
customers.registration_date,
customers.update_date,
customers.active,
customers.email,
shops.organizzazione,
shops.insegna,
shops.provincia,
shops.start_date,
shops.end_date,
campaign_prizes.prize_id,
campaign_prizes.prize,
campaign_prizes.prize_description,
campaign_prizes.prize_image,
customers_campaigns.register_date,
customers_campaigns.receipt_date,
customers_campaigns.receipt_amount,
customers_campaigns.receipt_nr,
customers_campaigns.shop_state,
customers_campaigns.`status`,
customers_receipts.`file`,
customers_receipts.image,
customers_receipts.doc
From
customers_campaigns
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
Inner Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
Inner Join campaigns ON campaigns.campaign_id = customers_campaigns.campaign_id
Inner Join shops ON customers_campaigns.shop_id = shops.insegna_id
Inner Join customers_receipts ON customers.customer_id = customers_receipts.customer_id AND customers_campaigns.campaign_id = customers_receipts.campaign_id
WHERE customers_campaigns.customer_id = ? AND customers_campaigns.campaign_id = ?";
			$params = array ( 
			intval($_SESSION['customer']['customer_id']),
			intval($_SESSION['promo_id'])
		);
		
		$query=$this->db->query($sql,$params);
		if ( $query->num_rows() < 0 ){
			return false;
		} else {
			return($query->result_array());
		}
	}
	
	function receipt ( $params ){
		/*$delete = array (
			$_SESSION['promo_id'],
			$_SESSION['customer']['customer_id'],
			$params['type']
		);
		
		$sql = "DELETE FROM customers_receipts WHERE campaign_id = ? AND customer_id = ? AND type = ?";
		$query=$this->db->query($sql,$delete);
		*/
		
		$sql = "SELECT * FROM customers_receipts WHERE campaign_id = ? AND customer_id = ?";
		$query= $this->db->query($sql,array($_SESSION['promo_id'],$_SESSION['customer']['customer_id']));
		if ( $query->num_rows() == 0 ){
			$data = array ( 
				'campaign_id' 	=> $_SESSION['promo_id'],
				'customer_id' 	=> $_SESSION['customer']['customer_id'],
				'file'			=> $params['file'],
				'type'			=> $params['type'],
				'image'			=> $params['image']
			);
			$this->session->set_userdata('upload_'.$params['type'], $params['file']);
			if ( ! $this->db->insert('customers_receipts', $data) ){
				$error = $this->db->error(); // Has keys 'code' and 'message'
				if ( $error['code'] == '1062' ){
					echo '<script>
							alert("Errore! Si prega di contattare l\'assistenza tecnica.");
						</script>';
				
				}
			}
		} else {
		
			$row = $query->row();
			$uuid = $row->uuid;
			if ( $params['type'] == '1' ){
				$data = array ( 
					'file'	=> $params['file'],
					'image' => $params['image']
				);
				$this->db->set(	$data );
				$this->db->where ( 'uuid' , $uuid );
				$this->db->update ( 'customers_receipts' );
			} else {
				$this->db->set ( "doc" , $params['file'] );
				$this->db->where ( 'uuid' , $uuid );
				$this->db->update ( 'customers_receipts' );
			}
		}
	}
	
	function save_promo ( $form ){
		date_default_timezone_set('Europe/Rome'); 
		$day = (int) substr($form['data-scontrino'], 0, 2);
   		$month = (int) substr($form['data-scontrino'], 3, 2);
   		$year = (int) substr($form['data-scontrino'], 6, 2);
		$hour = $form['ora-scontrino'];
		$min = $form['min-scontrino'];
		$date = $year.'-'.$month.'-'.$day.' '.$hour.':'.$min.':00';
		$importo = $amount = substr($form['importo-scontrino'],0,-2).'.'.substr($form['importo-scontrino'],-2);
		$delete = array (
			$_SESSION['promo_id'],
			$_SESSION['customer']['customer_id'],
			
		);
		$sql = "DELETE FROM customers_campaigns WHERE campaign_id = ? AND customer_id = ?";
		$query=$this->db->query($sql,$delete);		

		$data = array (
			'campaign_id' 	=> $_SESSION['promo_id'],
			'customer_id' 	=> $_SESSION['customer']['customer_id'],
			'prize_id'		=> $form['prize_id'],
			'register_date' => date('Y-m-d H:i:s'),
			'update_date'	=> date('Y-m-d H:i:s'),
			'shipment_date'	=> date('Y-m-d H:i:s'),
			'receipt_date'	=> $date,
			'receipt_amount'=> $importo,
			'receipt_nr'	=> $form['nr-scontrino'],
			'shop_id'		=> $form['shop_id'],
			'shop_state'	=> $form['shop_state'],
			'status'		=> 1
		);
		$data = $this->security->xss_clean($data);
		//print_r ( $data );
		if ( ! $this->db->insert('customers_campaigns', $data) ){
			$error = $this->db->error(); // Has keys 'code' and 'message'
			if ( $error['code'] == '1062' ){
				echo '<script>
					alert("Errore! Si prega di contattare l\'assistenza tecnica.");
					</script>';
				
			}
		};
	}
	
	function update_customer_campaign_status($params){
		$data = array ( 
					'status'	=> $params['nextstatus'],
				);
		$this->db->set(	$data );
		$this->db->where ( 'id' , $params['id'] );
		$this->db->update ( 'customers_campaigns' );
		
	}
	
	function view_receipt ( $id ){
		$sql = "SELECT image FROM customers_receipts WHERE receipt_id = ?";
		$query=$this->db->query($sql,$id);
		return($query->result_array(0));
	}
	
	
}	