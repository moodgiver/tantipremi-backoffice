<?php

class Campaign_Model extends CI_Model {

	function __construct(){
		parent::__construct();

	}

	function campaign($id){
        if ( !isSet ( $from_date ) ){
            $date = new DateTime();
            $to_date = $date->format('Y-m-d');
            $date->modify('-7 days');
            $from_date = $date->format('Y-m-d');
        }
		$sql = "Select
customers.customer_id,
customers.firstname,
customers.lastname,
customers.dob,
customers.address_1,
customers.address_nr,
customers.zip,
customers.city,
customers.state,
customers.country,
customers.phone,
customers.mobile,
customers.email,
customers.id_image,
campaign_prizes.prize,
campaign_prizes.prize_image,
customers_campaigns.id ,
customers_campaigns.campaign_id,
customers_campaigns.register_date,
customers_campaigns.receipt,
customers_campaigns.receipt_date,
customers_campaigns.receipt_nr,
customers_campaigns.receipt_checked,
customers_campaigns.estratto_checked,
customers_campaigns.status,
customers_campaigns.prize_id,
customers_campaigns.extra,
customers_campaigns.shop_id,
customers_campaigns.estratto,
shops.organizzazione,
shops.insegna,
shops.provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state,
campaigns.register_days,
campaigns.campaign,
campaigns.logic,
campaigns.invia_mail,
campaign_coupons.coupon,
campaign_coupons.released_date,
campaign_coupons.mail_sent,
coupons.coupon_id,
coupons.coupon as multi_coupon,
coupons.mail_sent as multi_coupon_sent,
coupons.release_date as multi_coupon_date

From
customers_campaigns
Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id AND customers_campaigns.shop_state = shops.provincia
LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
WHERE
customers_campaigns.campaign_id = ?
AND ( DATE(customers_campaigns.register_date)  BETWEEN ? AND ? )
GROUP BY customers_campaigns.id";

		$query = $this->db->query($sql,array($id,$from_date,$to_date));
   		return($query->result_array());
	}


	function campaign_export($id){
		$sql = "Select
customers.customer_id AS id,
customers.firstname AS COGNOME,
customers.lastname AS NOME,
customers.dob AS DATA_NASCITA,
customers.address_1 AS INDIRIZZO,
customers.address_nr AS NR_CIVICO,
customers.zip AS CAP,
customers.city AS COMUNE,
customers.state AS PV,
customers.country AS STATO,
customers.phone AS TELEFONO,
customers.mobile AS MOBILE,
customers.email AS EMAIL,
campaign_prizes.prize AS PREMIO,
customers_campaigns.register_date AS DATA_REGISTRAZIONE,
customers_campaigns.receipt_date AS DATA_SCONTRINO,
customers_campaigns.receipt_nr AS SCONTRINO_NR,
customers_campaigns.receipt_checked,
customers_campaigns.estratto_checked,
customers_campaigns.status AS STATO,
customers_campaigns.extra AS EXTRA,
customers_campaigns.shop_id,
customers_campaigns.estratto,
shops.organizzazione AS PV_ORGANIZZAZIONE,
shops.insegna AS INSEGNA,
shops.provincia AS provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state AS PROV_INSEGNA,
campaigns.campaign AS PROMOZIONE,
campaigns.invia_mail,
campaign_coupons.coupon AS COUPON,
campaign_coupons.released_date AS DATA_COUPON,
campaign_coupons.mail_sent AS COUPON_INVIATO,
campaign_coupons.coupon,
campaign_coupons.released_date,
campaign_coupons.mail_sent,
coupons.coupon_id,
coupons.coupon as multi_coupon,
coupons.mail_sent as multi_coupon_sent,
coupons.release_date as multi_coupon_date
From
customers_campaigns
Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
WHERE
customers_campaigns.campaign_id = ?
GROUP BY customers_campaigns.id
ORDER BY customers.customer_id";
	//AND customers_campaigns.shop_state = shops.provincia
		$query = $this->db->query($sql,$id);
   		return $query;
	}

	function campaign_filter($form){

      $day = (int) substr($form['from'], 0, 2);
   		$month = (int) substr($form['from'], 3, 2);
   		$year = (int) substr($form['from'], 6, 4);
			$dateS = $year.'-'.$month.'-'.$day;
			//$dateS= date_create($datestart);

			$day = (int) substr($form['to'], 0, 2);
   		$month = (int) substr($form['to'], 3, 2);
   		$year = (int) substr($form['to'], 6, 4);
			$dateE = $year.'-'.$month.'-'.$day;
			//$dateE= date_create($dateend);

        if ( $form['status'] != '' && $form['status'] != 'estratto_checked'){

		$sql = "Select
customers.customer_id,
customers.firstname,
customers.lastname,
customers.dob,
customers.address_1,
customers.address_nr,
customers.zip,
customers.city,
customers.state,
customers.country,
customers.phone,
customers.mobile,
customers.email,
customers.id_image,
campaign_prizes.prize,
campaign_prizes.prize_image,
customers_campaigns.id ,
customers_campaigns.campaign_id,
customers_campaigns.register_date,
customers_campaigns.receipt,
customers_campaigns.receipt_date,
customers_campaigns.receipt_nr,
customers_campaigns.receipt_checked,
customers_campaigns.estratto_checked,
customers_campaigns.status,
customers_campaigns.prize_id,
customers_campaigns.extra,
customers_campaigns.shop_id,
customers_campaigns.estratto,
shops.organizzazione,
shops.insegna,
shops.provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state,
campaigns.register_days,
campaigns.campaign,
campaigns.logic,
campaigns.invia_mail,
campaign_coupons.coupon,
campaign_coupons.released_date,
campaign_coupons.mail_sent,
campaign_coupons.coupon,
campaign_coupons.released_date,
campaign_coupons.mail_sent,
coupons.coupon_id,
coupons.coupon as multi_coupon,
coupons.mail_sent as multi_coupon_sent,
coupons.release_date as multi_coupon_date
From
customers_campaigns
Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
WHERE
customers_campaigns.campaign_id = ?
AND ( DATE(customers_campaigns.register_date)  BETWEEN ? AND ? )
AND customers_campaigns.status = ?";
            $query = $this->db->query($sql,array($form['id'],$dateS,$dateE,$form['status']));
            return($query->result_array());
        } else {
            $sql = "Select
customers.customer_id,
customers.firstname,
customers.lastname,
customers.dob,
customers.address_1,
customers.address_nr,
customers.zip,
customers.city,
customers.state,
customers.country,
customers.phone,
customers.mobile,
customers.email,
customers.id_image,
campaign_prizes.prize,
campaign_prizes.prize_image,
customers_campaigns.id ,
customers_campaigns.campaign_id,
customers_campaigns.register_date,
customers_campaigns.receipt,
customers_campaigns.receipt_date,
customers_campaigns.receipt_nr,
customers_campaigns.receipt_checked,
customers_campaigns.estratto_checked,
customers_campaigns.status,
customers_campaigns.prize_id,
customers_campaigns.extra,
customers_campaigns.shop_id,
customers_campaigns.estratto,
shops.organizzazione,
shops.insegna,
shops.provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state,
campaigns.register_days,
campaigns.campaign,
campaigns.logic,
campaigns.invia_mail,
campaign_coupons.coupon,
campaign_coupons.released_date,
campaign_coupons.mail_sent,
coupons.coupon_id,
coupons.coupon as multi_coupon,
coupons.mail_sent as multi_coupon_sent,
coupons.release_date as multi_coupon_date
From
customers_campaigns
Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
WHERE
customers_campaigns.campaign_id = ?
AND customers_campaigns.estratto_checked > ?
AND ( DATE(customers_campaigns.register_date)  BETWEEN ? AND ? )
GROUP BY customers_campaigns.id";
						$ec = -1;
						if ( $form['status'] == 'estratto_checked' ){
							$ec = 0;
						}
            $query = $this->db->query($sql,array($form['id'],$ec,$dateS,$dateE));
            return($query->result_array());
        }

	}

    function campaign_user_filter ( $id ){
        $sql = "Select
customers.customer_id,
customers.firstname,
customers.lastname,
customers.dob,
customers.address_1,
customers.address_nr,
customers.zip,
customers.city,
customers.state,
customers.country,
customers.phone,
customers.mobile,
customers.email,
customers.id_image,
campaign_prizes.prize,
campaign_prizes.prize_image,
customers_campaigns.id ,
customers_campaigns.campaign_id,
customers_campaigns.register_date,
customers_campaigns.receipt,
customers_campaigns.receipt_date,
customers_campaigns.receipt_nr,
customers_campaigns.receipt_checked,
customers_campaigns.estratto_checked,
customers_campaigns.status,
customers_campaigns.prize_id,
customers_campaigns.extra,
customers_campaigns.shop_id,
customers_campaigns.estratto,
shops.organizzazione,
shops.insegna,
shops.provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state,
campaigns.register_days,
campaigns.campaign,
campaigns.invia_mail,
coupons.coupon,
coupons.link,
coupons.release_date,
coupons.mail_sent
From
customers_campaigns
Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
WHERE
customers.customer_id = ?";
            $query = $this->db->query($sql,$id);
            return($query->result_array());
    }

	function campaign_export_filter($id,$status){
		$sql = "Select
customers.customer_id AS id,
customers.firstname AS COGNOME,
customers.lastname AS NOME,
customers.dob AS DATA_NASCITA,
customers.address_1 AS INDIRIZZO,
customers.address_nr AS NR_CIVICO,
customers.zip AS CAP,
customers.city AS COMUNE,
customers.state AS PV,
customers.country AS STATO,
customers.phone AS TELEFONO,
customers.mobile AS MOBILE,
customers.email AS EMAIL,
campaign_prizes.prize AS PREMIO,
customers_campaigns.register_date AS DATA_REGISTRAZIONE,
customers_campaigns.receipt_date AS DATA_SCONTRINO,
customers_campaigns.receipt_nr AS SCONTRINO_NR,
customers_campaigns.receipt_checked,
customers_campaigns.estratto_checked,
customers_campaigns.status AS STATO,
customers_campaigns.extra AS EXTRA,
customers_campaigns.shop_id,
customers_campaigns.estratto,
shops.organizzazione AS PV_ORGANIZZAZIONE,
shops.insegna AS INSEGNA,
shops.provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state AS PROV_INSEGNA,
campaigns.campaign AS PROMOZIONE,
campaigns.invia_mail,
campaign_coupons.coupon AS COUPON,
campaign_coupons.released_date AS DATA_COUPON,
campaign_coupons.mail_sent AS COUPON_INVIATO,
coupons.coupon_id,
coupons.coupon as multi_coupon,
coupons.mail_sent as multi_coupon_sent,
coupons.release_date as multi_coupon_date
From
customers_campaigns
Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
WHERE
customers_campaigns.campaign_id = ? AND customers_campaigns.status = ?
GROUP BY customers_campaigns.id
ORDER BY customers.customer_id";
		$query = $this->db->query($sql,array ($id,$status));
   		return $query;
	}


	function campaign_user_search($form){
		$search = $form['search'];
		$sql = 'Select
customers.customer_id,
customers.firstname,
customers.lastname,
customers.dob,
customers.address_1,
customers.address_nr,
customers.zip,
customers.city,
customers.state,
customers.country,
customers.phone,
customers.mobile,
customers.email,
campaign_prizes.prize,
campaign_prizes.prize_image,
customers_campaigns.id ,
customers_campaigns.campaign_id,
customers_campaigns.register_date,
customers_campaigns.receipt_date,
customers_campaigns.receipt_nr,
customers_campaigns.receipt_checked,
customers_campaigns.estratto_checked,
customers_campaigns.status,
customers_campaigns.extra,
customers_campaigns.shop_id,
customers_campaigns.estratto,
customers_receipts.receipt_id,
customers_receipts.image,
customers_receipts.file,
customers_receipts.doc,
shops.organizzazione,
shops.insegna,
shops.provincia,
shops.indirizzo,
shops.start_date,
shops.end_date,
customers_campaigns.shop_state
From
customers_campaigns
Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
Inner Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
Inner Join shops ON customers_campaigns.shop_id = shops.insegna_id
Inner Join customers_receipts ON customers_campaigns.campaign_id = customers_receipts.campaign_id AND customers.customer_id = customers_receipts.customer_id
WHERE customers_campaigns.campaign_id = '.$form["id"].' AND
(customers.firstname LIKE "'.$search.'%" OR customers.lastname LIKE "'.$search.'%")
ORDER BY customers_campaigns.register_date DESC
';

		/*$this->db->select($sql);
		$this->db->where('customers_campaigns.campaign_id' , $form['id'] );
		$this->db->like('customers.firstname' , $search , 'after' );
		$this->db->or_like ( 'customers.lastname' , $search , 'after');
		*/
		$query = $this->db->query($sql);
   		return($query->result_array());
	}

	function campaign_estrai($form){

	      $day = (int) substr($form['from'], 0, 2);
	   		$month = (int) substr($form['from'], 3, 2);
	   		$year = (int) substr($form['from'], 6, 4);
				$dateS = $year.'-'.$month.'-'.$day;
				//$dateS= date_create($datestart);

				$day = (int) substr($form['to'], 0, 2);
	   		$month = (int) substr($form['to'], 3, 2);
	   		$year = (int) substr($form['to'], 6, 4);
				$dateE = $year.'-'.$month.'-'.$day;
				//$dateE= date_create($dateend);



			$sql = "Select
	customers.customer_id,
	customers.firstname,
	customers.lastname,
	customers.dob,
	customers.address_1,
	customers.address_nr,
	customers.zip,
	customers.city,
	customers.state,
	customers.country,
	customers.phone,
	customers.mobile,
	customers.email,
	customers.id_image,
	campaign_prizes.prize,
	campaign_prizes.prize_image,
	customers_campaigns.id ,
	customers_campaigns.campaign_id,
	customers_campaigns.register_date,
	customers_campaigns.receipt,
	customers_campaigns.receipt_date,
	customers_campaigns.receipt_nr,
	customers_campaigns.receipt_checked,
	customers_campaigns.estratto_checked,
	customers_campaigns.status,
	customers_campaigns.prize_id,
	customers_campaigns.extra,
	customers_campaigns.shop_id,
	customers_campaigns.estratto,
	shops.organizzazione,
	shops.insegna,
	shops.provincia,
	shops.indirizzo,
	shops.start_date,
	shops.end_date,
	customers_campaigns.shop_state,
	campaigns.register_days,
	campaigns.campaign,
	campaigns.logic,
	campaigns.invia_mail,
	campaign_coupons.coupon,
	campaign_coupons.released_date,
	campaign_coupons.mail_sent,
	campaign_coupons.coupon,
	campaign_coupons.released_date,
	campaign_coupons.mail_sent,
	coupons.coupon_id,
	coupons.coupon as multi_coupon,
	coupons.mail_sent as multi_coupon_sent,
	coupons.release_date as multi_coupon_date
	From
	customers_campaigns
	Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
	Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
	LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
	LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
	LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
	LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
	WHERE
	customers_campaigns.campaign_id = ?
	AND ( DATE(customers_campaigns.register_date)  BETWEEN ? AND ? )
	AND shops.organizzazione = ?
	AND customers_campaigns.estratto = 0
	ORDER BY RAND()
	LIMIT 10";
	            $query = $this->db->query($sql,array($form['id'],$dateS,$dateE,$form['organizzazione']));
	            return($query->result_array());
	}

	function campaign_estratti($form){

		if ( isset ($form['from']) ){
	      $day = (int) substr($form['from'], 0, 2);
	   		$month = (int) substr($form['from'], 3, 2);
	   		$year = (int) substr($form['from'], 6, 4);
				$dateS = $year.'-'.$month.'-'.$day;
				//$dateS= date_create($datestart);

				$day = (int) substr($form['to'], 0, 2);
	   		$month = (int) substr($form['to'], 3, 2);
	   		$year = (int) substr($form['to'], 6, 4);
				$dateE = $year.'-'.$month.'-'.$day;
				//$dateE= date_create($dateend);
		}


			$sql = "Select
	customers.customer_id,
	customers.firstname,
	customers.lastname,
	customers.dob,
	customers.address_1,
	customers.address_nr,
	customers.zip,
	customers.city,
	customers.state,
	customers.country,
	customers.phone,
	customers.mobile,
	customers.email,
	customers.id_image,
	campaign_prizes.prize,
	campaign_prizes.prize_image,
	customers_campaigns.id ,
	customers_campaigns.campaign_id,
	customers_campaigns.register_date,
	customers_campaigns.receipt,
	customers_campaigns.receipt_date,
	customers_campaigns.receipt_nr,
	customers_campaigns.receipt_checked,
	customers_campaigns.estratto_checked,
	customers_campaigns.status,
	customers_campaigns.prize_id,
	customers_campaigns.extra,
	customers_campaigns.shop_id,
	customers_campaigns.estratto,
	shops.organizzazione,
	shops.insegna,
	shops.provincia,
	shops.indirizzo,
	shops.start_date,
	shops.end_date,
	customers_campaigns.shop_state,
	campaigns.register_days,
	campaigns.campaign,
	campaigns.logic,
	campaigns.invia_mail,
	campaign_coupons.coupon,
	campaign_coupons.released_date,
	campaign_coupons.mail_sent,
	campaign_coupons.coupon,
	campaign_coupons.released_date,
	campaign_coupons.mail_sent,
	coupons.coupon_id,
	coupons.coupon as multi_coupon,
	coupons.mail_sent as multi_coupon_sent,
	coupons.release_date as multi_coupon_date
	From
	customers_campaigns
	Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
	Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
	LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
	LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
	LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
	LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
	WHERE
	customers_campaigns.campaign_id = ?
	AND customers_campaigns.estratto = 1";
	if ( isset ( $form['from'] ) ){
		$sql = $sql." AND ( DATE(customers_campaigns.register_date)  BETWEEN ? AND ? )";
	}
	$query = $this->db->query($sql,array($form['id']));
	return($query->result_array());
	}

	public function campaign_estratto($form){
		$data = array (
			"estratto" 				=> 1 ,
			"data_estrazione"	=> date("Y-m-d H:i:s")
		);
		$this->db->where ( 'id' , $form['id'] );
		$this->db->update ( 'customers_campaigns' , $data );
		return true;
	}

	function campaign_premi_a_punti($form){

		if ( isset ($form['from']) ){
	      $day = (int) substr($form['from'], 0, 2);
	   		$month = (int) substr($form['from'], 3, 2);
	   		$year = (int) substr($form['from'], 6, 4);
				$dateS = $year.'-'.$month.'-'.$day;
				//$dateS= date_create($datestart);

				$day = (int) substr($form['to'], 0, 2);
	   		$month = (int) substr($form['to'], 3, 2);
	   		$year = (int) substr($form['to'], 6, 4);
				$dateE = $year.'-'.$month.'-'.$day;
				//$dateE= date_create($dateend);
		}


			$sql = "Select
	customers.customer_id,
	customers.firstname,
	customers.lastname,
	customers.dob,
	customers.address_1,
	customers.address_nr,
	customers.zip,
	customers.city,
	customers.state,
	customers.country,
	customers.phone,
	customers.mobile,
	customers.email,
	customers.id_image,
	campaign_prizes.prize,
	campaign_prizes.prize_image,
	customers_campaigns.id ,
	customers_campaigns.campaign_id,
	customers_campaigns.register_date,
	customers_campaigns.receipt,
	customers_campaigns.receipt_date,
	customers_campaigns.receipt_nr,
	customers_campaigns.receipt_checked,
	customers_campaigns.estratto_checked,
	customers_campaigns.status,
	customers_campaigns.prize_id,
	customers_campaigns.extra,
	customers_campaigns.shop_id,
	customers_campaigns.estratto,
	customers_campaigns.punti_uuid,
	shops.organizzazione,
	shops.insegna,
	shops.provincia,
	shops.indirizzo,
	shops.start_date,
	shops.end_date,
	customers_campaigns.shop_state,
	campaigns.register_days,
	campaigns.campaign,
	campaigns.logic,
	campaigns.invia_mail,
	campaign_coupons.coupon,
	campaign_coupons.released_date,
	campaign_coupons.mail_sent,
	campaign_coupons.coupon,
	campaign_coupons.released_date,
	campaign_coupons.mail_sent,
	coupons.coupon_id,
	coupons.coupon as multi_coupon,
	coupons.mail_sent as multi_coupon_sent,
	coupons.release_date as multi_coupon_date
	From
	customers_campaigns
	Inner Join campaigns ON customers_campaigns.campaign_id = campaigns.campaign_id
	Inner Join customers ON customers_campaigns.customer_id = customers.customer_id
	LEFT Join campaign_prizes ON customers_campaigns.prize_id = campaign_prizes.prize_id
	LEFT Join shops ON customers_campaigns.shop_id = shops.insegna_id
	LEFT JOIN campaign_coupons ON customers_campaigns.id = campaign_coupons.receipt_id
	LEFT JOIN coupons ON customers_campaigns.id = coupons.receipt_id
	WHERE
	customers_campaigns.campaign_id = ?
	AND customers_campaigns.punti_uuid <> ''
	GROUP BY customers_campaigns.punti_uuid";
	if ( isset ( $form['from'] ) ){
		$sql = $sql." AND ( DATE(customers_campaigns.register_date)  BETWEEN ? AND ? )";
	}
	$query = $this->db->query($sql,array($form['id']));
	return($query->result_array());
	}

    public function receipt_insert($form){
        date_default_timezone_set('Europe/Rome');
		$day = (int) substr($form['register_date'], 0, 2);
   		$month = (int) substr($form['register_date'], 3, 2);
   		$year = (int) substr($form['register_date'], 6, 4);
		$dateRegister = $year.'-'.$month.'-'.$day;
      $day = (int) substr($form['receipt_date'], 0, 2);
   		$month = (int) substr($form['receipt_date'], 3, 2);
   		$year = (int) substr($form['receipt_date'], 6, 4);
		$dateReceipt = $year.'-'.$month.'-'.$day;

        $data = array (
			'campaign_id' 	=> $form['campaign_id'],
			'customer_id' 	=> $form['customer_id'],
			'prize_id'		=> $form['prize_id'],
			'prize_detail_id' => 0,//$form['prize_detail_id'],
			'register_date' => $dateRegister,
			'update_date'	=> date('Y-m-d H:i:s'),
			'shipment_date'	=> date('Y-m-d H:i:s'),
			'receipt'		=> $_SESSION['scontrino'],
			'receipt_date'	=> $dateReceipt,
			'receipt_amount'=> '0',
			'receipt_nr'	=> $form['receipt'],
			'shop_id'		=> $form['shop_id'],
			'shop_state'	=> $form['shop_state'],
			'status'		=> 1
		);
		$data = $this->security->xss_clean($data);

		//print_r ( $data );
		if ( ! $this->db->insert('customers_campaigns', $data) ) {
            return false;
        } else {
            $sourceFile = "/home/admin/public_html/tpbolton/public/users/upload/temp/" . $_SESSION['scontrino'];
			$targetFile = "/home/admin/public_html/tpbolton/public/users/upload/" . $_SESSION['scontrino'];
			rename ( $sourceFile , $targetFile );
            return true;
        }

    }

    public function receipt_delete ( $form ){
		$sql = "INSERT INTO customers_campaigns_del SELECT * FROM customers_campaigns WHERE id = ?";
		$query = $this->db->query($sql,$form['id']);
		$this->db->where ( 'id' , $form['id'] );
		$this->db->delete( 'customers_campaigns' );
		return true;
	}

		public function receipt_shop ( $form ){
			$sql = "SELECT * FROM shops WHERE shop_id = ?";
			$query = $this->db->query($sql,$form['shop_id']);
		}

    public function premi ( $id ){
        $sql = "SELECT * FROM campaign_prizes WHERE campaign_id = ? AND prize_select = 1 GROUP BY prize_image";
        $query = $this->db->query($sql , $id );
        return ( $query->result_array() );
    }

	public function save_settings($form){
		date_default_timezone_set('Europe/Rome');
		$day = (int) substr($form['start'], 0, 2);
   	$month = (int) substr($form['start'], 3, 2);
   	$year = (int) substr($form['start'], 6, 4);
		$datestart = $year.'-'.$month.'-'.$day;
		$dateS= date_create($datestart);

		$day = (int) substr($form['end'], 0, 2);
   		$month = (int) substr($form['end'], 3, 2);
   		$year = (int) substr($form['end'], 6, 4);
		$dateend = $year.'-'.$month.'-'.$day;
		$dateE= date_create($dateend);

		$data = array (
			'campaign'		=> $form['campaign'],
			'url'			=> $form['url'],
			'start'			=> date_format($dateS,"Y/m/d H:i:s"),
			'end'			=> date_format($dateE,"Y/m/d H:i:s"),
			'register_days'	=> $form['days'],
			'active'		=> $form['active']

		);

		$this->db->set(	$data );
		$this->db->where ( 'campaign_id' , $form['id'] );
		$this->db->update ( 'campaigns' );

		$data = array (
			'abstract'		=> $form['shortdesc'],
			'howto'			=> $form['howto'],
			'description'	=> $form['description'],
			'faq'			=> $form['faq'],
			'email_template' => $form['email_template']
		);

		$this->db->set(	$data );
		$this->db->where ( 'campaign_id' , $form['id'] );
		$this->db->update ( 'campaign_asset' );

	}

	public function save_settings_colors ( $form ){
		$data = array (
			$form['field'] => $form['color']
		);
		$this->db->set(	$data );
		$this->db->where ( 'campaign_id' , $form['id'] );
		$this->db->update ( 'campaign_asset' );
		print_r ($form);
	}

	public function save_prize($form){
		$data = array (
			'prize'	=> $form['title'],
			'prize_description' => $form['desc']
		);
		$this->db->set(	$data );
		$this->db->where ( 'prize_id' , $form['id'] );
		$this->db->update ( 'campaign_prizes' );
	}

	public function add_prize($form){
		$data = array (
			'prize'	=> $form['title'],
			'prize_description' => $form['desc'],
			'campaign_id' => $form['id']
		);
		$this->db->insert ( 'campaign_prizes' , $data );
	}

	public function uploaded_file($form){
		$data = array (
			$form['field']	=> $form['file']
		);
		$this->db->set(	$data );
		$this->db->where ( 'campaign_id' , $form['id'] );
		$this->db->update ( 'campaign_asset' );
	}

	public function prize_uploaded_file($form){
		$this->db->set(	'prize_image' , $form['file'] );
		$this->db->where ( 'prize_id' , $form['id'] );
		$this->db->update ( 'campaign_prizes' );
	}

	public function prize_template($id){
		$sql = "SELECT coupon_template FROM campaign_prizes WHERE prize_id = ?";
		$query = $this->db->query($sql,$id);
		$template = $query->result_array();
		//echo $template[0]['coupon_template'] ;
		return $template[0]['coupon_template'] ;
	}

	public function receipt_checked(){

		$this->db->set(	'receipt_checked' , $_POST['checked'] );
		$this->db->where('id',$_POST['id']);
		return $this->db->update('customers_campaigns');
	}

	public function estratto_checked(){

		$this->db->set(	'estratto_checked' , $_POST['checked'] );
		$this->db->where('id',$_POST['id']);
		return $this->db->update('customers_campaigns');
	}
}
