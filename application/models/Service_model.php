<?php

class Service_Model extends CI_Model {

	function __construct(){
		parent::__construct();

	}



	function login($form){
		$username = $form['u'];
		$password  = MD5($form['p']);
		$sql = "SELECT users.*
			FROM users
			WHERE username = ? AND password = ?";
		$query = $this->db->query ( $sql , array ( $username , $password ) );
		if ( count($query->result_array()) != 0  ){
			$user = $query->result_array();
			if ( $user[0]['user_type'] == 'O' ){
				$sql = "SELECT * FROM campaign_users WHERE user_id = ?";
				$query = $this->db->query ( $sql , $user[0]['id'] );
				$c = $query->result_array();
				$a = array ();
				foreach ( $c as $r ){
					array_push( $a , $r['campaign_id'] );
				}
			} else {
				$a = 'all';
			}

			$session_data = array (
					'login' => true,
					'username' => $username,
					'user_type' => $user[0]['user_type'],
					'user_campaigns' => $a
			);
			$this->session->set_userdata('login', $session_data);
			return base_url();
		} else {
			return false;
		}



	}

	function users($id){
		if ( $id == '0' ){
			$sql = "
			Select
users.id,
users.username,
users.email,
users.`password`,
users.block,
users.user_type,
campaigns.campaign_id,
campaigns.campaign
From
users
left Join campaign_users ON users.id = campaign_users.user_id
left Join campaigns ON campaign_users.campaign_id = campaigns.campaign_id
";
			$query = $this->db->query($sql);
		}
		return $query->result_array();

	}

	function shops($id){
		$sql = "SELECT * FROM shops WHERE campaign_id = ? GROUP BY insegna ORDER BY insegna";
		$query = $this->db->query($sql,$id);
		return($query->result_array());
	}

	function all_shops($id){
		$sql = "SELECT * FROM shops WHERE campaign_id = ? ORDER BY insegna";
		$query = $this->db->query($sql,$id);
		return($query->result_array());
	}

	function states(){
		//$sql = "SELECT * FROM province ORDER BY nome_province";
		$sql = "SELECT provincia FROM shops GROUP BY provincia ORDER BY provincia";
		$query = $this->db->query($sql);
		return($query->result_array());
	}

    function campaign_shops_states ( $id ){
        $sql = "SELECT * FROM shops WHERE campaign_id = ? GROUP BY provincia ORDER BY provincia";
        $query = $this->db->query($sql,$id);
		return($query->result_array());
    }

    function campaign_shops_by_state ( $form ){
        $sql = "SELECT * FROM shops WHERE campaign_id = ? AND provincia = ? ORDER BY insegna";
        $query = $this->db->query($sql,array($form['campaign_id'],$form['pv']));
		return($query->result_array());
    }

	function shops_state($id){
		//$sql = "SELECT * FROM province ORDER BY nome_province";
		$sql = "SELECT * FROM shops WHERE provincia = ? ORDER BY insegna";
		$query = $this->db->query($sql,$id);
		return($query->result_array());
	}

	function organizations($id){
		$sql = "SELECT * FROM shops WHERE campaign_id = ? GROUP BY organizzazione ORDER BY organizzazione";
		$query = $this->db->query($sql,$id);
		return($query->result_array());
	}

	public function insertCSV($data){
    	$this->db->insert('shops', $data);
        return $this->db->insert_id();
    }

	public function create_coupons($form){
		$code = 'TP'.$form['code'];
		$codelen = 12	; // change as needed
		$a = array();
		for ( $n=0 ; $n < 100 ; $n++ ){			$code = 'TP'.$form['code'];

			for( $i=strlen($code); $i<$codelen; $i++) {			    $code .= dechex(rand(0,15));
			}
			echo $code;
			array_push ( $a , strtoupper($code) );
		}
		$a = array_unique ( $a );
		$array = array('prize_id =' => $form['prize'], 'campaign_id =' => $form['id'] , 'customer_id =' => 0 );
		$this->db->where($array);
		$this->db->delete('coupons');

		foreach ( $a as $c ){
			$data = array (
				'campaign_id' 	=> $form['id'] ,
				'prize_id'		=> $form['prize'],
				'coupon'		=> $c
			);
			$this->db->insert('coupons', $data);
		}

	}

	public function list_coupons ( $id , $prize ){
		$sql = "SELECT coupon
			FROM campaign_coupons
			WHERE campaign_id = ? AND prize_id = ?
			ORDER BY coupon";
		$query = $this->db->query($sql,array($id,$prize));
		return $query;

	}

    public function messages (){
        foreach ( $this->config->item('status_id') as $status ){
            echo $status;
        }
    }

    public function receipt_update($form){			date_default_timezone_set('Europe/Rome');
			$day = (int) substr($form['register_date'], 0, 2);
			$month = (int) substr($form['register_date'], 3, 2);
			$year = (int) substr($form['register_date'], 6, 4);
			$d = $year.'/'.$month.'/'.$day;
			$register_date = $d;
			$day = (int) substr($form['receipt_date'], 0, 2);
			$month = (int) substr($form['receipt_date'], 3, 2);
			$year = (int) substr($form['receipt_date'], 6, 4);
			$d = $year.'/'.$month.'/'.$day;
			$receipt_date = $d;
			if ( $form['receipt_img'] != '' ){
				$data = array (
					"receipt_nr"    => $form['receipt'] ,
					"register_date" => $register_date,
					"receipt_date"  => $receipt_date,
					"receipt"       => $form['receipt_img'],
					"prize_id"      => $form['prize_id'],
					"shop_id"		=> $form['shop_id']
        		);
				$sourceFile = "/home/admin/public_html/tpbolton/public/users/upload/temp/" . $form['receipt_img'];
        		$targetFile = "/home/admin/public_html/tpbolton/public/users/upload/" . $form['receipt_img'];
				rename ( $sourceFile , $targetFile );
			} else {
				$data = array (
					"receipt_nr"    => $form['receipt'] ,
					"register_date" => $register_date,
					"receipt_date"  => $receipt_date,
					"prize_id"      => $form['prize_id'],
					"shop_id"		=> $form['shop_id']
				);
			}
			$this->db->where ( "id" , $form['id'] );
			if ( $this->db->update ( 'customers_campaigns' , $data ) ){
				return true;
			} else {
				return false;
			}
		}


    public function send_coupon($form){
        $sql = "SELECT *  FROM campaign_coupons
                WHERE
                campaign_id = ?
                AND
                customer_id = ?
                AND
                receipt_id = ?
                AND
                mail_sent = 1";
        $query = $this->db->query($sql , array ( $form['promo'] , $form['customer'] , $form['receipt'] ) );

        if ( !$query->result_array() ){
            $sql = "SELECT * FROM campaign_coupons
                WHERE campaign_id = ? AND customer_id = 0 AND receipt_id = 0 AND mail_sent = 0 ORDER BY coupon_id LIMIT 0,1";
            $query = $this->db->query($sql,$form['promo']);
            $rec = $query->result_array();
            if ( $query->result_array() ){
                $rec_id = $rec[0]['coupon_id'];
                $_SESSION['coupon_id'] = $rec_id;
                $_SESSION['coupon_expires'] = date('Y-m-d H:i:s');
                $_SESSION['coupon_sent'] = 0;
                $coupon = $rec[0]['coupon'];
                $data = array (
    				'customer_id' 	=> $form['customer'] ,
    				'released_date' => date('Y-m-d H:i:s'),
                    'receipt_id'    => $form['receipt']
    			);
                $this->db->where('coupon_id',$rec_id);
                $this->db->update('campaign_coupons' , $data);
                return $coupon;
            } else {
                return "error-no-coupon";
            }
        } else {
            $rec = $query->result_array();
            $_SESSION['coupon_id'] = $rec[0]['coupon_id'];
            $_SESSION['coupon_expires'] = $rec[0]['released_date'];
            $_SESSION['coupon_sent'] = 1;
            return "error-coupon-sent";
        }

    }

    public function coupon_sent ( $form ){
        $sql = "UPDATE campaign_coupons
                SET
                mail_sent = 1
                WHERE
                coupon_id = ?";
        if ( $this->db->query ( $sql , $_SESSION['coupon_id'] ) ) {
            return true;
        } else {
            return false;
        }

    }
    public function coupon_multi_sent ( $form ){
        $sql = "UPDATE coupons  SET mail_sent = 1 WHERE coupon_id = ?";
        if ( $this->db->query ( $sql , $_SESSION['coupon_id'] ) ) {
            return true;
        } else {
            return false;
        }
    }

    public function voucher_template ( ){

		if ( $_POST['prize'] != 0 ){
			$sql = "SELECT coupon_template FROM campaign_prizes WHERE prize_id = ?";
      $query = $this->db->query($sql,$_POST['prize']);
      $temp = $query->result_array();
			$template = $temp[0]['coupon_template'];
		} else {
			$sql = "SELECT coupon_template FROM campaign_prizes WHERE campaign_id = ?";
        	$query = $this->db->query($sql,$_POST['promo']);
        	$temp = $query->result_array();
			$template = $temp[0]['coupon_template'];
		}

        return $template;
    }

    public function revoke_coupon (){
        $sql = "UPDATE campaign_coupons
                SET
                    customer_id = 0 ,
                    receipt_id = 0,
                    released_date = NULL
                WHERE
                    coupon_id = ?";
        $query = $this->db->query($sql,$_SESSION['coupon_id']);
        return true;

    }
    public function revoke_coupon_multi (){
        $sql = "UPDATE coupons
            SET
            customer_id = 0 ,
            receipt_id = 0,
            release_date = NULL
            WHERE
            coupon_id = ?";
            $query = $this->db->query($sql,$_SESSION['coupon_id']);        return true;
    }

    public function revoke_user_coupon ($form ){
        $sql = "UPDATE campaign_coupons
                SET
                    customer_id = 0 ,
                    receipt_id = 0,
                    released_date = NULL
                WHERE
                    campaign_id = ?
                    AND
                    customer_id = ?
                    AND
                    receipt_id = ?";
        $query = $this->db->query($sql,array ( $form['campaign'] , $form['customer'] , $form['receipt'] ) );
        return true;

    }
    public function send_coupon_multi($form){
		$prize_id = $form['prize'];
        if ( intval($form['prize']) == 0 && intval($form['promo']) == 12 ){
			$prize_id = 43;
		}
			$sql = "SELECT *  FROM coupons
                WHERE
                campaign_id = ?
                AND
                prize_id = ?
                AND
                customer_id = ?
                AND
                receipt_id = ?
                AND
                mail_sent = 1";
        	$query = $this->db->query($sql , array ( $form['promo'] , $prize_id, $form['customer'] , $form['receipt'] ) );


		if ( !$query->result_array() ){
			$sql = "SELECT * FROM coupons
                WHERE campaign_id = ? AND prize_id = ? AND customer_id = 0 AND receipt_id = 0 AND mail_sent = 0 ORDER BY coupon_id LIMIT 0,1";
            $query = $this->db->query($sql,array($form['promo'],$prize_id));

            $rec = $query->result_array();
			if ( $query->result_array() ){
                $rec_id = $rec[0]['coupon_id'];
                $_SESSION['coupon_id'] = $rec_id;
                $_SESSION['coupon_expires'] = date('Y-m-d H:i:s');
                $_SESSION['coupon_sent'] = 0;
                $_SESSION['coupon_type'] = 'coupon';
                $coupon = $rec[0]['coupon'];
                if ( $rec[0]['coupon'] == '' ){
                    $coupon = $rec[0]['link'];
                    $_SESSION['coupon_type'] = 'link';
                }
                $data = array (
                    'customer_id' 	=> $form['customer'] ,
                    'release_date' => date('Y-m-d H:i:s'),
                    'receipt_id'    => $form['receipt']
                );
                $this->db->where('coupon_id',$rec_id);
                $this->db->update('coupons' , $data);
                return $coupon;
             } else {
                return "error-no-coupon";
             }
        } else {
            $rec = $query->result_array();
            $_SESSION['coupon_id'] = $rec[0]['coupon_id'];
            $_SESSION['coupon_expires'] = $rec[0]['release_date'];
            $_SESSION['coupon_sent'] = 1;
            return "error-coupon-sent";
        }
    }

	public function consest_shops($id){
		$sql = "Select
			shops_contest.*,
			shops_users.email,
			shops_users.nome,
			shops_users.cognome,
			shops_materiali.image
			From
			campaigns
			Inner Join shops_contest ON campaigns.campaign_id = shops_contest.campaign_id
			LEFT Join shops_materiali ON shops_contest.insegna_id = shops_materiali.shop_id
			Inner Join shops_users ON shops_contest.insegna_id = shops_users.shop_id
			WHERE campaigns.campaign_id = ?
			GROUP BY shops_contest.insegna_id";
		$query = $this->db->query($sql , $id);
		if ( !$query->result_array() ){
			return false;
		} else {
			return $query->result_array();
		}
	}

	public function consest_foto($id){
		$sql = "Select
			shops_contest.*,
			shops_materiali.*,
			shops_users.email,
			shops_users.nome,
			shops_users.cognome
			From
			campaigns
			Inner Join shops_contest ON campaigns.campaign_id = shops_contest.campaign_id
			Inner Join shops_materiali ON shops_contest.insegna_id = shops_materiali.shop_id
			Inner Join shops_users ON shops_contest.insegna_id = shops_users.shop_id
			WHERE shops_contest.insegna_id = ?";
		$query = $this->db->query($sql , $id);
		if ( !$query->result_array() ){
			return false;
		} else {
			return $query->result_array();
		}
	}

}
