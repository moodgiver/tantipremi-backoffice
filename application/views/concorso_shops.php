
<div class="col-lg-12">
		<table id="tableshops" class="table table-striped table-bordered dataTable filter-result">
			<thead>
				<tr role="row">
				<th>#</th>
				<th class="sorting" tabindex="0">Gruppo</th>
				<th class="sorting" tabindex="0">Organizzazione</th>
				<th>Punto Vendita</th>
				<th>Regione</th>
				<th>Provincia</th>
				<th>Inizio</th>
				<th>Fine</th>
				<th></th>
				</tr>
			</thead>
			<tbody>
			<?php
				if ( count($shops) > 0 ){
					$c = 1;
					foreach ( $shops as $r ){
						echo '<tr class="pointer">
							<td>'.$c.'</td>
							<td>'.$r['gruppo'].'</td>
							<td>'.$r['organizzazione'].'</td>
							<td>'.$r['insegna'].'</td>
							<td>'.$r['regione'].'</td>
							<td>'.$r['provincia'].'</td>
							<td>'.$r['start_date'].'</td>
							<td>'.$r['end_date'].'</td>
							<td><span class="fa fa-edit btn-action pointer" data-controller="edit-shop" data-id="'.$r['insegna_id'].'" title="Modifica Punto Vendita"></span></td>
						</tr>';
						$c++;
					}
				}	
			
			?>
			</tbody>
		</table>
		<?php
			if ( count($shops) == 0 ){
			echo '
				<div class="col-lg-12">
				<p>
				
				<button class="btn btn-upload btn-primary btn-flat btn-lg">Carica Punti Vendita</button><br><br>
				<h3><span class="fa fa-info"></span> Informazioni per il caricamento Punti Vendita</h3>
				Cliccando sul pulsante Carica Punti Vendita il sistema caricher&aacute; automaticamente i punti vendita direttamente da un vostro file in formato <strong>xls (excel)</strong>.<br>
				Il file <strong>deve prevedere</strong> le seguenti colonne (<strong>l\'ordine delle  colonne deve essere rispettato</strong>):
				<ul>
					<li>Gruppo (numerico)</li>
					<li>Organizzazione</li>
					<li>Insegna</li>
					<li>Regione</li>
					<li>Sigla provincia (max 2 caratteri)</li>
					<li>Inizio promozione (aaaa-mm-gg)</li>
					<li>Fine promozione (aaaa-mm-gg)</li>
				</ul>
				
				</p>
				</div>
				';
			}
		?>
	</div>
	
</div>
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="col-md-12">
			<br>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="panel panel-primary">
				<div class="panel-heading">Carica Punti Vendita</div>
				<div class="panel-body" style="overflow:auto;">
	      			<form action="<?php echo base_url();?>admin/insertCSV" id="myDropzone" class="dropzone">
					<input type="hidden" class="campaign_id" name="id" value="<?=$_POST['id']?>">
					</form>
					
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-default" data-dismiss="modal">Chiudi</button>
				</div>
			</div>
			
		</div>
    </div>
  </div>
</div>


<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/dropzone/dropzone.js"></script>

<script>
$(document).ready ( function(){
	
	$('#tableshops').DataTable({
      "paging": true,
      "lengthChange": true,
	  "pageLength": 50,
      	"searching": true,
      	"ordering": true,
      	"info": true,
      	"autoWidth": false,
	  	"pagingType": "full_numbers",
	 "language":
	 	{
	  		"sEmptyTable":     "Nessun dato presente nella tabella",
		 	"sInfo":           "Vista da _START_ a _END_ di _TOTAL_ elementi",
			"sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
			"sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
			"sInfoPostFix":    "",
			"sInfoThousands":  ".",
			"sLengthMenu":     "Visualizza _MENU_ elementi",
			"sLoadingRecords": "Caricamento...",
			"sProcessing":     "Elaborazione...",
			"sSearch":         "Cerca:",
			"sZeroRecords":    "La ricerca non ha portato alcun risultato.",
			"oPaginate": {
				"sFirst":      "Inizio",
				"sPrevious":   "Precedente",
				"sNext":       "Successivo",
				"sLast":       "Fine"
			},
			"oAria": {
				"sSortAscending":  ": attiva per ordinare la colonna in ordine crescente",
				"sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
			}
	   }
    });

	
	$('#tableshops').on( 'click', 'tr', function () {
		var table = $('#tableshops').DataTable();
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );

	$('.btn-upload').click(function(){
		$('#uploadModal').modal('show');
	});

	Dropzone.autoDiscover = false;
            $("#myDropzone").dropzone({
                dictDefaultMessage: "Clicca qui o trascina il file qui per caricare i dati",
                clickable: true,
                maxFilesize: 5,
				autoProcessQueue: true,
                uploadMultiple: false,
                addRemoveLinks: false,
				createImageThumbnails: true,
				acceptedFiles: 'application/xls',
				dictFileTooBig: 'File superiore a 5 MB. Impossibile caricare',
				init: function () {
            		myDropzone = this;
					$('.dz-preview').remove();
		            this.on("success", function(file) {
					  $('.dz-error-message').html('');
					  $('.dz-preview').remove();
        		      $('#uploadModal').modal('hide');
	            	});
					this.on("error" , function ( e , errorMsg ){
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});
       			}
			
            });
	
});
</script>
