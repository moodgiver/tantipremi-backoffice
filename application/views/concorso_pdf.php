
<div class="col-lg-12">
	
	
	<div class="col-lg-12">
		<label>REGOLAMENO</label>
		<h4><?=$promo[0]['rules']?></h4>
		<button class="btn btn-upload">Carica</button>
	</div>
	
</div>
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="col-md-12">
			<br>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="panel panel-primary">
				<div class="panel-heading">Carica Regolamento</div>
				<div class="panel-body" style="overflow:auto;">
	      			<form action="<?php echo base_url();?>admin/upload" id="myDropzone" class="dropzone">
					<input type="hidden" name="field" value="rules">
					<input type="hidden" name="code" value="<?=$promo[0]['code']?>">
					<input type="hidden" class="campaign_id" name="id" value="<?=$promo[0]['campaign_id']?>">
					</form>
					
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-default" data-dismiss="modal">Chiudi</button>
				</div>
			</div>
			
		</div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/dropzone/dropzone.js"></script>

<script>
$(document).ready ( function(){
	$('.btn-upload').click(function(){
		$('#uploadModal').modal('show');
	});

	Dropzone.autoDiscover = false;
            $("#myDropzone").dropzone({
                dictDefaultMessage: "Clicca qui o trascina la tua immagine/file qui per caricarla",
                clickable: true,
                maxFilesize: 1,
				autoProcessQueue: true,
                uploadMultiple: false,
                addRemoveLinks: false,
				createImageThumbnails: true,
				acceptedFiles: 'image/*,application/pdf',
				dictFileTooBig: 'Immagine superiore a 1 MB. Impossibile caricare',
				init: function () {
            		myDropzone = this;
					$('.dz-preview').remove();
		            this.on("success", function(file) {
					  $('.dz-error-message').html('');
					  $('.dz-preview').remove();
        		      $('#uploadModal').modal('hide');
	            	});
					this.on("error" , function ( e , errorMsg ){
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});
       			}
			
            });
	
});
</script>
