<!-- user filter start -->
<?php
  $this->load->model('Service_model');
  $organizzazioni = $this->Service_model->organizations($_POST['id']);
?>

<div class="col-lg-1">
  <label>Stato</label>
  <?php
    if ( isset($_POST['da_estrarre']) ){
      echo '<h3>ESTRAZIONE</h3>';
    }
   ?>
</div>
<div class="col-lg-2">
  <select class="form-control select-input select-status" data-controller="status-filter" data-id="<?=$_POST['id']?>">
  <option value="">Tutti</option>
  <?php
    $id_status = $this->config->item('status_id') ;
    $status = $this->config->item('status');
    $nextstatus = $this->config->item('status_flow');
    $sendmail = $this->config->item('status_send_mail');
    $i = 0;
    $options = '<option value="">Vai a ...</option>';
    foreach ( $status as $s ){
      if ( $fstatus != '' ){
        if ( $fstatus == $id_status[$i] ){

          echo '<option value="'.$id_status[$i].'" selected>'.$s.'</option>';
        } else {
          echo '<option value="'.$id_status[$i].'">'.$s.'</option>';
        }
      } else {
        echo '<option value="'.$id_status[$i].'">'.$s.'</option>';
      }
      if ( $i > 0 ){
        $options .= '<option value="'.$id_status[$i].';'.$nextstatus[$i].';'.$sendmail[$i].'">'.$s.'</option>';
      }
      $i++;
    }
    $selected = '';
    if ( $fstatus == 'estratto_checked' ){
      $selected = 'selected';
    }
    echo '<option value="estratto_checked" '. $selected. '>Estrazioni confermate</option>';
  ?>
</select>

</div>
<div class="col-lg-1">
  <label>Dal</label>
</div>
<div class="col-lg-2">
  <input class="form-control filter-start-date datepicker startDateIscritti" name="startDateIscritti" value="<?=date('d/m/Y', strtotime("-7 days"));?>">
</div><div class="col-lg-1">
  <label>Al</label>
</div>
<div class="col-lg-2">
  <input class="form-control filter-end-date datepicker" name="endDateIscritti" value="<?=date('d/m/Y',now());?>">
</div>
<div class="col-lg-1">
  <button class="btn btn-primary btn-action" data-controller="iscritti-concorso-filter" data-id="<?=$_POST['id']?>">Filtra</button>
</div>
<div class="col-lg-1">
  <button class="btn btn-primary btn-action" data-controller="iscritti-concorso-estrazione-modal" data-id="<?=$_POST['id']?>">Estrai</button>
</div>
<div class="col-lg-1 text-right">
  <?php
      echo '
      <button class="btn btn-flat btn-success btn-action" data-controller="xls-status-export" data-id="'.$_POST['id'].'" title="Esporta in Excel"><span class="fa fa-file-excel-o"></span> Esporta</button>';
      //}

    ?>
</div>


<div class="modal fade" id="estrazioneModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" style="width:850px!important">
        <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="panel panel-primary">
            <div class="panel-heading">Imposta estrazione</div>
            <div class="panel-body" style="overflow:auto;">

              <div class="col-lg-5">
                <label>Organizzazione</label>
                <select class="form-control filter-organization">
                  <option value="">Tutte</option>
                  <?php
                    foreach ( $organizzazioni AS $org ){
                      echo '<option value="'.$org['organizzazione'].'">'.$org['organizzazione'].'</option>';
                    }
                  ?>
                </select>
              </div>
              <div class="col-lg-5">
                    <label>Nr giocate da estrarre su un totale di <?php echo count($iscritti) ?></label>
                    <input type="text" class="form-control filter-estrai-rows" value="10">
              </div>
            </div>

        </div>
        <div class="panel-footer text-center">
          <button class="btn btn-default" data-dismiss="modal">Chiudi</button>
                    &nbsp;
          <button class="btn btn-success btn-action" data-controller="iscritti-concorso-estrai" data-id="<?=$_POST['id']?>" data-rows="<?php echo count($iscritti)?>">Estrai</button>
        </div>
        </div>
      </div>
    </div>
</div>
