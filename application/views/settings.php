
<div class="container">
    <div class="col-lg-12">
	    <h2>Status/Messaggi</h2>
    </div>
    <table class="table table-striped table-bordered">
        <thead>
            <th>ID</th>
            <th>Status</th>
            <th>Messaggio WEB</th>
            <th>Email</th>
        </thead>
        <?php 
        foreach ( $this->config->item('status_id') as $status ){
            echo '<tr>
                  <td>'.$status.'</td>
                  <td>'.$this->config->item('status')[$status].'</td>
                  <td>'.$this->config->item('status_help')[$status].'</td>
                  <td>'.$this->config->item('status_email')[$status].'</td>
                </tr>';
        }
    ?>
    </table>
    
</div>