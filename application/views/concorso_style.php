<div class="col-lg-12">



	<div class="clearfix"></div>

	<div class="col-lg-3 text-center">
		<label>Immagine</label><br>
		<?php
			$thumb = $promo[0]['code'].'/'.$promo[0]['thumbnail'];
			if ( $promo[0]['thumbnail'] == '' ){
				$thumb = 'noimage.png';
			}
		?>
		<img src="<?=$this->service->web_url()?>public/img/<?=$thumb?>" class="btn-upload-image pointer" data-field="thumbnail" data-folder="public" width="90%"><span class="field_help">Cliccare per caricare<br>Formato: png <span class="fa fa-chevron-right"></span> 400x400px</span>
	</div>

	<div class="col-lg-3 text-center">
		<label>Fondo</label><br>
		<?php
			$bg = $promo[0]['code'].'/'.$promo[0]['background_image'];
			if ( $promo[0]['thumbnail'] == '' ){
				$bg = 'noimage.png';
			}
		?>
		<img src="<?=$this->service->web_url()?>public/img/<?=$bg?>" class="btn-upload-image pointer" data-field="background_image" data-folder="public" width="90%"><br>
		<span class="field_help">Cliccare per caricare<br>Formato: jpg/png <span class="fa fa-chevron-right"></span> 566x320px</span>
	</div>

	<div class="col-lg-2">
		<label>Colore di fondo pagina</label>
		<div class="input-group my-colorpicker colorpicker-element" data-field="background_color" data-id="<?=$promo[0]['campaign_id']?>">
			<input type="text" class="form-control settings_color" data-field="background_color" data-id="<?=$promo[0]['campaign_id']?>" name="background_color" value="<?=$promo[0]['background_color']?>">
            <div class="input-group-addon">
            	<i style="background-color: <?=$promo[0]['background_color']?>;"></i>
            </div>
        </div>
		<br><br>
		<label>Colore testo</label>
		<div class="input-group my-colorpicker colorpicker-element" data-field="foreground_color" data-id="<?=$promo[0]['campaign_id']?>">
			<input type="text" class="form-control settings_color" data-field="foreground_color" data-id="<?=$promo[0]['campaign_id']?>" name="foreground_color"  value="<?=$promo[0]['foreground_color']?>">
            <div class="input-group-addon">
            	<i style="background-color: <?=$promo[0]['foreground_color']?>;"></i>
            </div>
        </div>
	</div>

	<div class="col-lg-2">
		<label>Colore di fondo testata</label>
		<div class="input-group my-colorpicker colorpicker-element" data-field="header_bg" data-id="<?=$promo[0]['campaign_id']?>">
			<input type="text" class="form-control settings_color" data-field="header_bg" data-id="<?=$promo[0]['campaign_id']?>" name="header_bg" value="<?=$promo[0]['header_bg']?>">
            <div class="input-group-addon">
            	<i style="background-color: <?=$promo[0]['header_bg']?>;"></i>
            </div>
        </div>
		<br><br>
		<label>Colore testo testata</label>
		<div class="input-group my-colorpicker colorpicker-element" data-field="header_fg" data-id="<?=$promo[0]['campaign_id']?>">
			<input type="text" class="form-control settings_color" data-field="header_fg" data-id="<?=$promo[0]['campaign_id']?>" name="header_fg"  value="<?=$promo[0]['header_fg']?>">
            <div class="input-group-addon">
            	<i style="background-color: <?=$promo[0]['header_fg']?>;"></i>
            </div>
    </div><br><br>
		<label>Colore testo abstract</label>
		<div class="input-group my-colorpicker colorpicker-element" data-field="abstract_color" data-id="<?=$promo[0]['campaign_id']?>">
			<input type="text" class="form-control settings_color" data-field="abstract_color" data-id="<?=$promo[0]['campaign_id']?>" name="abstract_color"  value="<?=$promo[0]['abstract_color']?>">
            <div class="input-group-addon">
            	<i style="background-color: <?=$promo[0]['abstract_color']?>;"></i>
            </div>
    </div>
	</div>

</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="col-md-12">
			<br>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="panel panel-primary">
				<div class="panel-heading">Carica Regolamento</div>
				<div class="panel-body" style="overflow:auto;">
	      			<form action="<?php echo base_url();?>admin/upload" id="myDropzone" class="dropzone">
					<input type="hidden" class="field" name="field" value="">
					<input type="hidden" class="folder" name="folder">
					<input type="hidden" name="code" value="<?=$promo[0]['code']?>">
					<input type="hidden" class="campaign_id" name="id" value="<?=$promo[0]['campaign_id']?>">
					</form>

				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-default" data-dismiss="modal">Chiudi</button>
				</div>
			</div>

		</div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/dropzone/dropzone.js"></script>


<script src="<?=$this->config->item('adminlte')?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<link rel="stylesheet" href="<?=$this->config->item('adminlte');?>plugins/colorpicker/bootstrap-colorpicker.min.css">



<script>
$(document).ready ( function(){


	$('.my-colorpicker').colorpicker().on('changeColor',function(e){
			$.post ( 'ajax' ,
				{
					action: 'settings_color' ,
					id: $(this).data('id'),
					field: $(this).data('field'),
					color: e.color.toHex()
				} , function ( result ){
					doNotification ( '' , 'Colore aggiornato' );
				}
			);
	});

	$('.settings_color').on('change',function(e){
			$.post ( 'ajax' ,
				{
					action: 'settings_color' ,
					id: $(this).data('id'),
					field: $(this).data('field'),
					color: $(this).val()
				} , function ( result ){
					doNotification ( '' , 'Colore aggiornato' );
				}
			);
	});


	$('.my-colorpicker').colorpicker().on('changeColor',function(e){
			$.post ( 'ajax' ,
				{
					action: 'settings_color' ,
					id: $(this).data('id'),
					field: $(this).data('field'),
					color: e.color.toHex()
				}
			);
	});

	$('.btn-upload-image').click(function(){
		$('.field').val ( $(this).data('field') );
		$('.folder').val ( $(this).data('folder') );
		$('#uploadModal').modal('show');
	});

	Dropzone.autoDiscover = false;
            $("#myDropzone").dropzone({
                dictDefaultMessage: "Clicca qui o trascina la tua immagine/file qui per caricarla",
                clickable: true,
                maxFilesize: 1,
				autoProcessQueue: true,
                uploadMultiple: false,
                addRemoveLinks: false,
				createImageThumbnails: true,
				acceptedFiles: 'image/*,application/pdf',
				dictFileTooBig: 'Immagine superiore a 1 MB. Impossibile caricare',
				init: function () {
            		myDropzone = this;
					$('.dz-preview').remove();
		            this.on("success", function(file) {
					  $('.dz-error-message').html('');
					  $('.dz-preview').remove();
        		      $('#uploadModal').modal('hide');
	            	});
					this.on("error" , function ( e , errorMsg ){
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});
       			}

            });

});
</script>
