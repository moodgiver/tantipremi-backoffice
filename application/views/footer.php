	  <?php print_r ( $_SESSION );?>
	  </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; <?= date('d.m.Y')?> <a href="#">DUAL Srl</a>.</strong> <span class="ipcountry"></span>.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
	  <iframe id="workFrame" class="hide"></iframe>
		
	</div><!-- ./wrapper -->
	
    <div class="modal fade" id="genericModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width:850px!important">
            <div class="modal-content">
            <!---  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> --->
		        <div class="panel panel-primary">
	                <div class="panel-heading"></div>
		            <div class="panel-body" style="overflow:auto;">
	                </div>
                    <div class="panel-footer text-center">
                        <span class="dynobutton"></span>
		            </div>
		        </div>
            </div>
        </div>
    </div>
    
	<div class="notification bg-info"></div>
	<!--- <cfinclude template="/bin/controller/chat.cfm"> --->
	
	
	
   
    <!-- AdminLTE App -->
    <script src="<?=$this->config->item('adminlte');?>dist/js/app.min.js"></script>
	
	<script src="<?=$this->config->item('adminlte');?>plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?=$this->config->item('adminlte');?>plugins/datatables/dataTables.bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?=$this->config->item('adminlte');?>plugins/datatables/dataTables.bootstrap.css"> 

<script type="text/javascript" src="<?=$this->config->item('adminlte');?>plugins/daterangepicker/moment.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?=$this->config->item('cdn')?>js/jquery/jquery.ui.datepicker-it.js"></script>

<link rel="stylesheet" href="<?=$this->config->item('adminlte');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script type="text/javascript" src="<?=$this->config->item('adminlte');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js"></script>	
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
		 
		 <script src="<?=base_url();?>public/js/controller.js"></script>
		
		<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=$this->service->cdn_url()?>js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>


	<link rel="stylesheet" href="<?=base_url()?>public/css/theme.css" type="text/css" media="screen" />
  </body>
</html>