<?php
  date_default_timezone_set("Europe/Rome");
  $search = '';
  if ( isset ( $_POST['search'] ) ){
    $search = $_POST['search'];
  }
  $fstatus = '';
  if ( isset ( $_POST['status'] ) ){
    $fstatus = $_POST['status'];
  }
?>

<?php require_once ( 'giocate_filter.php' );?>

<div class="clearfix"></div>
  <br>
  <br>

  <!-- eof user filter -->
  <div class="clearfix"></div>
  <hr>

  <div class="col-lg-12" style="padding:0px!important;">
    <table id="table1" class="table table-striped table-bordered dataTable filter-result">
      <thead>
        <tr role="row">
          <th>#</th>
          <th class="sorting" tabindex="0">Cliente</th>
          <th class="sorting" tabindex="0">Email</th>
          <th>Registrazione</th>
          <th>Scontrino</th>
          <th>Punto Vendita</th>
          <th>Premio</th>
          <th>Stato</th>
          <th>Amministra</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
  <?php

    $uri = $this->service->web_url();
    $c = 1;
    $id = '';
          $isAdmin = 'hide';
          if ( $_SESSION['login']['user_type'] != 'O' ){
              $isAdmin = '';
          }

    if ( count($iscritti) > 0 ){
      foreach ( $iscritti as $r ){

        $filter = '/home/admin/public_html/tpbolton/public/users/upload/' . $r['customer_id']. '_' . $r['campaign_id'] . '_' . $r['receipt_nr'] . '*.*';
        $myimages = glob($filter);
        $img_icon = '';
        foreach ( $myimages AS $img ){
          $sc = explode('/',$img);
          $sc_img = $sc[count($sc)-1];
          $img_icon .= '<a href="http://www.tantipremi.it/public/users/upload/'.$sc_img.'" target="_blank"><span class="fa fa-picture-o pointer" title="Vedi scontrino" style="color:#000"></span></a>&nbsp';
        }

        if ( count($myimages) < 1 ){
          $img_icon = '<a href="http://www.tantipremi.it/public/users/upload/'.$r['receipt'].'" target="_blank"><span class="fa fa-picture-o pointer" title="Vedi scontrino"></span></a>';
        }

        $checked = $r['receipt_checked'];
        if ( $checked ){
          $receipt_checked = '<label class="label label-success pointer btn-action" data-controller="receipt-checked" data-id="'.$r['id'].'" data-checked="0">OK</label>';
        } else {
          $receipt_checked = '<label class="label label-danger pointer btn-action" data-controller="receipt-checked" data-id="'.$r['id'].'" data-checked="1">NO</label>';
        }

        $e_checked = $r['estratto_checked'];
        if ( $e_checked ){
          $estratto_checked = '<label class="label label-success pointer btn-action" data-controller="estratto-checked" data-id="'.$r['id'].'" data-checked="0">ES</label>';
        } else {
          $estratto_checked = '<label class="label label-danger pointer btn-action" data-controller="estratto-checked" data-id="'.$r['id'].'" data-checked="1">ES</label>';
        }

        $logic = $r['logic'];
        $send_coupon = 'send-coupon';
        if ( $logic == "coupon" ){
          $send_coupon = 'send-coupon-multi';
        }
        $status = $r['status'];
        $suggestedstatus = $r['status'];
        $blink = '';
        $rd = date("Ymmdd",strtotime($r['register_date']));
        $std = date("Ymmdd",strtotime($r['start_date']));
        $sd = date("Ymmdd",strtotime($r['receipt_date'].' +'.intval($r['register_days']).' days'));
        $ed = date("Ymmdd",strtotime($r['end_date']));
        $scd = date("Ymmdd",strtotime($r['receipt_date']));
        $sdate = date("Ymmdd",strtotime($r['receipt_date']));
        $shop_range_date = date("d/m/Y",strtotime($r['start_date'])).'-'.date("d/m/Y",strtotime($r['end_date']));
        $error_reg = '';
        $error_sc = '';
        $error_tooltip = '';
        $btn = 'Accetta';
        if ( $scd > $ed || $scd < $std ){

          $error_sc = "<span class='fa fa-exclamation-triangle faa-flash-animated'></span> <label class='label label-danger'>FUORI PERIODO</label>";
          $error_tooltip = "[FUORI PERIODO]";
          $suggestedstatus = 3;
          $blink = '';
          $btn = 'Conferma';
        }
                  if ( $rd > $sd ){
          $error_reg = "<span class='fa fa-exclamation-triangle faa-flash-animated'></span> <label class='label label-danger'>Oltre Limite</label>";
          $error_tooltip = "[OLTRE LIMITE]";
          $suggestedstatus = 2;
          $blink = '';
          $btn = 'Conferma';
        }

        $s = $this->config->item('status');
        $class = $this->config->item('status_label');
        $h = $this->config->item('status_help');
        $sendmail = $this->config->item('status_send_mail');
        $nextstatus = $this->config->item('status_flow');
        $btn = $this->config->item('status_btn');
        $label = '<label class="label label-'.$class[$suggestedstatus].' '.$blink.'" data-toggle="tooltip" title="'.$h[$status].' '.$error_tooltip.'">'.$s[$status].'</label>';
                  $disable_coupon = '';
        /*<br>
        <span class="fa fa-chevron-down"></span>
        <br>
        <button class="btn btn-'.$class[$status].' btn-sm btn-action" data-controller="update-status" data-id="'.$r['id'].'" data-status="'.$status.'" data-campaign="'.$r['campaign_id'].'" data-email="'.$r['email'].'" data-flow="'.$nextstatus[$status].'" data-sendmail="'.$sendmail[$status].'">'.$btn[$status].'</button>';
        */

                  if ( substr($r['id_image'],-3) == 'pdf' ){
                      if ( file_exists('/home/admin/public_html/tpbolton/public/users/download/'.$r['id_image']) ) {
                          $identita =  '<a href="'.$uri.'public/users/upload/'.$r['id_image'].'" target="_blank" title="Vedi documento"><span class="fa fa-file-pdf-o pointer"></span></a>';
                      } else {
                          $identita = '<span class="fa fa-question-circle danger pointer" title="Documento non disponibile"></span>';
                      }
                  } else {
                      $identita = '<span class="fa fa-picture-o pointer preview-receipt btn-action" data-controller="preview-doc" data-title="documento"  data-id="'.$uri.'public/users/upload/'.$r['id_image'].'" title="Vedi documento"></span>';
                  }

                  if ( substr($r['receipt'],-3) == 'pdf' ){
                      if ( file_exists('/home/admin/public_html/tpbolton/public/users/upload/'.$r['id_image']) ) {
                          $ricevuta = '<a href="'.$uri.'public/users/upload/'.$r['receipt'].'" target="_blank"><span class="fa fa-file-pdf-o pointer" title="Vedi scontrino"></span></a>';
                          $img_icon = $ricevuta;
                      } else {
                          $ricevuta = '<span class="fa fa-question-circle danger pointer" title="Scontrino non disponibile" style="color:red"></span>';
                      }
                  } else {
                      $ricevuta = '<span class="fa fa-picture-o pointer preview-receipt btn-action" data-controller="preview-doc" data-title="scontrino"  data-id="'.$uri.'public/users/upload/'.$r['receipt'].'" title="Vedi scontrino"></span>';
                  }

                  $datiVoucher = '';
                  $sendVoucher = '';
                  if ( $r['mail_sent'] ){
          if ( $logic != 'coupon'){
                      $datiVoucher = '<br><label class="label label-success pointer btn-action" data-controller="resend-coupon" data-id="'.$r['id'].'" data-coupon="'.$r['coupon'].'" data-coupon_date="'.date("d/m/Y",strtotime($r['released_date'])).'" data-email="'.$r['email'].'" data-prize="'.$r['prize_id'].'" data-title="'.$r['campaign'].'">Voucher del '.date("d/m/Y",strtotime($r['released_date'])).'</label>';
                      $sendVoucher = 'hide';
          }
                  }

                   if ( $logic == 'coupon' ){
                      if ( $r['multi_coupon_sent'] ){
                           $datiVoucher = '<br><label class="label label-success pointer" data-controller="resend-multi-coupon" data-id="'.$r['id'].'" data-coupon="'.$r['multi_coupon'].'" data-coupon_date="'.date("d/m/Y",strtotime($r['multi_coupon_date'])).'" data-email="'.$r['email'].'" data-prize="'.$r['prize_id'].'" data-title="'.$r['campaign'].'" title="'.$r['coupon_id'].'">Voucher del '.date("d/m/Y",strtotime($r['multi_coupon_date'])).'</label>';
                          $sendVoucher = 'hide';
                      }
                  }

                  $extra = '';
                  if ( $r['extra'] != '' ){
                      $extra = $r['extra'];
                  }
                  $phone = '';
                  if ( $r['phone'] != '' ){
                      $phone = '<span class="fa fa-phone"></span> '.$r['phone'].'<br>';
                  }
                  $estratto = '';
                  if ( $r['estratto'] ){
                    $estratto = '<span class="badge">Estratto</span>';
                  }
                  if ( isset($_POST['da_estrarre']) ){

                      $estratto = '<br><br><span class="estrazione-'.$r['id'].'"><button class="btn btn-sm btn-primary btn-action" data-controller="conferma-estrazione" data-id="'.$r['id'].'">Conferma Estrazione</button></span>';

                  }

        echo
          '<tr class="scontrino_'.$r['id'].'">
            <td>'.$c.'<br>'.$receipt_checked.'<br>'.$estratto_checked.'

                          </td>

            <td>'.$r['firstname'].' '.$r['lastname'].'<br><span class="fa fa-info-circle pointer"  data-toggle="tooltip" data-placement="right" title="'.$r['address_1'].' '.$r['zip'].' '.$r['city'].'""></span> '.$identita.'<br><span class="fa fa-mobile"></span> '.$r['mobile'].'</td>

            <td>'.$r['email'].'<br><br><strong>'.$extra.'</strong></td>



            <td><span class="register_'.$r['id'].'">'.date("d/m/Y",strtotime($r['register_date'])).'</span><br><span class="field-error">'.$error_reg.'</span></td>

            <!--<td>'.date("d/m/Y",strtotime($r['receipt_date'])).'</td>-->

            <td title="'.$shop_range_date.'"><span class="receipt_'.$r['id'].'">'.$r['receipt_nr'].'</span> <br><span class="receipt_date_'.$r['id'].'">'.date("d/m/Y",strtotime($r['receipt_date'])).'</span><br>'.$img_icon.'<br><small class="field-error">'.$error_sc.'</small><br></td>

            <td>'.$r['insegna'].' ('.$r['shop_state'].')<br>'.$r['organizzazione'].'</td>

            <td>'.$r['prize'].'</td>

            <td class="text-center">
              <div class="label_status_'.$r['id'].'">'.$label.'</div>
                              <br>

                              <button class="btn btn-sm btn-primary btn-action '.$sendVoucher.'" data-controller="'.$send_coupon.'" data-id="'.$r['campaign_id'].'" data-customer="'.$r['customer_id'].'" data-email="'.$r['email'].'" data-receipt="'.$r['id'].'" data-title="'.$r['campaign'].'" data-prize="'.$r['prize_id'].'" '.$disable_coupon.'><span class="fa fa-envelope"></span> Voucher</button>
                              '.$datiVoucher.'
            </td>
            <td><select class="form-control select-input" data-controller="update-status" data-id="'.$r['id'].'" data-status="'.$status.'" data-campaign="'.$r['campaign_id'].'" data-email="'.$r['email'].'" data-sendmail="'.$sendmail[$status].'">'.$options.'

            </select>
                    '.$estratto.'
                          </td>
                            <td>

                           <button class="btn btn-sm btn-primary btn-action pointer" data-shopid="'.$r['shop_id'].'" data-shop="'.$r['organizzazione'].'-'.$r['insegna'].'('.$r['provincia'].') - '.$r['indirizzo'].'" data-controller="edit-receipt" data-prize="'.$r['prize_id'].'" data-campaign="'.$r['campaign_id'].'" data-originator="partecipanti" data-receipt="'.$r['id'].'" data-customer="'.$r['firstname'].' '.$r['lastname'].'" data-registration="'.date("d/m/Y",strtotime($r['register_date'])).'" data-receipt_nr="'.$r['receipt_nr'].'" data-receipt_date="'.date("d/m/Y",strtotime($r['register_date'])).'" data-image="'.$r['receipt'].'" data-customer_id="'.$r['customer_id'].'"><span class="fa fa-edit"></span></button>
                          </td>
                          <td>
                          <button class="btn btn-sm btn-danger btn-action '.$sendVoucher.'" data-controller="delete-receipt" data-id="'.$r['id'].'" title="Elimina scontrino"><span class="fa fa-trash"></span></button>
                          </td>
        </tr>';
        $c++;

      }
    }

  ?>
  </tbody>
</table>

  <div class="modal fade" id="receiptModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" style="width:850px!important">
          <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="panel panel-primary">
                <div class="panel-heading">Modifica Giocata</div>
              <div class="panel-body" style="overflow:auto;">

                      <div class="col-lg-5">
                      <label>Cliente</label>
                      <input type="text" class="form-control customer" readonly>
                      </div>
                      <div class="col-lg-3">
                      <label>Scontrino nr</label>
                      <input type="text" class="form-control receipt_nr">
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-lg-4">
                      <label>Data Registrazione</label>
                      <input type="text" class="form-control register_date datepicker">
                      </div>
                      <div class="col-lg-4">
                      <label>Data Scontrino</label>
                      <input type="text" class="form-control receipt_date datepicker">
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-lg-6">
                      <br>
                      <br>
                      <label>Scontrino</label>
                      <input type="hidden" class="receipt_image">
                      <a href="#" class="open_scontrino" target="_blank"><img class="scontrino-img" style="max-height:200px;width:auto;"></a><button class="btn btn-default btn-open-upload">Sostituisci scontrino</button>
                      </div>
                      <div class="col-lg-6">
							<div class="col-lg-12 premi"></div>
					</div>
					<div class="clearfix"></div>

					<div class="col-lg-12">
						<label>Punto Vendita</label>
						<div class="puntovendita"></div><button class="btn btn-sm btn-primary btn-action btn-edit-receipt-puntovendita" data-controller="edit-receipt-puntovendita" data-id="">Modifica</button>
						</div>
						<div class="col-lg-4 provincia"></div>
						<div class="col-lg-8 insegne"><input type="text" class="shop_id" value=""></div>
                	</div>
                  <div class="panel-footer text-center">
                      <button class="btn btn-default" data-dismiss="modal">Chiudi</button>
                      &nbsp;
                      <button class="btn btn-success btn-action btn-save-receipt" data-controller="receipt-update" data-id="">Salva</button>
              </div>
          </div>
          </div>
      </div>
  </div>

  <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
  <div class="col-md-12">
    <br>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="panel panel-primary">
      <div class="panel-heading">Carica Scontrino</div>
      <div class="panel-body" style="overflow:auto;">
            <form action="<?php echo base_url();?>admin/upload_scontrino" id="myDropzone" class="dropzone">
        <input type="hidden" class="field" name="field" value="">
        <input type="hidden" class="customer_id" name="customer_id">
        <input type="hidden" class="campaign_id" name="id" value="">
        </form>

      </div>
      <div class="panel-footer text-right">
        <button class="btn btn-default" data-dismiss="modal">Chiudi</button>
      </div>
    </div>

  </div>
  </div>
</div>
</div>

<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/dropzone/dropzone.js"></script>




  <script>
$(function () {
  $('#table1').DataTable({
    "paging": true,
    "lengthChange": true,
  "pageLength": 50,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    "pagingType": "full_numbers",
 "language":
  {
      "sEmptyTable":     "Nessun dato presente nella tabella",
    "sInfo":           "Vista da _START_ a _END_ di _TOTAL_ elementi",
    "sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
    "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ".",
    "sLengthMenu":     "Visualizza _MENU_ elementi",
    "sLoadingRecords": "Caricamento...",
    "sProcessing":     "Elaborazione...",
    "sSearch":         "Cerca:",
    "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
    "oPaginate": {
      "sFirst":      "Inizio",
      "sPrevious":   "Precedente",
      "sNext":       "Successivo",
      "sLast":       "Fine"
    },
    "oAria": {
      "sSortAscending":  ": attiva per ordinare la colonna in ordine crescente",
      "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
    }
   }
  });
});

</script>

<script>
$(document).ready ( function(){
      $('.datepicker').datepicker( {
      dateFormat : "dd/mm/yy"
    });

      $('.btn-open-upload').click ( function(){
          $('#uploadModal').modal ('show' );
      });


      Dropzone.autoDiscover = false;
          $("#myDropzone").dropzone({
              dictDefaultMessage: "Clicca qui o trascina la tua immagine/file qui per caricarla",
              clickable: true,
              maxFilesize: 1,
      autoProcessQueue: true,
              uploadMultiple: false,
              addRemoveLinks: false,
      createImageThumbnails: true,
      acceptedFiles: 'image/*,application/pdf',
      dictFileTooBig: 'Immagine superiore a 1 MB. Impossibile caricare',
      init: function () {
              myDropzone = this;
        $('.dz-preview').remove();
              this.on("success", function(file) {
                    var filename = file['name'].split('.');
                    $('.receipt_image').val( $('.customer_id').val() +  '_sostituito.' + filename[1] );
                    $('.scontrino-img').attr ( 'src' ,  'http://www.tantipremi.it/public/users/upload/temp/' + $('.customer_id').val() +  '_sostituito.' + filename[1] )
          $('.dz-error-message').html('');
          $('.dz-preview').remove();
                $('#uploadModal').modal('hide');
              });
        this.on("error" , function ( e , errorMsg ){
          $('.dz-error-message').html(errorMsg);
          $('.dz-details').html('');
        });
          }

          });

 });
</script>
