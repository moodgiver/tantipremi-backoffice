
		<thead>
			<tr role="row">
			<th>#</th>
			<th class="sorting" tabindex="0">Cliente</th>
			<th class="sorting" tabindex="0">Email</th>
			<th>Telefono</th>
			<th>Registrazione</th>
			<!--- <th>Data scontrino</th> --->
			<th>Scontrino</th>
			<th>Punto Vendita</th>
			<th>Premio</th>
			<th>Stato</th>
			<th></th>
			</tr>
		</thead>
			
		<?php
			$uri = $this->service->web_url();
			$c = 1;
			$id = '';
			foreach ( $iscritti as $r ){
				$status = $r['status'];
				$rd = date("Ymmdd",strtotime($r['register_date']));
				$sd = date("Ymmdd",strtotime($r['start_date'].' +'.intval($r['register_days']).' days'));
				$ed = date("Ymmdd",strtotime($r['end_date']));
				$scd = date("Ymmdd",strtotime($r['receipt_date']));
				$error_reg = '';
				$error_sc = '';
				$btn = 'Accetta';
				if ( $rd > $sd ){
					
					$error_reg = "<span class='fa fa-exclamation-triangle'></span> Oltre limite";
					$status = 2;
					$btn = 'Conferma';
				}
				if ( $ed < $scd ){
					echo $ed.'-'.$scd;
					$error_sc = "<span class='fa fa-exclamation-triangle'></span> Fuori periodo";
					$status = 3;
					$btn = 'Conferma';
				} 
				
				$s = $this->config->item('status'); 
				$class = $this->config->item('status_label');
				$h = $this->config->item('status_help');
				$sendmail = $this->config->item('status_send_mail');
				$nextstatus = $this->config->item('status_flow');
				$btn = $this->config->item('status_btn');
				$label = '<label class="label label-'.$class[$status].'" data-toggle="tooltip" title="'.$h[$status].'">'.$s[$status].'</label>
				<br>
				<span class="fa fa-chevron-down"></span>
				<br>
				<button class="btn btn-'.$class[$status].' btn-sm btn-action" data-controller="update-status" data-id="'.$r['id'].'" data-status="'.$status.'" data-campaign="'.$r['campaign_id'].'" data-email="'.$r['email'].'" data-flow="'.$nextstatus[$status].'" data-sendmail="'.$sendmail[$status].'">'.$btn[$status].'</button>';
				
				echo 
					'<tr>
						<td>'.$c.'</td>
						
						<td>'.$r['firstname'].' '.$r['lastname'].'<br><span class="fa fa-question-circle pointer"  data-toggle="tooltip" data-placement="right" title="'.$r['address_1'].' '.$r['zip'].' '.$r['city'].'""></span> <span class="fa fa-picture-o pointer preview-receipt btn-action" data-controller="preview-doc" data-title="documento"  data-id="'.$uri.'public/users/upload/'.$r['doc'].'"></span></td>
						
						<td>'.$r['email'].'</td>
						
						<td><span class="fa fa-phone"></span> '.$r['phone'].'<br><span class="fa fa-mobile"></span> '.$r['mobile'].'</td>
						
						<td>'.date("d/m/Y",strtotime($r['register_date'])).'<br><small class="field-error">'.$error_reg.'</small></td>
						
						<!--<td>'.date("d/m/Y",strtotime($r['receipt_date'])).'</td>-->
						
						<td>'.$r['receipt_nr'].' <br>'.date("d/m/Y",strtotime($r['receipt_date'])).'<br><span class="fa fa-picture-o pointer preview-receipt btn-action" data-controller="preview-doc" data-title="scontrino"  data-id="'.$uri.'public/users/upload/'.$r['file'].'"></span><br><small class="field-error">'.$error_sc.'</small></td>
						
						<td>'.$r['insegna'].' ('.$r['shop_state'].')<br>'.$r['organizzazione'].'</td>
						
						<td>'.$r['prize'].'</td>
						
						<td class="text-center">
							<div class="label_status_'.$r['id'].'">'.$label.'</div>
						</td>
						<td></td>
				</tr>';
				$c++;
			}
		?>
	