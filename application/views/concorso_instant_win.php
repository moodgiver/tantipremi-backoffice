<div class="col-lg-12">
	<div class="col-lg-4">
    <label>Organizzazione</label>
    <select class="organizzazione form-control">
      <option value="">Seleziona ...</option>
    <?php
      foreach ( $organizzazioni AS $org ){
        echo '<option value="'.$org['organizzazione'].'">'.$org['organizzazione'].'</option>';
      }
    ?>
    </select>

  </div>
  <div class="col-lg-2">
    <label>Dal</label>
    <input class="form-control filter-start-date datepicker startDateInstantWin" name="startDateInstantWin" value="<?=date('d/m/Y', strtotime("-7 days"));?>">
  </div><div class="col-lg-1">

  </div>
  <div class="col-lg-2">
    <label>Al</label>
    <input class="form-control filter-end-date datepicker" name="endDateInstantWin" value="<?=date('d/m/Y',now());?>">

  </div>
  <div class="col-lg-2">

	</div>
  <div class="clearfix"></div>
  <div class="col-lg-3">
    <label>Da assegnare</label>
    <input class="form-control instantWin_nr" value="1">
  </div>
	<div class="col-lg-2">
		<label>&nbsp;</label>
		<button class="btn btn-primary btn-action btn-block btn-add-instantwin btn-flat" data-id="<?=$id?>">Aggiungi</button>
		<input type="hidden" class="campaign_id" value="<?=$id?>">
	</div>
	<div class="col-lg-2">
		<label>&nbsp;</label>
		<button class="btn btn-primary btn-block btn-filter-solo-vincite">Vedi Vincite</button>
	</div>
	<div class="col-lg-2">
		<label>&nbsp;</label>
		<button class="btn btn-warning btn-block btn-filter-no-vincite">Non assegnate</button>
	</div>
	<div class="col-lg-3 pull-right">
		<label>&nbsp;</label>
		<a href="export_instant_win/<?=$id?>"><button class="btn btn-success btn-block abtn-instant-win-assegnati" data-controller="instant-win-assegnati" data-id="<?=$id?>"><span class="fa fa-file-excel-o"></span> Esporta Instant Win </button></a>
	</div>
</div>
<div class="clearfix" style="margin:20px 0 20px 0"></div>

<div class="col-lg-12">
  <table class="table table-bordered">
    <thead>
      <th>*</th>
      <th>Organizzazione</th>
      <th>Data Vincita</th>
      <th>Assegnata</th>
      <th>Data Assegnazione</th>
      <th>Scontrino ID</th>
    </thead>
    <tbody>
      <?php
      if ( $instant_wins ){
        foreach ( $instant_wins AS $iw ){
          $dateAssigned = '';
          $assegnata = 'NO';
          $classe = "default";
          $scontrino = '';
          if ( $iw['date_assigned'] ){
            $dateAssigned = date('d/m/Y',strtotime($iw['date_assigned']));

          }
          if ( $iw['is_assigned'] ){
            $assegnata = 'SI';
            //$assegnata = $iw['lastname'].' '.$iw['firstname'].'<br>'.$iw['email'];
            $classe = 'success';
          }
          if ( $iw['receipt_uuid'] ){
            $scontrino = '<a href="http://www.tantipremi.it/public/users/upload/'.$iw['receipt_uuid'].'" target="_blank"><span class="fa fa-picture-o"></span></a>&nbsp;<button class="btn btn-sm  btn-view-scontrino" data-controller="instantwin-detail"  data-receipt="'.$iw['receipt_uuid'].'" data-row="'.$iw['instant_win_id'].'">Vedi</button>';
          } else {
		  	$scontrino = '';
		  }
          echo '<tr class="righe '.$classe.'">';
          echo '<td>'.$iw['instant_win_id'].'</td>';
          echo '<td>'.$iw['organizzazione'].'</td>';
          echo '<td>'.date('d/m/Y' , strtotime($iw['date_string'])).'</td>';
          echo '<td>'.$assegnata.'</td>';
          echo '<td>'.$dateAssigned.'</td>';
          echo '<td>'.$scontrino.'</td></tr>';
		  echo '<tr class="righe-dettaglio righe-'.$classe.' table-scontrini table-scontrini_'.$iw['instant_win_id'].'">
				<td colspan="6">
				<table class="table table-bordered  scontrino_'.$iw['instant_win_id'].'"></table>
		  		</td></tr>';
        }
      } else {
        echo '<tr><td colspan="6"><h3>Nessuna Instant Win definita</h3></td></tr>';
      }
      ?>
  </tbody>
  </table>

  <?php
  //print_r( $instant_wins )
  ?>
</div>

<script>
$(document).ready(function(){

  $('.datepicker').datepicker( {
    dateFormat : "dd/mm/yy"
  });


  $('.btn-view-scontrino').on('click',function(){
  	var id = $(this).data('row');
	var uuid = $(this).data('receipt');
  	$.post('ajax',
		{
			action: 'instantwin-detail',
			uuid: uuid
		}, function ( result ){
			//$('.table-scontrini').addClass('hide');
			$('.scontrino_' + id).removeClass('hide');
			$('.scontrino_' + id).html(result);
		}
	);
  });

	$('.btn-add-instantwin').on('click',function(){
		var id = $(this).data('id');
		$.post('ajax',
			{
				action: 'add-instant-win',
				campaign_id: id,
				organizzazione: $('.organizzazione').val(),
				qty: $('.instantWin_nr').val(),
				from: $('.filter-start-date').val(),
				to: $('.filter-end-date').val()
			},function(result){
				alert('Instant win assegnati');
				$.post('ajax',
					{
						action: 'instantwin-concorso',
						id: id
					}, function(result){
						$('.content').html(result);
					}
				)
			}
		)
	})

	$('.btn-filter-solo-vincite').on('click',function(){
		$('.righe').addClass('hide');
		$('.righe-dettaglio').addClass('hide');
		$('.success').removeClass('hide');
		$('.righe-success').removeClass('hide');
	})

	$('.btn-filter-no-vincite').on('click',function(){
		$('.righe').removeClass('hide');
		$('.righe-dettaglio').removeClass('hide');
		$('.success').addClass('hide');
		$('.righe-success').addClass('hide');
	})

});
</script>
