 <!-- Sidebar Menu -->
<ul class="sidebar-menu">
	<!--- <li class="header">Dashboard</li> --->
   	<!-- Optionally, you can add icons to t <li class="header">Dashboard</li>he links -->
	<li class="treeview active">
     	<a href="#"><i class="fa fa-diamond"></i> <span>Manager Concorsi</span> <i class="fa fa-angle-left pull-right"></i></a>
         <ul class="treeview-menu concorsi-menu">
        	<li class="menu btn-action" data-controller="menu" data-menu="concorsi" data-title="Concorsi">
				<a href="#"><span class="fa fa-table"></span> Concorsi</a>
			</li>
			<?php if ( $_SESSION['login']['user_type'] != 'O' ){
				echo '<li class="menu btn-action" data-controller="menu" data-menu="crea-concorso" data-title="Nuovo Concorso"><a href="#"><span class="fa fa-plus"></span> Crea Concorso</a>
			</li>';
			}
			?>
		</ul>
	</li>
    <?php if ( $_SESSION['login']['user_type'] != 'O' ){
        echo '<li class="menu btn-action" data-controller="menu" data-menu="registrazioni" data-title="Verifica registrazioni clienti">
	     	<a href="#"><i class="fa fa-server"></i> <span>Clienti</span></a></li>';
    }
    ?>
	<?php if ( $_SESSION['login']['user_type'] != 'O' ){
		echo '<li class="treeview">
	     	<a href="#"><i class="fa fa-users"></i> <span>Manager Utenti</span> <i class="fa fa-angle-left pull-right"></i></a>
	         <ul class="treeview-menu admin-utenti">
	        	<li class="menu btn-action" data-controller="menu" data-menu="utenti" data-title="Utenti">
					<a href="#"><span class="fa fa-users"></span> Utenti</a>
				</li>
			</ul>
		</li>
        <li class="menu btn-action" data-controller="menu" data-menu="settings" data-title="Impostazioni"><a href="#"><i  class="fa fa-cogs"></i> <span>Impostazioni</span></a></li>';
		}
		
	?>
	<li class="treeview"><a href="logout"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	
	
</ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>
	  
	  
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1 class="label label-primary">
            <span class="title-header">Dashboard</span>
            <span class="title-description"></span>
			<span class="fa fa-spinner fa-spin hide working"></span>
          </h1>
        </section>
		<div class="clearfix"></div>
        <section>
			<div class="row content">
	