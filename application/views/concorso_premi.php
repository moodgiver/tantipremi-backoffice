<div class="col-lg-12 pull-right">
	<div class="col-lg-11"></div>
	<div class="col-lg-1">
	<button class="btn btn-primary btn-block btn-add-prize btn-flat">Aggiungi</button>
	</div>
</div>
<br>
<br>
<div class="col-lg-12 hide prize-add-wrapper">
	<div class="col-lg-3">
				<img src="">
				<br>
				<small>Dopo l'inserimento delle informazioni base sar&agrave; possibile caricare l'immagine</small>
			</div>
			<div class="col-lg-9 whiteBG" style="padding:10px">
			<?php
				echo '
				<label>Premio</label>
				<input type="text" class="form-control p_name_0" name="prize" value="">
				<br>
				<textarea class="prize_description p_desc_0" name="prize_description" style="width:100%;color:#000;padding:0;"></textarea>
				<div class="pull-right">
					<button class="btn btn-primary btn-action" data-controller="add-prize" data-id="'.$promo[0]['campaign_id'].'">Salva</button>
				</div>';
				?>
			</div>
			<div class="clearfix"></div>
			<br><br>
</div>
<div class="col-lg-12">
	<?php 
		
	
		foreach ( $promo as $row ){
			$btnLogic = '';
			if ( $promo[0]['logic'] == 'coupon' ){
					$btnLogic =  '<button class="btn btn-flat btn-warning btn-action" data-controller="create-coupons" data-id="'.$promo[0]['campaign_id'].'" data-code="'.$promo[0]['code'].'"  data-prize="'.$row['prize_id'].'">Crea coupons</button>&nbsp;<button class="btn btn-flat btn-warning btn-action" data-controller="import-coupons" data-id="'.$promo[0]['campaign_id'].'" data-code="'.$promo[0]['code'].'"  data-prize="'.$row['prize_id'].'">Importa coupons</button>&nbsp;<button class="btn btn-flat btn-warning btn-action  coupons_'.$row['prize_id'].'" data-controller="export-coupons" data-id="'.$promo[0]['campaign_id'].'" data-code="'.$promo[0]['code'].'"  data-prize="'.$row['prize_id'].'" data-name="'.$row['prize'].'">Esporta coupons</button>';
			}
		
			if ( $row['prize_image'] == '' ){
				$img = '<button class="btn btn-default btn-flat btn-upload-image" data-field="prize" data-id="'.$row['prize_id'].'" data-folder="public">Foto</button>';
			} else {
				$img = '<img src="'.$this->service->web_url().'public/img/'.$row['code'].'/'.$row['prize_image'].'" class="btn-upload-image" data-field="prize" data-id="'.$row['prize_id'].'" data-folder="public" width="200">';
			}
			echo '<div class="col-lg-3 text-center">
				'.$img.'
			</div>
			<div class="col-lg-9 whiteBG" style="padding:10px">
				<label>Premio</label>
				<input type="text" class="form-control p_name_'.$row['prize_id'].'" name="prize" value="'.$row['prize'].'">
				<br>
				<textarea class="prize_description p_desc_'.$row['prize_id'].'" name="prize_description" style="width:100%;color:#000;padding:0;">'.$row['prize_description'].'</textarea>
				<div class="pull-right">
					'.$btnLogic.'&nbsp;<button class="btn btn-danger btn-action" data-controller="delete-prize" data-id="'.$row['prize_id'].'">Elimina</button>&nbsp;<button class="btn btn-primary btn-action" data-controller="save-prize" data-id="'.$row['prize_id'].'">Salva</button>
				</div>
			</div>
			<div class="clearfix"></div><br><br>
			';
			
		}
	?>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="col-md-12">
			<br>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="panel panel-primary">
				<div class="panel-heading">Immagine Premio</div>
				<div class="panel-body" style="overflow:auto;">
	      			<form action="<?php echo base_url();?>admin/upload" id="myDropzone" class="dropzone">
					<input type="hidden" class="field" name="field" value="">
					<input type="hidden" class="folder" name="folder">
					<input type="hidden" name="code" value="<?=$promo[0]['code']?>">
					<input type="hidden" class="campaign_id" name="campaign_id" value="<?=$promo[0]['campaign_id']?>">
					<input type="hidden" class="prize_id" name="id" value="">
					</form>
					
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-default" data-dismiss="modal">Chiudi</button>
				</div>
			</div>
			
		</div>
    </div>
  </div>
</div>

<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/dropzone/dropzone.js"></script>


<script src="<?=$this->config->item('adminlte')?>plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<link rel="stylesheet" href="<?=$this->config->item('adminlte');?>plugins/colorpicker/bootstrap-colorpicker.min.css"> 



<script>
$(document).ready ( function(){
	$('.btn-add-prize').click ( function(){
		$('.prize-add-wrapper').removeClass('hide');
	});

	$(".prize_description").wysihtml5();
	
	$('.btn-upload-image').click(function(){
		$('.field').val ( $(this).data('field') );
		$('.folder').val ( $(this).data('folder') );
		$('.prize_id').val( $(this).data('id') );
		$('#uploadModal').modal('show');
	});

	Dropzone.autoDiscover = false;
            $("#myDropzone").dropzone({
                dictDefaultMessage: "Clicca qui o trascina la tua immagine/file qui per caricarla",
                clickable: true,
                maxFilesize: 1,
				autoProcessQueue: true,
                uploadMultiple: false,
                addRemoveLinks: false,
				createImageThumbnails: true,
				acceptedFiles: 'image/*,application/pdf',
				dictFileTooBig: 'Immagine superiore a 1 MB. Impossibile caricare',
				init: function () {
            		myDropzone = this;
					$('.dz-preview').remove();
		            this.on("success", function(file) {
					  $('.dz-error-message').html('');
					  $('.dz-preview').remove();
        		      $('#uploadModal').modal('hide');
					  	$.post ( 'ajax' ,
							{
					  			action: 'premi',
								id: $('.campaign_id').val()
					  		} , function ( result ){
								$('.content').html(result);
							}
						)
	            	});
					this.on("error" , function ( e , errorMsg ){
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});
       			}
			
            });
	
});
</script>