<?php
  $pv_url = '//puntivendita.tantipremi.it/public/shops/foto';
?>
<div style="position:absolute;right:30px; top:70px"><button class="btn btn-primary menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Contest PV" data-action="contest-concorso" data-id="<?=$shops[0]['campaign_id']?>">Indietro</button></div>
<div class="col-lg-12">
		<table id="tableshops" class="table table-striped table-bordered dataTable filter-result">
			<thead>
				<tr role="row">
				<th>#</th>
				<th class="sorting" tabindex="0">Organizzazione</th>
				<th class="sorting" tabindex="0">Punto Vendita</th>

        <th class="sorting" tabindex="0">Regione</th>
        <th class="sorting" tabindex="0">Comune</th>
        <th class="sorting" tabindex="0">Provincia</th>
        <th>Indirizzo</th>
        <th>Inizio</th>
				<th>Fine</th>

				</tr>
			</thead>
			<tbody>
			<?php
				if ( count($shops) > 0 ){
						echo '<tr class="pointer">
							<td></td>

							<td>'.$shops[0]['organizzazione'].'</td>
              <td><strong>'.$shops[0]['insegna'].'</strong><br>'.$shops[0]['nome'].' '.$shops[0]['cognome'].'</td>
							<td>'.$shops[0]['regione'].'<br>'.$shops[0]['email'].'</td>
              <td>'.$shops[0]['comune'].'</td>
							<td>'.$shops[0]['provincia'].'</td>
              <td>'.$shops[0]['indirizzo'].'</td>
							<td>'.$shops[0]['start_date'].'</td>
							<td>'.$shops[0]['end_date'].'</td>
						</tr>';
            echo '<tr>
              <td colspan="9">';
              foreach ( $shops as $r ){
                echo '<div class="col-lg-6 text-center" style="margin-bottom:20px"><a href="'.$pv_url.'/'.$r['image']
                .'" target="_blank"><img src="'.$pv_url.'/'.$r['image']
                .'" style="max-height:200px;width:auto;"></a></div>';
              }
              echo '</td></tr>';
				}

			?>
			</tbody>
		</table>
