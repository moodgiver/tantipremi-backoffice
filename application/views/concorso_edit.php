<div class="col-lg-12 jumbotron">
	
	<div class="col-lg-4">
		<label>Nome Concorso</label>
		<input type="text" class="form-control required campaign_name" name="campaign" value="<?=$promo[0]['campaign']?>">
	</div>
	
	<div class="col-lg-2">
		<label>Codice</label>
		<input type="text" class="form-control required campaign_code" name="campaign" value="<?=$promo[0]['code']?>" readonly>
		<div class="field_help">Il codice non &eacute; modificabile</div>
	</div>
	
	<div class="col-lg-6">
		<label>URL</label>
		<input type="text" class="form-control campaign_url" name="campaign" value="<?=$promo[0]['url']?>">
		<div class="field_help">Indicare l'URL della gestione esterna quando prevista</div>
	</div>
	
	<div class="clearfix"></div>
	<br><br>
	
	<div class="col-lg-2">
		<label>Data Inizio</label>
		<input type="text" class="form-control required campaign_start datepicker" name="start" value="<?=date('d/m/Y',strtotime($promo[0]['start']))?>">
	</div>
	
	<div class="col-lg-2">
		<label>Data Fine</label>
		<input type="text" class="form-control required campaign_end datepicker" name="end" value="<?=date('d/m/Y',strtotime($promo[0]['end']))?>">
	</div>
	
	<div class="col-lg-2">
		<label>Limite registrazione (giorni)</label>
		<input type="text" class="form-control required campaign_days" name="days" value="<?=$promo[0]['register_days']?>" maxlength="3">
		<div class="field_help">Inserire 0 per nessun limite</div>
	</div>
	
	<div class="col-lg-2">
		<label>Logica Concorso</label>
		<select class="form-control logic" name="logic">
			<option value="coupon" <?php if ( $promo[0]['logic'] == 'coupon' ){ echo 'selected'; }?>>coupon</option>
			<option value="buoni">Invio buoni</option>
		</select>
	</div>
	
	<div class="col-lg-2">
		
		<?php 
			$checked = '';
			if ( $promo[0]['isActive'] == '1' ){
				$checked = 'checked';
			}
		?>
		<label>Attivo</label><br>
		<input type="checkbox" class="campaign_active" name="active" <?=$checked?>>
	</div>
	
	<div class="clearfix"></div>
	<br><br>
	
	<div>
	
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#abstract" aria-controls="abstract" role="tab" data-toggle="tab">Abstract</a></li>
			<li role="presentation"><a href="#howto" aria-controls="howto" role="tab" data-toggle="tab">Come fare</a></li>
			<li role="presentation"><a href="#descrizione" aria-controls="descrizione" role="tab" data-toggle="tab">Descrizione</a></li>
			<li role="presentation"><a href="#faq" aria-controls="faq" role="tab" data-toggle="tab">FAQ</a></li>
			
		</ul>
		
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="abstract">
				<div class="col-lg-12">
					<input type="text" class="form-control campaign_abstract" id="campaign_abstract" name="abstract" maxlength="255" value="<?=$promo[0]['abstract']?>">
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="howto">
				<div class="col-lg-12">
					<textarea class="ckeditor campaign_howto" name="campaign_howto" id="campaign_howto" style="width: 100%; height: 450px; font-size: 14px; line-height: 18px; border: 1px solid ##dddddd; padding: 10px;"><?=$promo[0]['howto']?></textarea>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="descrizione">
				<div class="col-lg-12">
					<textarea class="ckeditor campaign_description" id="campaign_description" name="campaign_description" style="width: 100%; height: 450px; font-size: 14px; line-height: 18px; border: 1px solid ##dddddd; padding: 10px;"><?=$promo[0]['description']?></textarea>
			
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="faq">
				<div class="col-lg-12">
					<textarea class="ckeditor campaign_faq" id="campaign_faq" name="campaign_faq" style="width: 100%; height: 450px; font-size: 14px; line-height: 18px; border: 1px solid ##dddddd; padding: 10px;"><?=$promo[0]['faq']?></textarea>
			
				</div>
			</div>
			
		</div>
	
	</div>
	<div class="clearfix"></div>
	<br><br>
	<div class="col-lg-12 text-center">
		<button class="btn btn-flat btn-primary btn-action" data-controller="settings-save" data-id="<?=$promo[0]['campaign_id']?>">Salva</button>
	</div>
</div>


<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script>
$(document).ready ( function(){
	$('.datepicker').datepicker( { 
		dateFormat : "dd/mm/yy" 
	});
	
	$.datepicker.setDefaults( $.datepicker.regional[ "it" ] );
	
	//$(".campaign_abstract").wysihtml5();
	//$(".campaign_description").wysihtml5();
	//$(".campaign_faq").wysihtml5();
	CKEDITOR.replace('campaign_howto');
	CKEDITOR.replace('campaign_description');
	CKEDITOR.replace('campaign_faq');
	
	
	
	$('.required').each(function(){
		if ( $(this).val() != '' ){
			console.log ( $(this).val() );
			$(this).removeClass('required');
		}
	});
});
</script>