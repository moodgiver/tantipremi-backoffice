<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TANTIPREMI</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
	<!-- jQuery 2.2.4 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	
	<!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<!--- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
 --->
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<!-- Theme style -->
    <link rel="stylesheet" href="<?=$this->config->item('adminlte');?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
	<link rel="stylesheet" href="<?= $this->config->item('adminlte');?>plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?= $this->config->item('adminlte');?>dist/css/skins/skin-blue.min.css">
  </head>
  <body class="hold-transition skin-blue sidebar-mini mylogin" style="padding-top:10%">
	<div class="col-lg-4"></div>
	<div class="col-lg-4 col-xs-12 jumbotron">
		
		<div class="col-lg-12 text-center">
	 		<img src="<?=base_url()?>public/logo-bolton.jpg"><br><br>
		</div>	
			<table class="table">
			<tbody><tr>
			<td><label>Utente</label></td>
			<td><input type="text" class="form-control input-lg ulogin"></td>
			</tr>
			<tr>
			<td><label>Password</label></td>
			<td><input type="password" class="form-control input-lg upwd"></td>
			</tr>
			</tbody></table>
		<div class="col-lg-12 text-center">
			<button type="button" class="btn btn-success btn-action" data-controller="login" data-action="login">Accedi</button>
		</div>	
		<!--
		<h1>SERVIZIO IN MANUTENZIONE</h1>
		<h4>E' in corso un aggiornamento del sistema. Si prega di riprovare pi&uacute; tardi</h4> -->
		<!--- <?=print_r($_SESSION)?> --->
	</div>
	<div class="col-lg-4"></div>
	<link rel="stylesheet" href="<?=base_url()?>public/css/theme.css" type="text/css" media="screen" />
	
   
    <!-- AdminLTE App -->
    <script src="<?=$this->config->item('adminlte');?>dist/js/app.min.js"></script>
	
	<script src="<?=$this->config->item('adminlte');?>plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?=$this->config->item('adminlte');?>plugins/datatables/dataTables.bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?=$this->config->item('adminlte');?>plugins/datatables/dataTables.bootstrap.css"> 

<script type="text/javascript" src="<?=$this->config->item('adminlte');?>plugins/daterangepicker/moment.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="<?=$this->config->item('cdn')?>js/jquery/jquery.ui.datepicker-it.js"></script>

<link rel="stylesheet" href="<?=$this->config->item('adminlte');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<script type="text/javascript" src="<?=$this->config->item('adminlte');?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.8/standard/ckeditor.js"></script>	
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
		 
		 <script src="<?=base_url();?>public/js/controller.js"></script>
		
		<!-- Add fancyBox -->
<link rel="stylesheet" href="<?=$this->service->cdn_url()?>js/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
  </body>
</html>			