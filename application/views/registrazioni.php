    <?php
        date_default_timezone_set("Europe/Rome"); 
		$search = '';
		if ( isset ( $_POST['search'] ) ){
			$search = $_POST['search'];
		}
		$fstatus = '';
		if ( isset ( $_POST['status'] ) ){
			$fstatus = $_POST['status'];
		}
	?>

	<!-- user filter start -->
	<div class="col-lg-1">
	<label>Cerca</label>
	</div>
	<div class="col-lg-6">
        <input type="text" class="form-control search-input" placeholder="nome o email" value="<?php if ( isSet ( $_POST['search'] ) ){ echo $_POST['search']; }?>">
	</div>
    
    <!--
    <div class="col-lg-1">
        <label>Dal</label> 
    </div>
    <div class="col-lg-2">
        <input class="form-control filter-start-date datepicker startDateIscritti" name="startDateIscritti" value="<?=date('d/m/Y', strtotime("-7 days"));?>">
    </div>
     <div class="col-lg-1">
        <label>Al</label> 
    </div>

    <div class="col-lg-2">
        <input class="form-control filter-end-date datepicker" name="endDateIscritti" value="<?=date('d/m/Y',now());?>">
    </div>
    -->
    <div class="col-lg-1">
        <button class="btn btn-primary btn-action" data-controller="registrazioni-filtra">Filtra</button>
    </div>

	
	<div class="clearfix"></div>
    <br>
    <br>
    <table id="table1" class="table table-striped table-bordered dataTable filter-result">
		<thead>
			<tr role="row">
			<th>#</th>
			<th class="sorting" tabindex="0">Cliente</th>
			<th class="sorting" tabindex="0">Email</th>
			<th>Telefono</th>
			<th class="sorting" tabindex="0">Registrazione</th>
			<th>Indirizzo</th>
			<th>Citt&aacute;</th>
			
			<th>Amministra</th>
			</tr>
		</thead>
		<tbody>
    <?php 
        if ( isSet($customers) ){
           foreach ( $customers as $row ){
                echo '
                    <tr class="customers customer_'.$row['customer_id'].'">
                        <td>'.$row['customer_id'].'</td>
                        <td>'.strtoupper($row['lastname']).' '.strtoupper($row['firstname']).'</td>
                        <td>'.$row['email'].'</td>
                        <td>'.$row['phone'].' - '.$row['mobile'] .'</td>
                        <td>'.$row['registration_date'].'</td>
                        <td>'.strtoupper($row['address_1']).' '.$row['address_nr'].'</td>
                        <td>'.strtoupper($row['city']).' ('.strtoupper($row['state']).')</td>
                        <td>
                            <button class="btn btn-sm btn-action btn-primary" data-controller="customer-scontrini" data-id="'.$row['customer_id'].'" data-active=""><span class="fa fa-tag" title="Scontrini"></span></button>
                            <button class="btn btn-sm btn-action btn-default" data-controller="customer-edit" data-id="'.$row['customer_id'].'"><span class="fa fa-user" title="Modifica anagrafica"></span></button>
                        </td>
                    </tr>';       
           }
        }
    ?>
        </tbody>
    </table>
    <input type="hidden" class="scontrini" value="">
    <table id="table-scontrini" data-status="" class="hide table table-striped table-bordered dataTable filter-result" style="margin-top:-20px!important">
		<thead>
			<tr role="row">
			<th>Scontrino Nr.</th>
			<th class="sorting" tabindex="0">Data scontrino</th>
			<th class="sorting" tabindex="0">Data Registrazione</th>
            <th class="sorting" tabindex="0">Promozione</th>
			<th>Premio</th>
			<th>Punto Vendita</th>
			<th>Vedi</th>
            <th><button class="btn btn-sm btn-primary btn-action btn-add-receipt'.$hide.'" data-controller="add-receipt" data-customer="<?=$customers[0]['firstname']?>  <?=$customers[0]['lastname']?>" data-registration="<?=date("d/m/Y")?>"  data-receipt_date="<?=date("d/m/Y")?>" data-customer_id="<?=$customers[0]['customer_id']?>">
            <span class="fa fa-plus"></span> Aggiungi</button>
            </th>
			</tr>
		</thead>
		<tbody class="scontrini-body">
        </tbody>
    </table>
    
    <input type="hidden" class="customer_id">
    <div class="modal fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width:850px!important">
            <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <div class="panel panel-primary">
	                <div class="panel-heading">Modifica Utente</div>
		            <div class="panel-body" style="overflow:auto;">
                       
                        <div class="col-lg-3">
                        <label>Nome</label>
                        <input type="text" class="form-control firstname">
                        </div>
                        <div class="col-lg-3">
                        <label>Cognome</label>
                        <input type="text" class="form-control lastname">
                        </div>
                        <div class="col-lg-3">
                        <label>Nato il</label>
                        <input type="text" class="form-control dob datepicker">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-5">
                        <label>Indirizzo</label>
                        <input type="text" class="form-control address_1">
                        </div>
                        <div class="col-lg-1">
                        <label>Nr</label>
                        <input type="text" class="form-control address_nr">
                        </div>
                        <div class="col-lg-3">
                        <label>Citt&agrave;</label>
                        <input type="text" class="form-control city">
                        </div>
                        <div class="col-lg-1">
                        <label>PV</label>
                        <input type="text" class="form-control state">
                        </div>
                        <div class="col-lg-2">
                        <label>CAP</label>
                        <input type="text" class="form-control zip">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-4">
                        <label>Telefono</label>
                        <input type="text" class="form-control phone">
                        </div>
                        <div class="col-lg-4">
                        <label>Mobile</label>
                        <input type="text" class="form-control mobile">
                        </div>
                        <div class="col-lg-4">
                        <label>C.F.</label>
                        <input type="text" class="form-control cf">
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="col-lg-4">
                        <label>Email</label>
                        <input type="text" class="form-control email">
                        </div>
                        <div class="col-lg-4">
                        <label>Password</label>
                        <input type="text" class="form-control password" placeholder="nuova password">
                        </div>
                        
                        <div class="col-lg-4">
                        <label>Registrazione</label>
                        <input type="text" class="form-control registration_date datepicker">
                        </div>
	                </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-default" data-dismiss="modal">Chiudi</button>
                        &nbsp;
                        <button class="btn btn-success btn-action btn-save-user" data-controller="customer-save" data-id="">Salva</button>
		            </div>
		        </div>
            </div>
        </div>
    </div>    
    
    
    <div class="modal fade" id="receiptModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width:850px!important">
            <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <div class="panel panel-primary">
	                <div class="panel-heading">Modifica Giocata</div>
		            <div class="panel-body scontrino-body" style="overflow:auto;">
                       
                        <div class="col-lg-5">
                        <label>Cliente</label>
                        <input type="text" class="form-control customer" readonly>
                        </div>
						
						<div class="col-lg-4">
                        <label>Data Registrazione</label>
                        <input type="text" class="form-control register_date datepicker">
                        </div>
						<div class="clearfix"></div>
                        <div class="col-lg-3">
                        <label>Scontrino nr</label>
                        <input type="text" class="form-control receipt_nr">
                        </div>
						
                        <div class="col-lg-4">
                        <label>Data Scontrino</label>
                        <input type="text" class="form-control receipt_date datepicker">
                        </div>
						<div class="col-lg-5">
						<label>Extra (Frase,ecc.)</label>
						<input type="text" class="form-control receipt_extra">
						</div>		
                        <div class="clearfix"></div>
                        <div class="col-lg-6">
                        <br>
                        <br>
                        <label>Scontrino</label>
                        <input type="hidden" class="receipt_image">
                        <img class="scontrino-img" width="150"><button class="btn btn-default btn-open-upload">Scontrino</button>
                        </div>
                       	
                        <div class="clearfix"></div>
                        
						
                        <div class="concorsi"></div>
                        <div class="premi"></div>
                        
                         <div class="clearfix"></div>
                        <div class="col-lg-4 province"></div>
                        <div class="col-lg-4 negozio"></div>
	                </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-default" data-dismiss="modal">Chiudi</button>
                        &nbsp;
                        <button class="btn btn-success btn-action btn-save-receipt" data-controller="receipt-update" data-id="">Salva</button>
		            </div>
		        </div>
            </div>
        </div>
    </div> 
    
    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="col-md-12">
			<br>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="panel panel-primary">
				<div class="panel-heading">Carica Scontrino</div>
				<div class="panel-body" style="overflow:auto;">
	      			<form action="<?php echo base_url();?>admin/upload_scontrino" id="myDropzone" class="dropzone">
					<input type="hidden" class="field" name="field" value="">
					<input type="hidden" class="customer_id" name="customer_id">
                    <input type="hidden" class="image_uuid" name="uuid" value="<?=$this->uuid->v4();?>">
					<input type="hidden" class="campaign_id" name="id" value="">
					</form>
					
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-default" data-dismiss="modal">Chiudi</button>
				</div>
			</div>
			
		</div>
    </div>
  </div>
</div>
    
<script type="text/javascript" src="<?=$this->service->cdn_url()?>js/dropzone/dropzone.js"></script>    
<script>
$(document).ready ( function(){
    $('.datepicker').datepicker( { 
	    dateFormat : "dd/mm/yy" 
	});
    
    $('.btn-open-upload').click ( function(){
        $('#uploadModal').modal ('show' );
    });
    
    Dropzone.autoDiscover = false;
            $("#myDropzone").dropzone({
                dictDefaultMessage: "Clicca qui o trascina la tua immagine/file qui per caricarla",
                clickable: true,
                maxFilesize: 1,
				autoProcessQueue: true,
                uploadMultiple: false,
                addRemoveLinks: false,
				createImageThumbnails: true,
				acceptedFiles: 'image/*,application/pdf',
				dictFileTooBig: 'Immagine superiore a 1 MB. Impossibile caricare',
				init: function () {
            		myDropzone = this;
					$('.dz-preview').remove();
		            this.on("success", function(file) {
                      var filename = file['name'].split('.');  
                      console.log ( file['name'] );
                      $('.receipt_image').val( $('.customer_id').val() +  '_' + $('.image_uuid').val() + '.' + filename[1] );
                      $('.scontrino-img').attr ( 'src' ,  'http://www.tantipremi.it/public/users/upload/temp/' + $('.customer_id').val() +  '_' + $('.image_uuid').val() + '.' + filename[1] )
					  $('.dz-error-message').html('');
					  $('.dz-preview').remove();
        		      $('#uploadModal').modal('hide');
	            	});
					this.on("error" , function ( e , errorMsg ){
						$('.dz-error-message').html(errorMsg);
						$('.dz-details').html('');
					});
       			}
			
            });
});
</script>   