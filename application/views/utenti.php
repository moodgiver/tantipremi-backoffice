
<div class="col-lg-12">
	<table id="table1" class="table table-striped table-bordered">
		<thead>
			<th>#</th>
			<th>Utente</th>
			<th>Email</th>
			<th>Ruolo</th>
			<th>Concorsi</th>
			<th></th>
		</thead>
		<tbody>
			<?php
				$c = 1;
				$id = '';
				foreach ( $users as $user ){
					if ( $id != $user['id'] ){
						echo '<tr>
						<td>'.$c.'</td>
						<td class="username_'.$user['id'].'">'.$user['username'].'</td>
						<td class="email_'.$user['id'].'">'.$user['email'].'</td>
						<td>'.$user['user_type'].'</td>
						<td>'.$user['campaign'].'&nbsp;';
						if ( $user['user_type'] == 'O' ){
							echo '<button class="btn bnt-primary btn-flat btn-action" data-controller="user-campaign-remove" data-id="'.$user['id'].'" data-campaign="'.$user['campaign_id'].'" title="Rimuovi dal concorso"><span class="fa fa-close"></span></button>';
						}
						echo '<td>';
					if ( $user['user_type'] != 'SA' ){
						echo '<button class="btn bnt-primary btn-flat btn-edit" data-controller="user-edit" data-id="'.$user['id'].'"><span class="fa fa-edit"></span></button>&nbsp;
						<button class="btn bnt-primary btn-flat btn-action" data-controller="user-delete" data-id="'.$user['id'].'"><span class="fa fa-trash"></span></button>';
						echo '</td>';
					}
					} else {
						echo '<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>'.$user['campaign'].'&nbsp;';
						if ( $user['user_type'] == 'O' ){
							echo '<button class="btn bnt-primary btn-flat btn-action" data-controller="user-campaign-remove" data-id="'.$user['id'].'" data-campaign="'.$user['campaign_id'].'" title="Rimuovi dal concorso"><span class="fa fa-close"></span></button>';
						}
						echo '</td><td></td>';
					}
					
					echo '</td></tr>';
					$id = $user['id'];
					$c++;
				}
			?>
		</tbody>
		
	</table>
	<div class="col-lg-12 text-center"><p>Legenda Ruolo: SA = Super Administrator   A = Amministratore   O = Operatore</p></div>
</div>

<div class="modal fade" id="modal-utente" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modifica Utente</h4>
      </div>
      <div class="modal-body">
		  <div class="row">
	        <div class="col-lg-6">
				<label>Utente</label>
				<input type="text" class="form-control username">
			</div>
			<div class="col-lg-6">
				<label>Password</label>
				<input type="password" class="form-control password">
			</div>
			<div class="col-lg-6">
				<label>Email</label>
				<input type="email" class="form-control email">
			</div>
	      </div>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

	<script>
	
  $(function () {
    $('#table1').DataTable({
      "paging": false,
      "lengthChange": true,
	  "pageLength": 50,
      	"searching": true,
      	"ordering": false,
      	"info": true,
      	"autoWidth": false,
	  	"pagingType": "full_numbers",
	 "language":
	 	{
	  		"sEmptyTable":     "Nessun dato presente nella tabella",
		 	"sInfo":           "Vista da _START_ a _END_ di _TOTAL_ elementi",
			"sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
			"sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
			"sInfoPostFix":    "",
			"sInfoThousands":  ".",
			"sLengthMenu":     "Visualizza _MENU_ elementi",
			"sLoadingRecords": "Caricamento...",
			"sProcessing":     "Elaborazione...",
			"sSearch":         "Cerca:",
			"sZeroRecords":    "La ricerca non ha portato alcun risultato.",
			"oPaginate": {
				"sFirst":      "Inizio",
				"sPrevious":   "Precedente",
				"sNext":       "Successivo",
				"sLast":       "Fine"
			},
			"oAria": {
				"sSortAscending":  ": attiva per ordinare la colonna in ordine crescente",
				"sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
			}
	   }
    });
  });
	</script>	
	
	<script>
	
	$(document).ready ( function(){
		$('.btn-edit').click ( function(){
			var id = $(this).data('id');
			$('.username').val ( $('.username_' + id ).html() );
			$('.email').val ( $('.email_' + id ).html() );
			$('#modal-utente').modal('show');
		});
	});
	
	</script>