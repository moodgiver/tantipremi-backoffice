<?php
  $hide = 'hide';
  if ( $_SESSION['login']['user_type'] != 'O' ){
    $hide = '';
  }
  $return = '';
  $s = $this->config->item('status');
  $class = $this->config->item('status_label');
  foreach ( $scontrini as $scontrino ){
    $ext = substr($scontrino['receipt'],-3);
    if ( $ext != 'pdf' ){
      $sc = '<span class="fa fa-picture-o pointer preview-receipt btn-action" data-controller="preview-doc" data-title="scontrino"  data-id="//www.tantipremi.it/public/users/upload/'.$scontrino['receipt'].'"></span>';
    } else {
      $sc = '<a href="//www.tantipremi.it/public/users/upload/'.$scontrino['receipt'].'" target="_blank"><span class="fa fa-file-pdf-o pointer"></span></a>';
    }
    $stato = '<label class="label label-'.$class[$scontrino['status']].'">'.$s[$scontrino['status']].'</label>';

    $coupon = '';
    if ( $scontrino['coupon'] ){
      $coupon = '<tr>
        <td colspan="8">
          <label class="badge">Coupon</label>: <strong>'.$scontrino['coupon'].'</strong> - <label class="badge">'.$scontrino['release_date'].'</label>
        </td>
      </tr>';
    }
    if ( $scontrino['receipt_nr'] != '' ){
        echo '<tr>
        <td>'.$scontrino['receipt_nr'].'['.$scontrino['id'].']<br>'.$stato.'</td>
        <td>'.date("d/m/Y",strtotime($scontrino['receipt_date'])).'</td>
        <td>'.date("d/m/Y",strtotime($scontrino['register_date'])).'</td>
        <td>'.$scontrino['campaign'].'</td>
        <td>'.$scontrino['prize'].'</td>
        <td>'.$scontrino['insegna'].' ('.$scontrino['shop_state'].')<br>'.$scontrino['organizzazione'].'</td>
        <td>'.$sc.'</td>
        <td><button class="btn btn-sm btn-primary btn-action '.$hide.'" data-controller="edit-receipt" data-campaign="'.$scontrino['campaign_id'].'" data-originator="clienti"  data-receipt="'.$scontrino['id'].'" data-customer="'.$scontrino['firstname'].' '.$scontrino['lastname'].'" data-registration="'.date("d/m/Y",strtotime($scontrino['register_date'])).'" data-receipt_nr="'.$scontrino['receipt_nr'].'" data-receipt_date="'.date("d/m/Y",strtotime($scontrino['register_date'])).'" data-customer_id="'.$scontrino['customer_id'].'" data-image="'.$scontrino['receipt'].'" data-prize="'.$scontrino['prize_id'].'"><span class="fa fa-edit"></span></button>
        <input type="hidden" id="extra_'.$scontrino['id'].'" value="'.$scontrino['extra'].'">
         </td>
        </tr>'
        .$coupon;
    }
  }
?>
