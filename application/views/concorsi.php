<div class="col-lg-12">
	<table id="table1" class="table table-striped table-bordered">
	  	<thead>
			<th></th>
			<th>Concorso</th>
			<th>Inizio</th>
			<th>Fine</th>
			<th>Stato</th>
			<th></th>
		</thead>
		<tbody>
		<?php 
			  	$datestring = '%d/%m/%Y';
				//print_r ( $_SESSION );
				$permission = false;
				if ( $_SESSION['login']['user_type'] != 'O' ){
					$permission = true;
				}
			  	foreach ( $campaigns as $row ){
					if ( $_SESSION['login']['user_type'] != 'O' || in_array ( $row['campaign_id'] , $_SESSION['login']['user_campaigns'] ) ){
					
						$datestart = date('Ymmdd',strtotime($row['start']));
						$dateend = date('Ymmdd',strtotime($row['end']));
						$oggi = date('Ymmdd');
						if ( $datestart < $oggi ){
							if ( $dateend > $oggi ){
								$classe = "success";
								$stato = 'ATTIVO';
							} else {
								$classe = "danger";
								$stato = 'CHIUSO';
							}
						} else {
							$classe = "danger";
							$stato = 'NON ATTIVO';
						}
						if ( $row['is_active'] == '0' ){
						$classe = "info";
						$stato = "CHIUSA";
					}
						echo '<tr>
						<td class="btn-action" data-controller="edit-concorso" data-title="'.$row['campaign'].'" data-id="'.$row['campaign_id'].'" title="Modifica Concorso">
							<img src="'.$this->service->web_url().'public/img/'.$row['code'].'/'.$row['thumbnail'].'" width="50">
						</td>
						<td class="btn-action" data-controller="edit-concorso" data-title="'.$row['campaign'].'" data-id="'.$row['campaign_id'].'" title="Modifica Concorso">
							'.$row['campaign'].'
						</td>
						<td>
							'.mdate($datestring,strtotime($row["start"])).'
						</td>
						<td>
							'.mdate($datestring,strtotime($row["end"])).'
						</td>
						<td>
							<label class="label label-'.$classe.'">'.$stato.'</label>
						</td>
						<td>';
						if ( $permission ){
							echo '
							<button class="btn btn-sm btn-default btn-action" data-controller="edit-concorso" data-title="'.$row['campaign'].'" data-id="'.$row['campaign_id'].'" title="Modifica Concorso"><span class="fa fa-edit"></span></button>&nbsp;';
						}
						echo '<button class="btn btn-sm btn-default btn-action" data-controller="iscritti-concorso" data-title="'.$row['campaign'].'" data-id="'.$row['campaign_id'].'" data-permission="'.$permission.'" title="Vedi Partecipanti"><span class="fa fa-users"></span></button>
						</td>
						</tr>';
					}
				}
		  ?>
		  </tbody>
	</table>
</div>	