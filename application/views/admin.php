
			
		<div class="clearfix"></div>
        <section>
			<div class="row content">
				<div class="col-lg-12">
				<table class="table table-striped">
			  	<thead>
					<th>Concorso</th>
					<th>Inizio</th>
					<th>Fine</th>
					<th>Stato</th>
				</thead>
			  <?php 
			  	$datestring = '%d/%m/%Y';
				
			  	foreach ( $campaigns as $row ){
					$datestart = date('Ymmdd',strtotime($row['start']));
					$dateend = date('Ymmdd',strtotime($row['end']));
					$oggi = date('YmdHi');
					if ( $datestart < $oggi ){
						if ( $dateend > $oggi ){
							$classe = "success";
							$stato = 'ATTIVA';
						} else {
							$classe = "danger";
							$stato = 'Conclusa';
						}
					} else {
						$classe = "danger";
						$stato = 'Non iniziata';
					}
					if ( $row['is_active'] == '0' ){
						$classe = "info";
						$stato = "CHIUSA";
					}
					echo '<tr>
					<td>
						'.$row['campaign'].'
					</td>
					<td>
						'.mdate($datestring,strtotime($row["start"])).'
					</td>
					<td>
						'.mdate($datestring,strtotime($row["end"])).'
					</td>
					<td>
						<label class="label label-'.$classe.'">'.$stato.'</label>
					</td>
					</tr>';
				}
			  ?>
			  </table>
				</div>
			</div>
			          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

