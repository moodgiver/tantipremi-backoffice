
<div class="col-lg-12">
		<table id="tableshops" class="table table-striped table-bordered dataTable filter-result">
			<thead>
				<tr role="row">
				<th>#</th>
				<th class="sorting" tabindex="0">Organizzazione</th>
				<th class="sorting" tabindex="0">Punto Vendita</th>

        <th class="sorting" tabindex="0">Regione</th>
        <th class="sorting" tabindex="0">Comune</th>
        <th class="sorting" tabindex="0">Provincia</th>
				<th>Indirizzo</th>
				<th>Inizio</th>
				<th></th>
				</tr>
			</thead>
			<tbody>
			<?php
				if ( count($shops) > 0 ){
					$c = 1;
					foreach ( $shops as $r ){
						$imagesLink = '';
						if ( $r['image'] == '' ){
							$imagesLink = 'hide';
						}
						echo '<tr class="pointer">
							<td>'.$c.'</td>

							<td>'.$r['organizzazione'].'</td>
							<td><strong>'.$r['insegna'].'</strong><br>'.$r['nome'].' '.$r['cognome'].'</td>
							<td>'.$r['regione'].'<br>'.$r['email'].'</td>
              <td>'.$r['comune'].'</td>
							<td>'.$r['provincia'].'</td>
							<td>'.$r['indirizzo'].'</td>
							<td>ND</td>
							<td><span class="fa fa-picture-o btn-action pointer '.$imagesLink.'" data-controller="contest-shop-foto" data-id="'.$r['insegna_id'].'" title="Vedi foto Punto Vendita"></span></td>
						</tr>';
						$c++;
					}
				}

			?>
			</tbody>
		</table>
