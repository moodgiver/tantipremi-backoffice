$(document).ready(function() {
    b = $('body');
    c = $('.content');
    title = $('.title-header');
    ajax = 'ajax';

    b.delegate('.search-input', 'keypress', function(e) {
        var controller = $(this).data('controller');
        if (e.which == 13) {
            // enter pressed
            switch (controller) {
                case 'search-user':

                    c.html('');

                    var search = $(this).val();

                    $.post(ajax,

                        {

                            action: controller,

                            id: $(this).data('id'),

                            search: search

                        },
                        function(result) {

                            c.html(result);

                        }

                    );

                    break;

            }

        }



    });



    b.delegate('.select-input', 'change', function() {

        var controller = $(this).data('controller');

        switch (controller) {





            case "update-status":

                var btn = $(this);

                var a = $(this).val().split(';');

                var status = $(this).data('status');

                var newstatus = a[0];

                var nextstatus = a[1];

                var sendmail = a[2];

                console.log('id = ' + $(this).data('id'));

                console.log('status = ' + status);

                console.log('campaign = ' + $(this).data('campaign'));

                console.log('email = ' + $(this).data('email'));

                console.log('next = ' + a[0]);

                console.log('sendmail = ' + sendmail);



                $.post('ajax_customer_status',

                    {

                        id: $(this).data('id'),

                        status: status,

                        email: $(this).data('email'),

                        campaign: $(this).data('campaign'),

                        nextstatus: newstatus,

                        sendmail: sendmail

                    },
                    function(result) {

                        $.post('ajax_refresh_status',

                            {

                                id: btn.data('id'),

                                status: btn.data('status'),

                                email: btn.data('email'),

                                campaign: btn.data('campaign'),

                                nextstatus: newstatus,

                                sendmail: sendmail

                            },
                            function(result) {

                                //console.log ( result );

                                $('.label_status_' + btn.data('id')).html(result);

                            }

                        );

                        //$('.label_status_' + $(this).data('id')).html =

                    }

                );



                break;



            case "list-shops":

                if ($('.campaign_id').val() != '') {

                    $.post(ajax,

                        {

                            action: 'list-shops',

                            campaign_id: $('.campaign_id').val(),

                            pv: $(this).val()

                        },
                        function(result) {

                            var selection = '<label>Punto vendita </label><select class="form-control shop_id">' + result + '</select>'
                            if ( !$(this).data('insegna') ){
                                return $('.negozio').html(selection);
                            } else {
                                return $('.insegne').html(selection);
                            }


                        }

                    );

                } else {

                    alert("Selezionare la promozione prima di indicare il punto vendita");

                }

                break;

            case "list-shops-receipt":
                        $.post(ajax,
                            {

                                action: 'list-shops',

                                campaign_id: $('.campaign_id').val(),

                                pv: $(this).val()

                            },
                            function(result) {

                                var selection = '<label>Punto vendita </label><select class="form-control shop_id">' + result + '</select>'
                                return $('.insegne').html(selection);


                            }

                        );

                    break;


            case "list-premi":

                $.post(ajax,

                    {

                        action: 'premi-concorso',

                        id: $(this).val()

                    },
                    function(result) {
                        var selection = '<div class="col-lg-4"><label>Premio </label><select class="form-control prize_id">' + result + '</select></div>'

                        $('.premi').html(selection);


                    }

                );



                $.post(ajax,

                    {

                        action: 'list-province',

                        id: $(this).val()

                    },
                    function(result) {

                        $('.province').html('<label>Provincia</label><select class="form-control select-input provincia" data-controller="list-shops""><option value="">seleziona...</option>' + result + '</select>');

                    }

                );

                break;



        }



    });



    b.delegate('.btn-action', 'click', function() {

        var controller = $(this).data('controller');

        var obj = $(this);

        switch (controller) {



            case 'login':

                $.post(ajax,

                    {

                        action: 'login',

                        u: $('.ulogin').val(),

                        p: $('.upwd').val()

                    },
                    function(result) {

                        console.log(result);

                        if (result != '') {

                            $('.mylogin').hide('fade');

                            window.location.href = result;

                        } else {

                            alert('Utente o password errati');

                        }

                    }

                );

                break;



            case 'menu':

                title.html($(this).data('title'));

                c.html('');

                $.post(ajax,

                    {

                        action: $(this).data('menu')

                    },
                    function(result) {

                        c.html(result);

                    }

                );

                break;



            case 'edit-concorso':

                title.html('Modifica ' + $(this).data('title'));



                var titolo = $(this).data('title');

                var id = $(this).data('id');

                c.html('');

                $.post(ajax,

                    {

                        action: controller,

                        id: $(this).data('id')

                    },
                    function(result) {

                        doMenuConcorso(titolo, id);



                        c.html(result);

                    }

                );

                break;



            case 'concorso-setting':



                var titolo = $(this).data('title');

                var id = $(this).data('id');

                var action = $(this).data('action');

                title.html(titolo);

                c.html('');

                $.post(ajax,

                    {

                        action: action,

                        id: $(this).data('id'),

                        from: $('.startDateIscritti').val(),

                        to: $('.endDateIscritti').val()

                    },
                    function(result) {

                        c.html(result);

                    }

                );

                break;



            case 'settings-save':

                var id = $(this).data('id');

                var active = 0;

                if ($('.campaign_active').is(':checked')) {

                    active = 1;

                }

                $('textarea.ckeditor').each(function() {

                    var $textarea = $(this);

                    $textarea.val(CKEDITOR.instances[$textarea.attr('name')].getData());

                });

                $.post(ajax,

                    {

                        action: controller,

                        id: $(this).data('id'),

                        campaign: $('.campaign_name').val(),

                        url: $('.campaign_url').val(),

                        start: $('.campaign_start').val(),

                        end: $('.campaign_end').val(),

                        days: $('.campaign_days').val(),

                        shortdesc: $('.campaign_abstract').val(),

                        description: $('#campaign_description').val(),

                        faq: $('#campaign_faq').val(),

                        howto: $('#campaign_howto').val(),

                        active: active

                    },
                    function(result) {

                        doNotification('', 'Informazioni salvate');

                    }

                );

                break;



            case "save-prize":

                var id = $(this).data('id');

                $.post(ajax,

                    {

                        action: controller,

                        id: id,

                        title: $('.p_name_' + id).val(),

                        desc: $('.p_desc_' + id).val()

                    }

                );

                break;



            case "add-prize":

                var id = $(this).data('id');

                $.post(ajax,

                    {

                        action: controller,

                        id: id,

                        title: $('.p_name_0').val(),

                        desc: $('.p_desc_0').val()

                    }

                );

                break;



            case "create-coupons":

                var id = $(this).data('id');

                var code = $(this).data('code');

                var prize = $(this).data('prize');

                $.post(ajax,

                    {

                        action: controller,

                        id: id,

                        code: code,

                        prize: prize

                    },
                    function(result) {

                        doNotification('', 'Coupon creati');

                        $('.coupons_' + prize).removeClass('hide');

                    }

                );

                break;



            case "export-coupons":

                var id = $(this).data('id');

                var prize = $(this).data('prize');

                $('#workFrame').attr('src', 'export_coupons/' + id + '/' + prize + '/');

                break;



            case "iscritti-concorso":

                title.html('Partecipanti ' + $(this).data('title'));

                c.html('');



                if ($(this).data('permission')) {

                    doMenuConcorso($(this).data('title'), $(this).data('id'));

                }

                $.post(ajax,

                    {

                        action: controller,

                        id: $(this).data('id')

                    },
                    function(result) {

                        c.html(result);

                    }

                );

                break;



            case "iscritti-concorso-filter":
                title.html($('.title-header').html());
                if ($(this).data('permission')) {
                    doMenuConcorso($(this).data('title'), $(this).data('id'));
                }
                var sDate = $('.filter-start-date').val();
                var eDate = $('.filter-end-date').val();
                $.post(ajax,
                    {
                        action: controller,
                        id: $(this).data('id'),
                        nome: $('.search-name').val(),
                        status: $('.select-status').val(),
                        from: $('.filter-start-date').val(),
                        to: $('.filter-end-date').val(),
                        organizzazione: $('.filter-organization').val()
                    },
                    function(result) {
                        c.html('');
                        c.html(result);
                        $('.filter-start-date').val(sDate)
                        $('.filter-end-date').val(eDate)
                    }
                );

                break;

              case "iscritti-concorso-estrazione-modal":
                $('#estrazioneModal').modal('show');
                break;

              case "iscritti-concorso-estrai":
                    title.html($('.title-header').html());
                    if ($(this).data('permission')) {
                        doMenuConcorso($(this).data('title'), $(this).data('id'));
                    }
                    var sDate = $('.filter-start-date').val();
                    var eDate = $('.filter-end-date').val();
                    var totali = $(this).data('rows');
                    var estrarre = $('.filter-estrai-rows').val();
                    $('#estrazioneModal').modal('hide')
                    $.post(ajax,
                        {
                            action: controller,
                            id: $(this).data('id'),
                            from: $('.filter-start-date').val(),
                            to: $('.filter-end-date').val(),
                            organizzazione: $('.filter-organization').val(),
                            da_estrarre: $('.filter-estrai-rows').val()
                        },
                        function(result) {
                            c.html('');
                            c.html(result);
                            $('.filter-start-date').val(sDate)
                            $('.filter-end-date').val(eDate)

                        }
                    );
                    break;

            case "conferma-estrazione":
              var id = $(this).data('id')
              $.post ( ajax ,
                {
                  action: controller,
                  id: id
                },function(result){
                  console.log ( result );
                  $('.estrazione-' + id ).html('<span class="badge">Estratto</span>');
                }
              );
              break;

            case "estratti-concorso":
              title.html($('.title-header').html());
              if ($(this).data('permission')) {
                doMenuConcorso($(this).data('title'), $(this).data('id'));
              }
              var sDate = $('.filter-start-date').val();
              var eDate = $('.filter-end-date').val();
              $.post(ajax,
                {
                    action: controller,
                    id: $(this).data('id'),
                    from: $('.filter-start-date').val(),
                    to: $('.filter-end-date').val()
                },
                function(result) {
                    c.html('');
                    c.html(result);
                    $('.filter-start-date').val(sDate)
                    $('.filter-end-date').val(eDate)
                }
            );

            break;

            case "edit-receipt":

                $('.customer').val($(this).data('customer'));

                $('.receipt_nr').val($(this).data('receipt_nr'));

                $('.register_date').val($(this).data('registration'));

                $('.receipt_date').val($(this).data('receipt_date'));

                $('.receipt_extra').val($('#extra_' + $(this).data('receipt')).val());

                //$('.receipt_image').val ( $(this).data('image') );
								$('.open_scontrino').prop('href','http://www.tantipremi.it/public/users/upload/' + $(this).data('image'));

                $('.scontrino-img').attr('src', 'http://www.tantipremi.it/public/users/upload/' + $(this).data('image'));

								$('.puntovendita').html($(this).data('shop'));
                $('.shop_id').val($(this).data('shopid'));

                $('.btn-edit-receipt-puntovendita').attr('data-id',$(this).data('campaign'));

                $('.btn-save-receipt').attr('data-controller', 'receipt-update');

                $('.btn-save-receipt').attr('data-id', $(this).data('receipt'));

                $('.btn-save-receipt').removeClass('hide');



                $('.customer_id').val($(this).data('customer_id'));
				var prize = $(this).data('prize');

                $.post(ajax,

                    {

                        action: 'premi-concorso',

                        id: $(this).data('campaign')

                    },
                    function(result) {



                        var selection = '<label>Premio </label><select class="form-control prize_id">' + result + '</select>'

                        $('.premi').html(selection);
						console.log ( "premio" , prize );
						$('.prize_id').val(prize).change();

                    }

                );



                $('#receiptModal').modal('show');

                break;

            case 'receipt-checked':

              var id = $(this).data('id');
              var obj = $(this);
              var checked = $(this).data('checked');
              $.post( ajax ,
                {
                  action: 'receipt-checked',
                  id: id,
                  checked: checked
                } , function ( result ){
                  if ( checked == '1' ){
                    obj.removeClass('label-danger');
                    obj.addClass('label-success');
                    obj.attr('data-checked','0');
                    obj.html('OK');
                  } else {
                    obj.removeClass('label-success');
                    obj.addClass('label-danger');
                    obj.attr('data-checked','1');
                    obj.html('NO');
                  }
                }
              );
            break;
            case 'estratto-checked':

              var id = $(this).data('id');
              var obj = $(this);
              var checked = $(this).data('checked');
              $.post( ajax ,
                {
                  action: 'estratto-checked',
                  id: id,
                  checked: checked
                } , function ( result ){
                  if ( checked == '1' ){
                    obj.removeClass('label-danger');
                    obj.addClass('label-success');
                    obj.attr('data-checked','0');
                    //obj.html('OK');
                  } else {
                    obj.removeClass('label-success');
                    obj.addClass('label-danger');
                    obj.attr('data-checked','1');
                    //obj.html('NO');
                  }
                }
              );
            break;

            case "edit-receipt-puntovendita":
              var id = $(this).data('id')
              $.post ( ajax ,
                {
                  action: 'list-province',
                  id: id
                } , function(result){

                      var selection = '<input type="hidden" class="campaign_id" value="' + id + '"><label>Provincia </label><select class="form-control select-input" data-controller="list-shops-receipt">' + result + '</select>';
                      $('.provincia').html ( selection );
                }
              );
              break;

            case "receipt-update":

                var id = $(this).data('id');

                var originator = $(this).data('originator');

                $.post(ajax,

                    {

                        action: controller,

                        id: $(this).data('id'),

                        receipt: $('.receipt_nr').val(),

                        register_date: $('.register_date').val(),

                        receipt_date: $('.receipt_date').val(),

                        receipt_extra: $('.receipt_extra').val(),

                        receipt_img: $('.receipt_image').val(),

                        prize_id: $('.prize_id').val(),

                        shop_id: $('.shop_id').val()

                    },
                    function(result) {

                        if (result) {

                            if (originator == 'clienti') {

                                $.post(ajax,

                                    {

                                        action: 'customer-scontrini',

                                        customer_id: $('.customer_id').val(),

                                    },
                                    function(result) {

                                        $('.customers').addClass('hide');

                                        $('.customer_' + $('.customer_id').val()).removeClass('hide');

                                        $('#table-scontrini').removeClass('hide');

                                        $('.scontrini-body').html(result);

                                    }

                                );

                            }

                            /*$('.receipt_nr_' + id).html ( $('.receipt_nr').val () );

                            $('.register_' + id).html ( $('.register_date').val () );

                            $('.receipt_date_' + id).html ( $('.receipt_date').val () );

                            */

                            doNotification("Dati aggiornati", "Dati dello scontrino modificati. Eventuali indicazioni sono da considerare nulle. Aggiornare lo stato per verifica");

                            $('#receiptModal').modal('hide');

                        } else {

                            alert("Errore nella modifica dei dati dello scontrino. Contattare l'amministratore");

                        }

                    }

                );



                break;



            case "add-receipt":



                $('.customer').val($(this).data('customer'));



                $('.register_date').val($(this).data('registration'));

                $('.receipt_date').val($(this).data('receipt_date'));

                $('.btn-save-receipt').attr('data-controller', 'receipt-insert');

                $('.btn-save-receipt').attr('data-customer_id', $(this).data('customer_id'));

                //$('.btn-save-receipt').attr('data-id',"0");

                $.post(ajax,

                    {

                        action: 'list-concorsi'

                    },
                    function(result) {



                        var selection = '<div class="clearfix"></div><div class="col-lg-4"><label>Concorso </label><select class="form-control campaign_id select-input" data-controller="list-premi"><option value="">seleziona...</option>' + result + '</select></div>'

                        $('.concorsi').html(selection);

                        $('#receiptModal').modal('show');

                    }

                );



                break;



            case "receipt-insert":

                console.log($(this).data('customer_id'));

                $.post(ajax,

                    {

                        action: controller,

                        customer_id: $('.customer_id').val(),

                        receipt: $('.receipt_nr').val(),

                        register_date: $('.register_date').val(),

                        receipt_date: $('.receipt_date').val(),

                        receipt_img: $('.receipt_image').val(),

                        campaign_id: $('.campaign_id').val(),

                        prize_id: $('.prize_id').val(),

                        shop_id: $('.shop_id').val(),

                        shop_state: $('.provincia').val()

                    },
                    function(result) {

                        if (result) {

                            doNotification("Scontrini", "Lo scontrino e' stato aggiunto con successo");

                            $('#receiptModal').modal('hide');

                            $.post(ajax,

                                {

                                    action: 'customer-scontrini',

                                    customer_id: $('.customer_id').val(),

                                },
                                function(result) {

                                    $('.customers').addClass('hide');

                                    $('.customer_' + $('.customer_id').val()).removeClass('hide');

                                    $('#table-scontrini').removeClass('hide');

                                    $('.scontrini-body').html(result);

                                }

                            );

                        } else {

                            alert("Errore nell'inserimento dello scontrino. Contattare l'amministratore");

                        }

                    }

                );

                break;



            case "delete-receipt":

                var conferma = confirm("Confermi la cancellazione di questo scontrino");

                var id = $(this).data('id');

                if (conferma) {



                    $.post(ajax,

                        {

                            action: 'delete-customer-receipt',

                            id: id

                        },
                        function(result) {

                            $('.scontrino_' + id).addClass('hide');

                            doNotification("Elimina scontrino", "Lo scontrino e' stato eliminato");

                        }

                    );

                }

                break;



            case "xls-status-export":

                var id = $(this).data('id');

                var status = $('.select-status').val();

                if (status != '') {

                    $('#workFrame').attr('src', 'export_status_filter/' + id + '/' + status + '/');

                } else {

                    $('#workFrame').attr('src', 'export_status/' + id);

                }

                break;





            case "preview-doc":



                $.fancybox.open({
                    href: $(this).data('id'),
                    title: $(this).data('title')
                });

                break;



            case "send-mail-status":

                $.post('ajax_mail_status',

                    {

                        id: $(this).data('id'),

                        status: $(this).data('status'),

                        email: $(this).data('email'),

                        campaign: $(this).data('campaign')

                    },
                    function(result) {

                        console.log(result);

                    }

                );

                break;



            case "update-status":

                var btn = $(this);

                $.post('ajax_customer_status',

                    {

                        id: $(this).data('id'),

                        status: $(this).data('status'),

                        email: $(this).data('email'),

                        campaign: $(this).data('campaign'),

                        nextstatus: $(this).data('flow'),

                        sendmail: $(this).data('sendmail')

                    },
                    function(result) {

                        $.post('ajax_refresh_status',

                            {

                                id: btn.data('id'),

                                status: btn.data('status'),

                                email: btn.data('email'),

                                campaign: btn.data('campaign'),

                                nextstatus: btn.data('flow'),

                                sendmail: btn.data('sendmail')

                            },
                            function(result) {

                                //console.log ( result );

                                $('.label_status_' + btn.data('id')).html(result);

                            }

                        );

                        //$('.label_status_' + $(this).data('id')).html =

                    }

                );

                break;



            case 'send-coupon':

                var promo = $(this).data('id');

                var customer = $(this).data('customer');

                var email = $(this).data('email');

                var receipt = $(this).data('receipt');

                var campaign = $(this).data('title');

                var prize = $(this).data('prize');

                $.post(ajax,

                    {

                        action: controller,

                        promo: promo,

                        customer: customer,

                        email: email,

                        receipt: receipt,

                        prize: prize

                    },
                    function(result) {



                        if (result != "error-coupon-sent" && result != "error-no-coupon") {

                            $('.panel-body').html(result);



                            $('#genericModal').modal('show');



                            $('.dynobutton').html('<button class="btn btn-danger btn-action" data-dismiss="modal" data-controller="delete-send-coupon">Elimina</button>&nbsp;<button class="btn btn-success btn-action" data-dismiss="modal" data-controller="confirm-send-coupon" data-email="' + email + '" data-campaign="' + campaign + '">Invia</button>');

                        } else {

                            if (result == "error-coupon-sent") {



                                alert("E' gia' stato emesso un voucher per questa giocata");

                            }

                            if (result == "error-no-coupon") {

                                alert("Non esistono voucher per questo concorso");

                            }

                        }

                    }

                );

                break;



            case 'send-coupon-multi':

                var promo = $(this).data('id');

                var customer = $(this).data('customer');

                var email = $(this).data('email');

                var receipt = $(this).data('receipt');

                var campaign = $(this).data('title');

                var prize = $(this).data('prize');

                $.post(ajax,

                    {

                        action: controller,

                        promo: promo,

                        customer: customer,

                        email: email,

                        receipt: receipt,

                        prize: prize

                    },
                    function(result) {

                        if (result != "error-coupon-sent" && result != "error-no-coupon") {

                            $('.panel-body').html(result);



                            $('#genericModal').modal('show');



                            $('.dynobutton').html('<button class="btn btn-danger btn-action" data-dismiss="modal" data-controller="delete-send-coupon-multi">Elimina</button>&nbsp;<button class="btn btn-success btn-action" data-dismiss="modal" data-controller="confirm-send-coupon-multi" data-email="' + email + '" data-campaign="' + campaign + '">Invia</button>');

                        } else {

                            if (result == "error-coupon-sent") {



                                alert("E' gia' stato emesso un voucher per questa giocata");

                            }

                            if (result == "error-no-coupon") {

                                alert("Non esistono voucher per questo concorso");

                            }

                        }

                    }

                );

                break;



            case 'delete-send-coupon':

                $.post(ajax,

                    {

                        action: controller

                    },
                    function(result) {

                        $('#genericModal').modal('hide');

                    }

                );

                break;



            case 'delete-send-coupon-multi':

                $.post(ajax,

                    {

                        action: controller

                    },
                    function(result) {

                        $('#genericModal').modal('hide');

                    }

                );

                break;



            case 'confirm-send-coupon':

                $.post(ajax,

                    {

                        action: controller,

                        campaign: $(this).data('campaign'),

                        email: $(this).data('email'),

                        voucher: $('.panel-body').html()

                    },
                    function(result) {

                        if (result == '') {

                            doNotification("Invio Voucher", "Voucher inviato regolarmente");

                        } else {

                            alert("Errore nell'invio del voucher");

                        }

                    }

                );

                break;





            case 'confirm-send-coupon-multi':

                $.post(ajax,

                    {

                        action: controller,

                        campaign: $(this).data('campaign'),

                        email: $(this).data('email'),

                        voucher: $('.panel-body').html()

                    },
                    function(result) {

                        if (result == '') {

                            doNotification("Invio Voucher", "Voucher inviato regolarmente");

                        } else {

                            alert("Errore nell'invio del voucher");

                        }

                    }

                );

                break;



            case 'resend-coupon':

                var email = $(this).data('email');

                var campaign = $(this).data('title');

                $.post(ajax,

                    {

                        action: controller,

                        id: $(this).data('id'),

                        coupon: $(this).data('coupon'),

                        prize: $(this).data('prize'),

                        email: $(this).data('email')

                    },
                    function(result) {

                        $('.panel-body').html(result);

                        $('#genericModal').modal('show');

                        $('.dynobutton').html('<button class="btn btn-danger" data-dismiss="modal">Chiudi</button>&nbsp;<button class="btn btn-success btn-action" data-dismiss="modal" data-controller="confirm-resend-coupon" data-email="' + email + '" data-campaign="' + campaign + '">Invia</button>');

                    }

                );

                break;



            case 'confirm-resend-coupon':

                $.post(ajax,

                    {

                        action: controller,

                        campaign: $(this).data('campaign'),

                        email: $(this).data('email'),

                        voucher: $('.panel-body').html()

                    },
                    function(result) {

                        if (result) {

                            doNotification("Invio Voucher", "Voucher inviato regolarmente");

                        } else {

                            alert("Errore nell'invio del voucher");

                        }

                    }

                );

                break;



            case 'registrazioni':

                $.post(ajax,

                    {

                        action: controller

                    }

                );

                break;



            case 'registrazioni-filtra':

                $.post(ajax,

                    {

                        action: controller,

                        search: $('.search-input').val()

                    },
                    function(result) {

                        c.html('');

                        c.html(result);

                    }

                );

                break;



            case 'customer-scontrini':

                $('.customer_id').val($(this).data('id'));

                if ($('.scontrini').val() == '') {

                    $('.scontrini').val('active');

                    $.post(ajax,

                        {

                            action: controller,

                            customer_id: obj.data('id')

                        },
                        function(result) {

                            $('.customers').addClass('hide');

                            $('.customer_' + obj.data('id')).removeClass('hide');

                            $('#table-scontrini').removeClass('hide');

                            $('.scontrini-body').html(result);

                        }

                    );

                } else {



                    $('.customers').removeClass('hide');

                    $('#table-scontrini').addClass('hide');

                    $('.scontrini-body').html('');

                    $('.scontrini').val('');

                }

                break;



            case 'customer-edit':

                $.post(ajax,

                    {

                        action: controller,

                        id: $(this).data('id')

                    },
                    function(result) {

                        var customer = JSON.parse(result);

                        console.log(customer);

                        $('.firstname').val(customer[0]['firstname']);

                        $('.lastname').val(customer[0]['lastname']);

                        $('.dob').val(customer[0]['data_nascita']);

                        $('.address_1').val(customer[0]['address_1']);

                        $('.address_nr').val(customer[0]['address_nr']);

                        $('.city').val(customer[0]['city']);

                        $('.state').val(customer[0]['state']);

                        $('.zip').val(customer[0]['zip']);

                        $('.phone').val(customer[0]['phone']);

                        $('.mobile').val(customer[0]['mobile']);

                        $('.cf').val(customer[0]['cf']);

                        $('.email').val(customer[0]['email']);

                        $('.registration_date').val(customer[0]['data_registrazione']);

                        $('.btn-save-user').attr('data-id', customer[0]['customer_id']);

                        $('#customerModal').modal('show');

                    }

                );



                break;



            case 'customer-save':

                $.post(ajax,

                    {

                        action: controller,

                        id: $(this).data('id'),

                        firstname: $('.firstname').val(),

                        lastname: $('.lastname').val(),

                        dob: $('.dob').val(),

                        address_1: $('.address_1').val(),

                        address_nr: $('.address_nr').val(),

                        city: $('.city').val(),

                        state: $('.state').val(),

                        zip: $('.zip').val(),

                        phone: $('.phone').val(),

                        mobile: $('.mobile').val(),

                        cf: $('.cf').val(),

                        email: $('.email').val(),

                        password: $('.password').val(),

                        registration_date: $('.registration_date').val(),

                    },
                    function(result) {

                        $('#customerModal').modal('hide');

                        doNotification("Registrazioni", "Dati di registrazione salvati");

                    }

                );

                break;





            case 'edit-shop':

                var id = $(this).data('id');

                $.post(ajax,

                    {

                        action: controller,

                        id: id

                    }

                );

                break;

        }



    });



    function doMenuConcorso(titolo, id) {

        $('.concorso-menu').remove();

        $('.concorsi-menu').prepend('<li class="menu concorso-menu treeview active"><a href="#"><span class="fa fa-diamond"></span> ' + titolo + '</a><ul class="treeview-menu menu-open" style="display:block">\
		<li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Partecipanti ' + titolo + '" data-action="iscritti-concorso" data-id="' + id + '"><a href="#"><span class="fa fa-users"></span> Partecipanti</a></li>\
              <li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Estratti ' + titolo + '" data-action="estratti-concorso" data-id="' + id + '"><a href="#"><span class="fa fa-bullhorn"></span> Estratti</a></li>\
              <li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Premi a punti ' + titolo + '" data-action="premi-a-punti-concorso" data-id="' + id + '"><a href="#"><span class="fa fa-bullhorn"></span> Premi a Punti</a></li>\
							<li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Punti vendita ' + titolo + '" data-action="shops-concorso" data-id="' + id + '"><a href="#"><span class="fa fa-shopping-cart"></span> Punti vendita</a></li>\
              <li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Contest PV ' + titolo + '" data-action="contest-concorso" data-id="' + id + '"><a href="#"><span class="fa fa-shopping-cart"></span> Contest PV</a></li>\
              <li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Instant Win ' + titolo + '" data-action="instantwin-concorso" data-id="' + id + '"><a href="#"><span class="fa fa-shopping-cart"></span> Instant Win</a></li>\
							<li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-action="setting" data-title="Impostazioni ' + titolo + '" data-id="' + id + '"><a href="#"><span class="fa fa-cog"></span> Impostazioni</a></li>\
							<li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-action="style" data-title="Stile ' + titolo + '" data-id="' + id + '"><a href="#"><span class="fa fa-paint-brush"></span> Stile</a></li>\
							<li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-action="premi" data-title="Premi ' + titolo + '" data-id="' + id + '"><a href="#"><span class="fa fa-cubes"></span> Premi</a></li>\
							<li class="menu btn-action" data-controller="concorso-setting" data-menu="concorsi" data-title="Regolamento ' + titolo + '" data-action="regolamento" data-id="' + id + '"><a href="#"><span class="fa fa-file-pdf-o"></span> Regolamento</a></li>\
							</ul></li>');
    }



});



function doNotification(title, msg) {

    $('.notification').html('<h4>' + title + '</h4>');

    $('.notification').append('<p>' + msg + '</p>');

    $('.notification').show('fade');

    var removeMe = setTimeout(clearNotification, 3000);

}



function clearNotification() {

    $('.notification').hide('fade');

}





// avvio attivita  AJAX



$(document).ajaxStart(function() {



    $('.working').removeClass('hide');



});



// termine attivita  AJAX

$(document).ajaxStop(function() {

    $('.working').addClass('hide');



});
